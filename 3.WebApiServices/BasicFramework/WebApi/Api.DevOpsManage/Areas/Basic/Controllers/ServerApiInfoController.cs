﻿using Api.DevOpsManage.App_Start;
using Common.Model;
using Container.Library;
using DevOps.Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Validate.Library;

namespace Api.DevOpsManage.Areas.Basic.Controllers
{
    /// <summary>
    /// 接口服务相关逻辑接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Basic")]
    [Route("api/Basic/[controller]")]
    [ApiController]
    public class ServerApiInfoController : BaseApiController
    {

        private readonly ISysServerApiInfoService sysServerApiInfoService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public ServerApiInfoController()
        {
            sysServerApiInfoService = UnityCIContainer.Instance.GetService<ISysServerApiInfoService>();
        }

        #region 查询
        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<SysServerApiInfoResponse>> LoadPageList([FromBody] ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<SysServerApiInfoResponse>>();

            TryCatch(() =>
            {

                resultInfo = sysServerApiInfoService.LoadPageList(inputInfo);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "获取参数设置分页列表失败");

            }, $"系统错误，接口信息管理-获取参数设置分页列表失败");
            return resultInfo;
        }

        #endregion

        #region 更新

        /// <summary>
        /// 更新
        /// </summary>
        /// <returns></returns>
        [HttpPost("Save")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Save([FromBody] SysServerApiInfoSaveRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = sysServerApiInfoService.Save(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "更新参数信息失败");

            }, $"系统错误，接口信息管理-更新参数信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 删除
        /// </summary>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(List<string> Ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = sysServerApiInfoService.Remove(Ids);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "删除参数信息失败");

            }, $"系统错误，接口信息管理-删除参数信息失败");
            return resultInfo;
        }

        #endregion

    }
}
