﻿using System;
using System.Collections.Generic;
using Api.DevOpsManage.App_Start;
using DevOps.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.DevOpsManage.Areas.Basic.Controllers
{

    /// <summary>
    ///异常日志控制器 
    /// </summary>
    [ApiExplorerSettings(GroupName = "Basic")]
    [Route("api/Basic/[controller]")]
    [ApiController]
    public class ExceptionLogController : BaseApiController
    {
        private readonly IExceptionLogService exceptionLogService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ExceptionLogController()
        {
            exceptionLogService = UnityCIContainer.Instance.GetService<IExceptionLogService>();
        }

        /// <summary>
        /// 获取服务名称Select列表信息
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [HttpGet("LoadServiceNameList")]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadServiceNameList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() =>
            {
                resultInfo = exceptionLogService.LoadServiceNameList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取系统类型Select列表信息失败");

            }, $"系统错误，异常日志功能-获取系统类型Select列表信息失败");

            return resultInfo;
        }

        #region 异常日志管理相关逻辑

        /// <summary>
        /// 根据异常日志条件分页获取列表 关键字【业务标题】【操作明细】
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<ExceptionLogInfoResponse>> LoadList([FromBody]ParametersInfo<ExceptionLogQuery> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ExceptionLogInfoResponse>>();

            TryCatch(() =>
            {
                resultInfo = exceptionLogService.LoadExceptionList(inputInfo);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取异常日志分页列表失败");
            }, $"系统错误，日志管理-获取异常日志分页列表失败");
            return resultInfo;
        }
        /// <summary>
        /// 获取异常日志类型Select列表信息
        /// </summary>
        /// <returns></returns>

        [HttpGet("LoadLogTypeList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadLogTypeList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() =>
            {
                resultInfo = exceptionLogService.LoadLogTypeList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取异常日志类型Select列表信息失败");

            }, $"系统错误，日志管理-获取异常日志类型Select列表信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 批量删除日志
        /// </summary>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove([FromBody]ExceptionLogRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = exceptionLogService.Remove(request);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "删除异常日志失败");

            }, $"系统错误，日志管理-删除异常日志失败");

            return resultInfo;
        }


        /// <summary>
        /// 解决异常问题
        /// </summary>
        /// <returns></returns>
        [HttpGet("SolveException/{id}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> SolveException(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = exceptionLogService.SolveException(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "解决异常问题失败");

            }, $"系统错误，日志管理-解决异常问题失败");

            return resultInfo;
        }
        
        #endregion

        #region 异常日志统计相关逻辑
        /// <summary>
        /// 获取对应时间段内，异常信息数量
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet("LoadExceptionCount/{beginTime}/{endTime}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<ExceptionCountResponse> LoadExceptionCount(DateTime beginTime, DateTime endTime)
        {

            var resultInfo = new ResultJsonInfo<ExceptionCountResponse>();

            TryCatch(() =>
            {
                resultInfo = exceptionLogService.LoadExceptionCount(beginTime, endTime);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取对应时间段内，异常信息数量失败");

            }, $"系统错误，获取对应时间段内，异常信息数量失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取最新错误信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadLatestList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<ExceptionLogInfoResponse>> LoadLatestList()
        {
            var resultInfo = new ResultJsonInfo<List<ExceptionLogInfoResponse>>();

            TryCatch(() =>
            {
                resultInfo = exceptionLogService.LoadLatestList();
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取最新错误信息失败");
            }, $"系统错误，获取最新错误信息失败");
            return resultInfo;
        }
        #endregion
    }
}