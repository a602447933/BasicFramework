﻿using System.Collections.Generic;
using Api.DevOpsManage.App_Start;
using DevOps.Logic;
using Common.Model;
using Container.Library;
using DevOps.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
/*
* 命名空间: Api.DevOpsManage.Areas.Authority.Controllers
*
* 功 能： 运维系统主页面接口控制器
*
* 类 名： HomeController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.DevOpsManage.Areas.Admin.Controllers
{
    /// <summary>
    /// 系统主页面接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Home")]
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : BaseApiController
    {
        private readonly ISysUserService userService = null;
        private readonly IFunctionCfgService functionCfgService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public HomeController()
        {
            //用户管理逻辑注入
            userService = UnityCIContainer.Instance.GetService<ISysUserService>();
            //功能配置管理逻辑注入
            functionCfgService = UnityCIContainer.Instance.GetService<IFunctionCfgService>();
        }

        #region 主界面菜单逻辑方法

        /// <summary>
        /// 获取用户对应应用的可操作的所有菜单信息
        /// </summary>
        /// <param name="appSecret">应用appSecret</param>
        /// <returns></returns>
        [HttpGet("LoadMenuList/{appSecret}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<MenuInfo>> LoadMenuList(string appSecret)
        {
            var resultInfo = new ResultJsonInfo<List<MenuInfo>>();
            TryCatch(() =>
            {
                resultInfo = functionCfgService.LoadMenuOfUserList(appSecret);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取用户对应应用的可操作的所有菜单信息失败");

            }, $"系统错误，系统主页面管理-获取用户对应应用的可操作的所有菜单信息失败");
            return resultInfo;
        }


        /// <summary>
        /// 获取用户对应应用的可操作的一级菜单信息
        /// </summary>
        /// <param name="appSecret">应用appSecret</param>
        /// <returns></returns>
        [HttpGet("LoadTopMenuList/{appSecret}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<SysFunctionCfgEntity>> LoadTopMenuList(string appSecret)
        {
            var resultInfo = new ResultJsonInfo<List<SysFunctionCfgEntity>>();

            TryCatch(() =>
            {
                //层级级别 0：所有 10：一级 20：二级 30：三级
                resultInfo = functionCfgService.LoadTopMenuOfUserList(appSecret, 10);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取用户对应系统的可操作的一级菜单信息失败");

            }, $"系统错误，系统主页面管理-获取用户对应系统的可操作的一级菜单信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取左侧菜单信息
        /// </summary>
        /// <param name="appSecret">应用appSecret</param>
        /// <param name="parentId">父节点ID【就是当前节点】</param>
        /// <returns></returns>
        [HttpGet("LoadSubMenus/{appSecret}/{parentId}")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<List<MenuInfo>> LoadSubMenus(string appSecret, string parentId)
        {
            var resultInfo = new ResultJsonInfo<List<MenuInfo>>();

            TryCatch(() =>
            {
                resultInfo = functionCfgService.LoadSubMenus(appSecret, parentId);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取左侧菜单信息失败");

            }, $"系统错误，系统主页面管理-获取左侧菜单信息失败");
            return resultInfo;
        }
        #endregion
    }
}