﻿using System.Collections.Generic;
using Api.DevOpsManage.App_Start;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DevOps.Logic;
using Validate.Library;

namespace Api.Manage.Areas.Safe.Controllers
{
    /// <summary>
    /// 服务管理接口
    /// </summary>
    [ApiExplorerSettings(GroupName = "Safe")]
    [Route("api/Safe/[controller]")]
    [ApiController]
    public class ServiceManageController : BaseApiController
    {

        private readonly ISafeServiceInfoService safeServiceInfoService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public ServiceManageController()
        {
            safeServiceInfoService = UnityCIContainer.Instance.GetService<ISafeServiceInfoService>();
        }

        #region 服务基础信息管理

        #region 查询

        /// <summary>
        /// 根据IP地址，服务名称，端口号信息获取服务分页列表信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<ServiceInfoResponse>> LoadPageList([FromBody]ParametersInfo<ServiceInfoQueryRequest> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ServiceInfoResponse>>();

            TryCatch(() =>
            {
                resultInfo = safeServiceInfoService.LoadPageList(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取服务分页列表信息失败");

            }, $"系统错误，服务管理-根据IP地址，服务名称，端口号信息获取服务分页列表信息失败");
            return resultInfo;
        }

        #endregion

        #region 更新功能

        /// <summary>
        /// 更新服务信息
        /// </summary>
        /// <param name="serviceInfo"></param>
        /// <returns></returns>
        [HttpPost("OperateService")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> OperateService([FromBody]ServiceInfoModifyRequest serviceInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                serviceInfo.Validate();
                resultInfo = safeServiceInfoService.OperateService(serviceInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "新增服务信息失败");

            }, $"系统错误，服务管理-新增服务信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = safeServiceInfoService.Remove(ids);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "物理删除消息失败");

            }, $"系统错误，消息管理-物理删除消息失败");
            return resultInfo;
        }


        /// <summary>
        /// 远程操作服务信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet("ControlService/{id}/{type}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> ControlService(string id,int type)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = safeServiceInfoService.ControlService(id, type);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "远程操作服务信息失败");

            }, $"系统错误，服务管理-远程操作服务信息失败");
            return resultInfo;
        }


        /// <summary>
        /// 全面核查
        /// </summary>
        /// <returns></returns>
        [HttpGet("Check")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Check()
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = safeServiceInfoService.Check();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "全面核查失败");

            }, $"系统错误，服务管理-全面核查失败");
            return resultInfo;
        }
        
        #endregion

        #endregion
    }
}