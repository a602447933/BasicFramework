﻿using System.Collections.Generic;
using Api.DevOpsManage.App_Start;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using DevOps.Logic;

namespace Api.Manage.Areas.Safe.Controllers
{
    /// <summary>
    /// 应用接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Safe")]
    [Route("api/Safe/[controller]")]
    [ApiController]
    public class ApplicationInfoController : BaseApiController
    {
        /// <summary>
        /// 应用逻辑实例
        /// </summary>
        public ISafeApplicationInfoService sysApplicationInfo = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public ApplicationInfoController() {

            sysApplicationInfo = UnityCIContainer.Instance.GetService<ISafeApplicationInfoService>();
        }

        #region 应用基础管理

        #region 查询
        /// <summary>
        /// 根据关键字 分页获取应用列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadSysApplicationList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<SafeApplicationInfoResponse>> LoadSysApplicationList(ParametersInfo<string> inputInfo) {
            var resultInfo = new ResultJsonInfo<List<SafeApplicationInfoResponse>>();

            TryCatch(() => {

                resultInfo = sysApplicationInfo.LoadSysApplicationList(inputInfo);

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "根据关键字获取所有的未删除应用信息失败");

            }, $"系统错误，应用管理-根据关键字获取所有的未删除应用信息失败");

            return resultInfo;
        }
        /// <summary>
        /// 获取应用类型列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadApplicationTypeList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public  ResultJsonInfo<List<EnumToSelectItem>> LoadApplicationTypeList() {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() => {

                resultInfo = sysApplicationInfo.LoadApplicationTypeList();

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "获取应用类型列表失败");

            }, $"系统错误，应用管理-获取应用类型列表失败");

            return resultInfo;
        }

        /// <summary>
        /// 获取应用列表下拉框信息
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadSelectList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadSelectList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() =>
            {
                resultInfo = sysApplicationInfo.LoadSelectList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取应用列表下拉框信息失败");

            }, $"系统错误，应用管理-获取应用列表下拉框信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取应用列表Select树状数据
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadTreeList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<SelectListInfo>> LoadTreeList()
        {
            var resultInfo = new ResultJsonInfo<List<SelectListInfo>>();

            TryCatch(() =>
            {
                resultInfo = sysApplicationInfo.LoadTreeList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取应用列表Select树状数据失败");

            }, $"系统错误，应用管理-获取应用列表Select树状数据失败");
            return resultInfo;
        }



        #endregion

        #region 修改
        /// <summary>
        /// 修改应用
        /// </summary>
        /// <returns></returns>
        [HttpPost("ModifySysApplication")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> ModifySysApplication(SafeApplicationInfoModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() => {

                resultInfo = sysApplicationInfo.ModifySysApplication(inputInfo);

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "修改应用信息失败");

            }, $"系统错误，应用管理-修改应用信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 删除应用
        /// </summary>
        /// <returns></returns>
        [HttpPost("DeleteSysApplication")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
       public ResultJsonInfo<int> DeleteSysApplication(List<string> applicationId) {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() => {

                resultInfo = sysApplicationInfo.DeleteSysApplication(applicationId);

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "删除应用信息失败");

            }, $"系统错误，应用管理-删除应用信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 禁用/启用应用
        /// </summary>
        /// <returns></returns>
        [HttpGet("ForbiddenSysApplication/{applicationId}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> ForbiddenSysApplication(string applicationId) {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() => {

                resultInfo = sysApplicationInfo.ForbiddenSysApplication(applicationId);

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "禁用/启用应用信息失败");

            }, $"系统错误，应用管理-禁用/启用应用信息失败");

            return resultInfo;
        }
        #endregion

        #region 添加
        /// <summary>
        /// 新增应用
        /// </summary>
        /// <returns></returns>
        [HttpPost("AddSysApplication")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> AddSysApplication(SafeApplicationInfoAddRequest addRequest) {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() => {

                resultInfo = sysApplicationInfo.AddSysApplication(addRequest);

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "新增应用信息失败");

            }, $"系统错误，应用管理-新增应用信息失败");

            return resultInfo;
        }

        #endregion

        #endregion

    }
}