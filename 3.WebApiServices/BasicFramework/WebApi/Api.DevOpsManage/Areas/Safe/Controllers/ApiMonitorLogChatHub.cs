﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Model;
using Container.Library;
using DevOps.Logic;
using Microsoft.AspNetCore.SignalR;

namespace Api.DevOpsManage.Areas.Safe.Controllers
{
    /// <summary>
    ///接口访问监控日志【继承Hub】
    /// </summary>
    public class ApiMonitorLogChatHub : BaseHub
    {
        private readonly IApiMonitorLogService apiMonitorLogService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ApiMonitorLogChatHub()
        {
            apiMonitorLogService = UnityCIContainer.Instance.GetService<IApiMonitorLogService>();
        }

        /// <summary>
        /// 获取某个时间段内的接口实时访问统计信息
        /// </summary>
        /// <returns></returns>
        public Task InterfaceRequestInfo(DateTime beginTime, DateTime endTime)
        {
            var resultInfo = new ResultJsonInfo<Echarts2DInfo<string, string>>();

            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.loadRequestInfoByTime(beginTime, endTime);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取接口实时访问统计信息失败");

            }, $"系统错误，接口访问监控日志-获取接口实时访问统计信息失败");

            //由连接ID标识的特定客户端
            return Clients.Client(Context.ConnectionId).SendAsync("InterfaceRequestMsg", resultInfo);
        }

        /// <summary>
        /// 获取高频访问接口信息
        /// </summary>
        /// <returns></returns>
        public Task HighFrequencyInterfaceInfo(DateTime beginTime, DateTime endTime)
        {
            var resultInfo = new ResultJsonInfo<Echarts2DInfo<Dictionary<string, object>, string>>();

            TryCatch(() =>
            {
                resultInfo = apiMonitorLogService.LoadHighFrequencyInterface(beginTime, endTime);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取高频访问接口信息失败");

            }, $"系统错误，接口访问监控日志-获取高频访问接口信息失败");

            //由连接ID标识的特定客户端
            return Clients.Client(Context.ConnectionId).SendAsync("HighFrequencyInterfaceMsg", resultInfo);
        }
    }
}