﻿using System.Collections.Generic;
using Api.DevOpsManage.App_Start;
using DevOps.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;
/*
* 命名空间: Api.DevOpsManage.Areas.Authority.Controllers
*
* 功 能： 菜单功能逻辑接口
*
* 类 名： FunctionCfgController
*
* Version   变更日期          负责人       变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/25     14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.DevOpsManage.Areas.Authority.Controllers
{
    /// <summary>
    /// 菜单功能逻辑接口
    /// </summary>
    [ApiExplorerSettings(GroupName = "Authority")]
    [Route("api/Authority/[controller]")]
    [ApiController]
    public class FunctionCfgController : BaseApiController
    {

        IFunctionCfgService functionCfgService = null;
        /// <summary>
        /// 菜单功能逻辑接口构造函数
        /// </summary>
        public FunctionCfgController()
        {
            //菜单功能逻辑注入
            functionCfgService = UnityCIContainer.Instance.GetService<IFunctionCfgService>();
        }

        #region 菜单基础功能管理

        #region 信息查询

        /// <summary>
        /// 根据菜单名称或系统类型获取菜单列表信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadListInfo")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<List<FunctionCfgInfoResponse>> LoadListInfo([FromBody]ParametersInfo<FunctionCfgQueryRequest> queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<FunctionCfgInfoResponse>>();

            TryCatch(() =>
            {
                resultInfo = functionCfgService.LoadListInfo(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据关键字获取所有的未删除菜单信息失败");

            }, $"系统错误，菜单管理-根据关键字获取所有的未删除菜单信息失败");

            return resultInfo;
        }


        /// <summary>
        /// 根据菜单名称或系统类型获取菜单树状列表信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadTreeListInfo")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<List<MenuInfo>> LoadTreeListInfo([FromBody]FunctionCfgQueryRequest queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<MenuInfo>>();

            TryCatch(() =>
            {
                resultInfo = functionCfgService.LoadTreeListInfo(queryInfo);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据菜单名称或系统类型获取菜单树状列表信息失败");

            }, $"系统错误，菜单管理-根据菜单名称或系统类型获取菜单树状列表信息失败");

            return resultInfo;
        }


        #endregion

        #region 更新操作

        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、操作失败，返回 1205，无效操作
        /// </remarks>
        /// <param name="addInfo">根节点信息</param>
        /// <returns></returns>
        [HttpPost("AddInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> AddInfo([FromBody]FunctionCfgAddRequest addInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                addInfo.Validate();
                resultInfo = functionCfgService.AddInfo(addInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "新增节点信息失败");

            }, $"系统错误，菜单功能管理-新增节点信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、无对应值存在、操作失败，返回 1205
        /// </remarks>
        /// <returns></returns>
        [HttpPost("ModifyInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> ModifyInfo([FromBody]FunctionCfgModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                modifyInfo.Validate();
                resultInfo = functionCfgService.Modify(modifyInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "修改节点信息信息失败");

            }, $"系统错误，菜单功能管理-修改节点信息信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 移动顺序操作
        /// </summary>
        /// <param name="moveInfo"></param>
        /// <returns></returns>
        [HttpPost("MoveSort")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> MoveSort([FromBody]FunctionCfgMoveRequest moveInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                moveInfo.Validate();
                resultInfo = functionCfgService.MoveSort(moveInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "移动顺序操作失败");

            }, $"系统错误，菜单功能管理-移动顺序操作失败");

            return resultInfo;
        }

        /// <summary>
        /// 菜单启用与停用操作
        /// </summary>
        /// <param name="id">菜单唯一标识符</param>
        /// <returns></returns>
        [HttpGet("ForbidOrEnable/{id}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> ForbidOrEnable(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                resultInfo = functionCfgService.ForbiddenInfo(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "启用与停用操作失败");

            }, $"系统错误，菜单功能管理-启用与停用操作失败");

            return resultInfo;
        }

        /// <summary>
        /// 删除节点
        /// </summary>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(string[] ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                resultInfo = functionCfgService.Remove(ids);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "删除节点失败");

            }, $"系统错误，菜单功能管理-删除节点操作失败");

            return resultInfo;
        }

        #endregion

        #endregion

        #region 与权限相关功能

        /// <summary>
        /// 根据角色ID，获取所有可操作菜单已赋权限情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadListInfoByRoleId")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<List<FunctionCfgRoleMenuResponse>> LoadListInfoByRoleId([FromBody]FunctionCfgRoleQuery queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<FunctionCfgRoleMenuResponse>>();

            TryCatch(() =>
            {
                resultInfo = functionCfgService.LoadListInfoByRoleId(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据角色ID，获取所有可操作菜单已赋权限情况信息失败");

            }, $"系统错误，菜单管理-根据角色ID，获取所有可操作菜单已赋权限情况信息失败");

            return resultInfo;
        }


        /// <summary>
        /// 根据角色ID，获取所有可操作菜单已赋权限情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadTreeListByRoleId")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<List<MenuTreeInfo>> LoadTreeListByRoleId([FromBody]FunctionCfgRoleQuery queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<MenuTreeInfo>>();

            TryCatch(() =>
            {
                resultInfo = functionCfgService.LoadTreeListByRoleId(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据角色ID，获取所有可操作菜单已赋权限情况信息失败");

            }, $"系统错误，菜单管理-根据角色ID，获取所有可操作菜单已赋权限情况信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 修改对应角色赋值的菜单操作权限
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        [HttpPost("ModifyRoleMenuInfo")]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [Authorize]
        public ResultJsonInfo<int> ModifyRoleMenuInfo([FromBody]FunctionCfgRoleModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = functionCfgService.ModifyRoleMenuInfo(modifyInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "修改对应角色赋值的菜单操作权限信息失败");

            }, $"系统错误，菜单管理-修改对应角色赋值的菜单操作权限信息失败");

            return resultInfo;
        }
        

        #endregion

        #region 公用功能

        /// <summary>
        /// 获取功能类型Select列表信息
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        [HttpGet("LoadFunctionTypeList")]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadFunctionTypeList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            TryCatch(() =>
            {
                resultInfo = functionCfgService.LoadFunctionTypeList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取功能类型Select列表信息失败");

            }, $"系统错误，菜单基础功能管理-获取功能类型Select列表信息失败");


            return resultInfo;
        }

        #endregion
    }
}