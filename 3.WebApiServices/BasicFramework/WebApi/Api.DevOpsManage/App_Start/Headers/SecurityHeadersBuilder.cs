﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/*
* 命名空间: Api.DevOpsManage.App_Start.Headers
*
* 功 能： 响应头的增删方法
*
* 类 名： SecurityHeadersBuilder
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/15 10:41:59 				Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Api.DevOpsManage.App_Start
{
    /// <summary>
    /// 响应头的增删方法
    /// </summary>
    public class SecurityHeadersBuilder
    {
        private readonly SecurityHeadersPolicy _policy = new SecurityHeadersPolicy();
        /// <summary>
        /// 增加客户端头部
        /// </summary>
        /// <param name="header"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public SecurityHeadersBuilder AddCustomHeader(string header, string value)
        {
            _policy.SetHeaders[header] = value;
            return this;
        }
        /// <summary>
        /// 删除头部
        /// </summary>
        /// <param name="header"></param>
        /// <returns></returns>
        public SecurityHeadersBuilder RemoveHeader(string header)
        {
            _policy.RemoveHeaders.Add(header);
            return this;
        }
        /// <summary>
        /// 构造
        /// </summary>
        /// <returns></returns>
        public SecurityHeadersPolicy Build()
        {
            return _policy;
        }
    }
}
