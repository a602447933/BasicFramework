﻿using Common.Library;
using Common.Model;
using Microsoft.AspNetCore.Mvc;
using Redis.Library;
using System;

namespace Api.DevOpsManage.App_Start
{
    /// <summary>
    /// API控制器基础类
    /// </summary>
    public class BaseApiController : ControllerBase
    {
        /// <summary>
        /// try catch 方法
        /// </summary>
        /// <param name="action"></param>
        /// <param name="resoveExceptionAction"></param>
        /// <param name="message"></param>
        protected void TryCatch(Action action, Action<Exception> resoveExceptionAction = null, string message = "程序出现错误")
        {
            Try.CatchLog(action, ex =>
            {
                if (ex.GetType().Name != "ValidateException")
                {

                    RedisClient.GetExceptionRedisDb().SaveErrorLog(ServiceName.DevOpsService.GetEnumItemDescription(), message, ex);
                }
                else
                {
                    RedisClient.GetExceptionRedisDb().SaveWarnLog(ServiceName.DevOpsService.GetEnumItemDescription(), message, ex);
                }
                resoveExceptionAction?.Invoke(ex);

            }, message);
        }
    }
}
