﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
/*
* 命名空间: Api.MallManage.Areas.Client.Controllers
*
* 功 能： 购物车接口控制器
*
* 类 名： CartController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/11/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.MallManage.Areas.Goods.Controllers
{
    /// <summary>
    /// 购物车接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Goods")]
    [Route("api/Goods/[controller]")]
    [ApiController]
    public class CartController : BaseApiController
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public CartController()
        {
        }

    }
}