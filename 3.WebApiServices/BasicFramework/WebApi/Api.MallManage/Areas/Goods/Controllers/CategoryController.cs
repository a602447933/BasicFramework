﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Library;
using Common.Model;
using Container.Library;
using Mall.Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;

/*
* 命名空间: Api.MallManage.Areas.Client.Controllers
*
* 功 能： 商品分类接口控制器
*
* 类 名： CategoryController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/11/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.MallManage.Areas.Goods.Controllers
{
    /// <summary>
    /// 商品分类接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Goods")]
    [Route("api/Goods/[controller]")]
    [ApiController]
    public class CategoryController : BaseApiController
    {

        IGoodsCategoryService goodsCategoryService = null;
        /// <summary>
        /// 部门接口控制器构造函数
        /// </summary>
        public CategoryController()
        {
            //商品分类管理逻辑注入
            goodsCategoryService = UnityCIContainer.Instance.GetService<IGoodsCategoryService>();
        }


        #region 商品分类基础信息管理模块

        #region 信息查询

        /// <summary>
        /// 获取所有商品分类树状信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadAllTreeList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<TreeInfo>> LoadAllTreeList()
        {
            var resultInfo = new ResultJsonInfo<List<TreeInfo>>();

            TryCatch(() => {

                resultInfo = goodsCategoryService.LoadAllTreeList();

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "获取所有商品分类树状信息失败");

            }, $"系统错误，商品分类管理-获取所有商品分类树状信息失败");

            return resultInfo;
        }


        /// <summary>
        /// 根据关键字获取所有的商品分类树状信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、无对应值存在，返回 1205
        /// </remarks>
        /// <param name="inputInfo">inputInfo.parameters关键字</param>
        /// <returns></returns>
        [HttpPost("LoadTreeList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<List<GoodsCategoryTreeInfo>> LoadTreeList([FromBody]ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<GoodsCategoryTreeInfo>>();

            TryCatch(() => {

                resultInfo = goodsCategoryService.LoadTreeList(inputInfo.parameters);

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "根据关键字获取所有的商品分类树状信息失败");

            }, $"系统错误，商品分类管理-根据关键字获取所有的商品分类树状信息失败");

            return resultInfo;
        }

        #endregion

        #region 更新操作

        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、操作失败、无对应父节点信息，返回 1205
        /// </remarks>
        /// <param name="addInfo">子节点信息</param>
        /// <returns></returns>
        [HttpPost("Addnode")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> Addnode([FromBody]GoodsCategoryAddRequest addInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                addInfo.Validate();
                resultInfo = goodsCategoryService.Addnode(addInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "新增根节点信息失败");

            }, $"系统错误，商品分类管理-新增根节点信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、无对应值存在、操作失败，返回 1205
        /// </remarks>
        /// <returns></returns>
        [HttpPost("ModifyInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> ModifyInfo([FromBody]GoodsCategoryModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                modifyInfo.Validate();
                resultInfo = goodsCategoryService.Modify(modifyInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "修改节点信息信息失败");

            }, $"系统错误，商品分类管理-修改节点信息信息失败");

            return resultInfo;
        }

        #endregion

        #region 删除信息

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack,true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = goodsCategoryService.Remove(ids);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "删除信息操作失败");

            }, $"系统错误，商品分类管理-删除信息操作失败");

            return resultInfo;
        }
        #endregion

        #region 移动顺序操作

        /// <summary>
        /// 移动顺序操作（1：上移/2：下移）
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// </remarks>
        /// <returns></returns>
        [HttpPost("Move")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> Move([FromBody]GoodsCategoryMoveRequest moveInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                moveInfo.Validate();
                resultInfo = goodsCategoryService.Move(moveInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "移动顺序操作失败");

            }, $"系统错误，商品分类管理-移动顺序操作失败");

            return resultInfo;
        }

        #endregion

        #endregion

    }
}