﻿using System.Collections.Generic;
using Common.Model;
using Container.Library;
using Mall.Logic;
using Mall.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;

/*
* 命名空间: Api.MallManage.Areas.Client.Controllers
*
* 功 能： 商品接口控制器
*
* 类 名： InfoController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/11/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.MallManage.Areas.Goods.Controllers
{
    /// <summary>
    /// 商品接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Goods")]
    [Route("api/Goods/[controller]")]
    [ApiController]
    public class GoodsInfoController : BaseApiController
    {

        private readonly IGoodsInfoService goodsInfoService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public GoodsInfoController()
        {
            goodsInfoService = UnityCIContainer.Instance.GetService<IGoodsInfoService>();
        }

        #region 商品管理相关逻辑

        #region 查询操作

        /// <summary>
        /// 获取商品分页列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<GoodsInfoResponse>> LoadList([FromBody]ParametersInfo<GoodsInfoQuery> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<GoodsInfoResponse>>();

            TryCatch(() =>
            {
                resultInfo = goodsInfoService.LoadPageList(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取分页列表失败");

            }, $"系统错误，商品管理-获取分页列表失败");

            return resultInfo;
        }

        /// <summary>
        /// 根据商品id获取附件文件
        /// </summary>
        /// <param name="goodsId"></param>
        /// <returns></returns>
        [HttpGet("LoadAttachList/{goodsId}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<GoodsPictureEntity>> LoadAttachList(string goodsId)
        {
            var resultInfo = new ResultJsonInfo<List<GoodsPictureEntity>>();

            TryCatch(() =>
            {
                resultInfo = goodsInfoService.LoadAttachList(goodsId);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据商品id获取附件文件失败");

            }, $"系统错误，商品管理-根据商品id获取附件文件失败");

            return resultInfo;
        }


        /// <summary>
        /// 根据id获取对应的信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("LoadSingleById/{id}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<GoodsInfoResponse> LoadSingleById(string id)
        {
            var resultInfo = new ResultJsonInfo<GoodsInfoResponse>();

            TryCatch(() =>
            {
                resultInfo = goodsInfoService.LoadSingleById(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据id获取对应的信息失败");

            }, $"系统错误，商品管理-根据id获取对应的信息失败");

            return resultInfo;
        }



        /// <summary>
        /// 根据id获取对应的详情信息【基本信息+附件】
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("LoadDetailById/{id}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<GoodsDetailResponse> LoadDetailById(string id)
        {
            var resultInfo = new ResultJsonInfo<GoodsDetailResponse>();

            TryCatch(() =>
            {
                resultInfo = goodsInfoService.LoadDetailById(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "根据id获取对应的详情信息失败");

            }, $"系统错误，商品管理-根据id获取对应的详情信息失败");

            return resultInfo;
        }

        #endregion

        #region 更新操作

        /// <summary>
        /// 更新商品信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("SaveInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> SaveInfo([FromBody]GoodsInfoModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = goodsInfoService.SaveInfo(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "更新商品信息失败");

            }, $"系统错误，商品管理-更新商品信息失败");
            return resultInfo;
        }


        /// <summary>
        /// 是否开启 热卖  推荐  特价  上架 出现的
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet("EnableInfo/{id}/{type}")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> EnableInfo(string id,int type)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {
                resultInfo = goodsInfoService.EnableInfo(id,type);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "操作失败");

            }, $"系统错误，商品管理-操作失败");
            return resultInfo;
        }


        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            TryCatch(() =>
            {

                resultInfo = goodsInfoService.Remove(ids);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "物理删除商品失败");

            }, $"系统错误，商品管理-物理商品消息失败");
            return resultInfo;
        }

        #endregion

        #endregion
    }
}