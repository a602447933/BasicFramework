﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Model;
using Container.Library;
using Mall.Logic;
using Mall.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;


/*
* 命名空间: Api.MallManage.Areas.Client.Controllers
*
* 功 能： 客户地址管理接口控制器
*
* 类 名： ClientBalanceController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/11/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Api.MallManage.Areas.Client.Controllers
{

    /// <summary>
    /// 客户金额变动明细信息控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Client")]
    [Route("api/Client/[controller]")]
    [ApiController]
    public class ClientBalanceController : BaseApiController
    {

        private readonly IClientBalanceService clientbalanceService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public ClientBalanceController()
        {
            clientbalanceService = UnityCIContainer.Instance.GetService<IClientBalanceService>();
        }

        #region 查询

        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<ClientBalanceDetailEntity>> LoadPageList([FromBody]ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ClientBalanceDetailEntity>>();
            TryCatch(() =>
            {
                resultInfo = clientbalanceService.LoadPageList(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取分页列表失败");

            }, $"系统错误，客户金额变动明细管理-获取分页列表失败");
            return resultInfo;
        }

        #endregion

        #region 更新

        /// <summary>
        /// 更新客户金额变动明细
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("SaveInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> SaveInfo([FromBody]ClientBalanceModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = clientbalanceService.SaveInfo(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "更新客户金额变动明细失败");

            }, $"系统错误，客户金额变动明细管理-更新客户金额变动明细失败");
            return resultInfo;
        }

        #endregion


        #region 删除

        /// <summary>
        /// 删除客户金额变动明细
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                resultInfo = clientbalanceService.Remove(ids);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "删除客户金额变动明细失败");

            }, $"系统错误，客户金额变动明细管理-删除客户金额变动明细失败");

            return resultInfo;
        }
        #endregion


    }
}