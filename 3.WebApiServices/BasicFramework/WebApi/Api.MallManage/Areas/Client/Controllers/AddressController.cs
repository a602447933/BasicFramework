﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Model;
using Container.Library;
using Mall.Logic;
using Mall.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;

/*
* 命名空间: Api.MallManage.Areas.Client.Controllers
*
* 功 能： 客户地址管理接口控制器
*
* 类 名： AddressController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/11/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.MallManage.Areas.Client.Controllers
{
    /// <summary>
    /// 客户地址管理接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Client")]
    [Route("api/Client/[controller]")]
    [ApiController]
    public class AddressController : BaseApiController
    {

        private readonly IClientAddressService clientaddressService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public AddressController()
        {
            clientaddressService = UnityCIContainer.Instance.GetService<IClientAddressService>();
        }

        #region 查询

        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<ClientAddressInfoEntity>> LoadPageList([FromBody]ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ClientAddressInfoEntity>>();
            TryCatch(() =>
            {
                resultInfo = clientaddressService.LoadPageList(inputInfo);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取分页列表失败");

            }, $"系统错误，用户地址管理-获取分页列表失败");

            return resultInfo;
        }

        #endregion

        #region 更新

        /// <summary>
        /// 更新用户地址信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("SaveInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> SaveInfo([FromBody]ClientAddressModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = clientaddressService.SaveInfo(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "更新用户地址信息失败");

            }, $"系统错误，用户地址管理-更新用户地址信息失败");
            return resultInfo;
        }

        #endregion

        #region 删除

        /// <summary>
        /// 删除用户地址信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                resultInfo = clientaddressService.Remove(ids);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "删除用户地址信息失败");

            }, $"系统错误，用户地址管理-删除用户地址信息失败");

            return resultInfo;
        }

        #endregion
    }
}