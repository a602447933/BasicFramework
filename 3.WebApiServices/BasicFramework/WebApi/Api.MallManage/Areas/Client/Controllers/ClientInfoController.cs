﻿using Common.Model;
using Container.Library;
using Mall.Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Validate.Library;
/*
* 命名空间: Api.MallManage.Areas.Client.Controllers
*
* 功 能： 客户管理接口控制器
*
* 类 名： ClientInfoController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/11/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Api.MallManage.Areas.Client.Controllers
{
    /// <summary>
    /// 客户管理接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Client")]
    [Route("api/Client/[controller]")]
    [ApiController]
    public class ClientInfoController : BaseApiController
    {
        private readonly IClientInfoService clientinfoService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public ClientInfoController() {

            clientinfoService = UnityCIContainer.Instance.GetService<IClientInfoService>();
        }

        #region 查询

        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<List<ClientInfoResponse>> LoadPageList([FromBody]ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ClientInfoResponse>>();

            TryCatch(() =>
            {
                resultInfo = clientinfoService.LoadPageList(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取分页列表失败");

            }, $"系统错误，客户管理-获取分页列表失败");
            return resultInfo;
        }
        #endregion

        #region 更新

        /// <summary>
        /// 更新客户信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("SaveInfo")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> SaveInfo([FromBody]ClientInfoModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = clientinfoService.SaveInfo(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "更新客户信息失败");

            }, $"系统错误，客户管理-更新客户信息失败");
            return resultInfo;
        }
        #endregion

        #region 删除

        /// <summary>
        /// 删除客户信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [ClientApiFilter(DeviceType.PCBack, true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            TryCatch(() =>
            {
                resultInfo = clientinfoService.Remove(ids);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "物理删除客户失败");

            }, $"系统错误，客户管理-物理客户消息失败");

            return resultInfo;
        }

        #endregion
    }
}