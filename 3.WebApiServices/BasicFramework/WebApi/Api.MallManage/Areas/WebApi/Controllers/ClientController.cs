﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Model;
using Container.Library;
using Mall.Logic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Serialize.Library;
using Validate.Library;

namespace Api.MallManage.Areas.WebApi.Controllers
{
    /// <summary>
    /// 移动端登陆逻辑接口
    /// </summary>
    [ApiExplorerSettings(GroupName = "Client")]
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : BaseApiController
    {

        private readonly IClientInfoService clientInfoService = null;
        private readonly IJwtFactory jwtFactoryObj = null;
        private readonly JwtIssuerOptions jwtOptionsObj = null;

        /// <summary>
        /// 登录相关接口控制器构造函数
        /// </summary>
        public ClientController(IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions)
        {
            //前端用户管理逻辑注入
            clientInfoService = UnityCIContainer.Instance.GetService<IClientInfoService>();
            //Jwt操作逻辑
            jwtFactoryObj = jwtFactory;
            //Jwt配置信息
            jwtOptionsObj = jwtOptions.Value;
        }

        #region 登录

        /// <summary>
        /// 微信自动授权登录【需增加时间戳】
        /// </summary>
        /// <param name="inputInfo">微信账号信息</param>
        /// <returns></returns>
        [HttpPost("LoginCheck")]
        [ClientApiFilter(DeviceType.WeChat, false)]
        public ResultJsonInfo<WechatUserLoginInfo> LoginCheck([FromBody]ClientLoginRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<WechatUserLoginInfo>();

            TryCatch(() =>
            {
                inputInfo.Validate();
                resultInfo = clientInfoService.LoginCheck(inputInfo);
                if (resultInfo.Code == ActionCodes.Success)
                {
                    var userInfo = JsonHelper.ToJson(resultInfo.Data).JsonToObject<Dictionary<string, object>>();
                    var claimsIdentity = jwtFactoryObj.GenerateClaimsIdentity(userInfo);
                    var token = jwtFactoryObj.GenerateEncodedToken(resultInfo.Data.name, resultInfo.Data.token, claimsIdentity).GetAwaiter().GetResult();
                    resultInfo.Data.token = token;
                }

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "微信自动授权登录失败");

            }, $"系统错误，用户管理-微信自动授权登录失败");
            return resultInfo;
        }

        #endregion

        #region 查询

        /// <summary>
        /// 通过微信openId获取用户信息【需要授权信息，需增加时间戳】
        /// </summary>
        /// <param name="openId">微信openId</param>
        /// <returns>返回用户信息</returns>
        [HttpGet("LoadByOpenId/{openId}")]
        [ClientApiFilter(DeviceType.WeChat, true)]
        public ResultJsonInfo<WechatUserLoginInfo> LoadByOpenId(string openId)
        {
            var resultInfo = new ResultJsonInfo<WechatUserLoginInfo>();

            TryCatch(() =>
            {
                resultInfo = clientInfoService.LoadByOpenId(openId);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "通过微信openId获取用户信息失败");

            }, $"系统错误，用户管理-通过微信openId获取用户信息失败");
            return resultInfo;
        }

        #endregion

        #region 更新

        #endregion

    }
}