﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 图片上传请求实体
    /// </summary>
    public class ImageUploadInfo
    {
        /// <summary>
        /// 服务名称文件夹路径
        /// </summary>
        public ServiceName servicePath
        {
            get; set;
        }

        /// <summary>
        /// 文件夹名称【不可空】
        /// </summary>
        public string uploadFolder
        {
            get; set;
        }

        /// <summary>
        /// 原图片名称【可空】
        /// </summary>
        public string originalName
        {
            get; set;
        }

        /// <summary>
        /// 图片base64信息【不可空】
        /// </summary>
        public string imageBase64
        {
            get; set;
        }

    }
}
