﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 数据库实例名
    /// </summary>
    public enum DatabaseName
    {
        ///// <summary>
        /////基础数据库-Sql_Oracle_DB
        ///// </summary>
        //[Description("Sql_Oracle_DB")]
        //Sql_Oracle_DB = 100,

        /// <summary>
        ///业务数据库
        /// </summary>
        [Description("Sql_BUS_DB")]
        Sql_BUS_DB = 110,

        /// <summary>
        ///空间数据库
        /// </summary>
        [Description("Sql_SDE_DB")]
        Sql_SDE_DB = 120,
    }
}
