﻿/*
* 命名空间: Common.Model
*
* 功 能： 服务名称枚举
*
* 类 名： ServiceName
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/14 14:13:23 	Harvey    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 服务名称枚举
    /// </summary>
    public enum ServiceName
    {
        /// <summary>
        /// 运维管理服务
        /// </summary>
        [Description("运维管理服务")]
        DevOpsService = 100,

        /// <summary>
        /// 文件管理服务
        /// </summary>
        [Description("文件管理服务")]
        FilesService = 110,

        /// <summary>
        /// 管网管理服务
        /// </summary>
        [Description("管网管理服务")]
        OfficialService = 120,

        /// <summary>
        ///商城管理服务
        /// </summary>
        [Description("商城管理服务")]
        MallService = 130,
    }
}
