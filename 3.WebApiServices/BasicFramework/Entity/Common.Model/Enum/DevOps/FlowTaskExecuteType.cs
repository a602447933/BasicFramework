﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 流程任务处理类型
    /// 处理类型 100等待中 110未处理 120处理中 130已完成 140已退回 150他人已处理 160他人已退回 170已转交 180已委托 190已阅知
    /// </summary>
    public enum FlowTaskExecuteType
    {
        /// <summary>
        /// 100等待中
        /// </summary>
        [Description("等待中")]
        Waiting = 100,

        /// <summary>
        /// 110未处理
        /// </summary>
        [Description("未处理")]
        Untreated = 110,

        /// <summary>
        /// 120处理中
        /// </summary>
        [Description("处理中")]
        Processing = 120,

        /// <summary>
        /// 130已完成
        /// </summary>
        [Description("已完成")]
        Completed = 130,

        /// <summary>
        /// 140已退回
        /// </summary>
        [Description("已退回")]
        Returned = 140,

        ///// <summary>
        ///// 150他人已处理
        ///// </summary>
        //[Description("他人已处理")]
        //DealtByOthers = 150,

        ///// <summary>
        ///// 160他人已退回
        ///// </summary>
        //[Description("他人已退回")]
        //ReturnedByOthers = 160,

        /// <summary>
        /// 170已转交
        /// </summary>
        [Description("已转交")]
        PassOn = 170,

        /// <summary>
        /// 180已委托
        /// </summary>
        [Description("已委托")]
        Commissioned = 180,

        /// <summary>
        /// 190已阅知
        /// </summary>
        [Description("已阅知")]
        HasPassed = 190,
    }
}
