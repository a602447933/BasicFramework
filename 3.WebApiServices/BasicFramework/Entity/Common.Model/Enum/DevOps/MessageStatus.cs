﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;


/*
* 命名空间: Common.Model
*
* 功 能： 消息状态  100-未读，101-已读
*
* 类 名： MessageStatus
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/8 10:52:04 	Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 消息状态
    /// </summary>
    public enum MessageStatus
    {
        /// <summary>
        /// 未读
        /// </summary>
        [Description("未读")]
        Unread = 100,

        /// <summary>
        /// 已读
        /// </summary>
        [Description("已读")]
        AlreadyRead = 101,
    }
}
