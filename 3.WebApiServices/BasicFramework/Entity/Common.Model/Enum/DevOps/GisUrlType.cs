﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// GIS网络类型
    /// </summary>
    public enum GisUrlType
    {
        /// <summary>
        ///WMS
        /// </summary>
        [Description("WMS")]
        WMS = 100,

        /// <summary>
        ///WFS
        /// </summary>
        [Description("WFS")]
        WFS = 110,

        /// <summary>
        ///WLS
        /// </summary>
        [Description("WLS")]
        WLS = 120,

        /// <summary>
        ///MMD
        /// </summary>
        [Description("MMD")]
        MMD = 130,

        /// <summary>
        ///MTS
        /// </summary>
        [Description("MTS")]
        MTS = 140,
    }
}
