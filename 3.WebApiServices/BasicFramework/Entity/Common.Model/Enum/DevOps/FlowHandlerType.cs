﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 流程处理人类型
    /// </summary>
    public enum FlowHandlerType
    {
        /// <summary>
        /// 所有人员
        /// </summary>
        [Description("所有人员")]
        AllPersonnel = 100,

        /// <summary>
        /// 选择的人员
        /// </summary>
        [Description("选择的人员")]
        SelectPersonnel = 110,

        /// <summary>
        /// 发起者部门领导
        /// </summary>
        [Description("发起者部门领导")]
        DepartmentHead = 120,

        /// <summary>
        /// 发起者上级部门领导
        /// </summary>
        [Description("发起者上级部门领导")]
        SuperiorHead = 130,

        /// <summary>
        /// 发起者所有领导
        /// </summary>
        [Description("发起者所有领导")]
        AllHead = 140,

    }
}
