﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 流程退回类型
    /// </summary>
    public enum FlowReturnType
    {
        //退回类型100退回上一步 110退回第一步 120退回指定步骤
        /// <summary>
        /// 退回上一步
        /// </summary>
        [Description("退回上一步")]
        BackLastStep = 100,

        /// <summary>
        /// 退回第一步
        /// </summary>
        [Description("退回第一步")]
        BackFirstStep = 110,

        /// <summary>
        /// 退回指定步骤
        /// </summary>
        [Description("退回指定步骤")]
        BackSpecifiedStep = 120,
    }
}
