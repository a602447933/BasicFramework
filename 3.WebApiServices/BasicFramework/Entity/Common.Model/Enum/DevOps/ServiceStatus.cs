﻿/*
* 命名空间: Common.Model
*
* 功 能： 服务状态
*
* 类 名： ServiceStatus
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/9 14:40:48 	Harvey    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 服务状态【无状态0，开始或运行中100，停止200，删除或卸载300】
    /// </summary>
    public enum ServiceStatus
    {
        /// <summary>
        /// 无状态
        /// </summary>
        [Description("无状态")]
        Stateless = 0,

        /// <summary>
        /// 已安装，未运行
        /// </summary>
        [Description("已安装，未运行")]
        NotRunning = 10,
     
        /// <summary>
        /// 运行中
        /// </summary>
        [Description("运行中")]
        Running = 100,

        /// <summary>
        /// 停止
        /// </summary>
        [Description("停止")]
        Stop = 200,

        /// <summary>
        /// 已卸载
        /// </summary>
        [Description("已卸载")]
        Uninstalled = 300
    }
}
