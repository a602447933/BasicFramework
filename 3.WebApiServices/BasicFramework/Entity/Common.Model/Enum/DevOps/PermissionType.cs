﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 权限类别
    /// </summary>
    public enum PermissionType
    {
        /// <summary>
        /// 100图层 
        /// </summary>
        [Description("图层")]
        Layertree = 100,

        /// <summary>
        /// 200功能
        /// </summary>
        [Description("功能")]
        Function = 200,
    }
}
