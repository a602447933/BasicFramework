﻿using System.ComponentModel;

/*
* 命名空间: Common.Model
*
* 功 能： 信息类型 100：对话信息 101：系统消息:
*
* 类 名： MessageType
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/8 10:49:00 	Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 信息类型
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// 对话信息
        /// </summary>
        [Description("对话信息")]
        DialogueInfo = 100,

        /// <summary>
        /// 系统消息
        /// </summary>
        [Description("系统消息")]
        SystemInfo = 101,

    }
}
