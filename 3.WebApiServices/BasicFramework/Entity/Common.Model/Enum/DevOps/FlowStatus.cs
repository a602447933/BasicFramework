﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 流程状态
    /// </summary>
    public enum FlowStatus
    {
        //状态 100设计中 110已安装 120已卸载 130已删除

        /// <summary>
        /// 100设计中
        /// </summary>
        [Description("设计中")]
        UnderDesign = 100,

        /// <summary>
        /// 110已安装
        /// </summary>
        [Description("已安装")]
        Installed = 110,

        /// <summary>
        /// 120已卸载
        /// </summary>
        [Description("已卸载")]
        Unloaded = 120,

        /// <summary>
        /// 130已删除
        /// </summary>
        [Description("已删除")]
        Deleted = 130,

    }
}