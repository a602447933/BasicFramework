﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

/*
* 命名空间:  Common.Model
*
* 功 能：流程状态枚举
*
* 类 名： Flow
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/19 9:32:12               Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 流程状态枚举
    /// </summary>
    public enum FlowType
    {
        /// <summary>
        /// 设计中
        /// </summary>
        [Description("设计中")]
        UnderDesign = 100,
        /// <summary>
        /// 已安装
        /// </summary>
        [Description("已安装")]
        Installed =110,

        /// <summary>
        /// 已卸载
        /// </summary>
        [Description("已卸载")]
        Unloaded = 120,

        /// <summary>
        /// 已删除
        /// </summary>
        [Description("已删除")]
        HaveDeleted = 130,
    }
}
