﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 系统异常类型
    /// </summary>
    public enum ExceptionLogType
    {
        /// <summary>
        /// 100：警告
        /// </summary>
        [Description("警告")]
        Warning = 100,

        /// <summary>
        /// 101：严重警告
        /// </summary>
        [Description("严重警告")]
        SeriousWarning = 101,

        /// <summary>
        /// 102：错误
        /// </summary>
        [Description("错误")]
        Error = 102,

        /// <summary>
        /// 103：致命异常信息
        /// </summary>
        [Description("致命异常信息")]
        Fatal = 103,
        
    }
}
