﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 流程会签类型
    /// </summary>
    public enum FlowSignStrategy
    {
        //100不会签 110所有步骤同意 120一个步骤同意即可 130依据比例
        /// <summary>
        /// 100不会签
        /// </summary>
        [Description("不会签")]
        NotSign = 100,

        /// <summary>
        /// 110所有步骤同意
        /// </summary>
        [Description("所有步骤同意")]
        AllStepsAgree = 110,

        /// <summary>
        /// 120一个步骤同意即可
        /// </summary>
        [Description("一个步骤同意即可")]
        OneStepAgree = 120,

        /// <summary>
        /// 130依据比例
        /// </summary>
        [Description("依据比例")]
        Wahlbeteiligung = 130,
    }
}
