﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间: Common.Model
*
* 功 能：钉钉返回的部门信息
*
* 类 名：DindinDepartmentList
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/8 16:49:18       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 钉钉返回的部门信息
    /// </summary>
    public class DingDepartmentListResponse
    {
        /// <summary>
        /// department
        /// </summary>
        public List<DepartmentDomain> department { get; set; }

        /// <summary>
        /// errcode
        /// </summary>
        public long errcode { get; set; }

        /// <summary>
        /// errmsg
        /// </summary>
        public string errmsg { get; set; }
    }

    /// <summary>
    /// 部门信息
    /// </summary>
    public class DepartmentDomain
    {
        /// <summary>
        /// 部门ID
        /// </summary>
        public long id
        {
            get; set;
        }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 父部门ID，1为根部门。
        /// </summary>
        public long parentid
        {
            get; set;
        }

        /// <summary>
        /// 是否创建一个关联此部门的企业群，默认为false。
        /// </summary>
        public bool createDeptGroup
        {
            get; set;
        }

        /// <summary>
        /// 当群已经创建后，是否有新人加入部门时会自动加入该群：
        /// true：自动加入群
        /// false：不会自动加入群
        /// 示例
        /// </summary>
        public bool autoAddUser
        {
            get; set;
        }
    }
}
