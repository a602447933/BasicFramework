﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：钉钉用户状态信息
*
* 类 名：DingEmployeeStatusResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/4/9 16:43:30       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 钉钉用户状态信息
    /// </summary>
    public class DingEmployeeStatusResponse
    {
        /// <summary>
        /// 员工的userid
        /// </summary>
        public string userid { get; set; }

        /// <summary>
        /// 员工的状态 1：待离职 2：已离职 3：未离职
        /// </summary>
        public int status { get; set; }
    }
}
