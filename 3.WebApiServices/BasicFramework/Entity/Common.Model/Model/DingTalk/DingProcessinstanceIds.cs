﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：分页获取审批实例id列表
*
* 类 名：DingProcessinstanceIds
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/15 10:33:41       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 分页获取审批实例id列表
    /// </summary>
    public class DingProcessinstanceIds
    {
        /// <summary>
        /// 下一页分页开始游标
        /// </summary>
        public int next_cursor { get; set; }

        /// <summary>
        /// 审批实例id列表
        /// </summary>
        public List<string> list { get; set; }
    }
}
