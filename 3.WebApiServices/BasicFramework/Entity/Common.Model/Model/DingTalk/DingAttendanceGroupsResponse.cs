﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:Common.Model
*
* 功 能：钉钉考勤组信息
*
* 类 名：DingAttendanceGroupsResponse
*
* Version   变更日期            负责人  变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/10 17:58:02   LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 钉钉考勤组信息
    /// </summary>
    public class DingAttendanceGroupsResponse
    {
        ///<summary>
        /// result结果
        ///</summary>
        public DingAttendanceGroups result { get; set; }

        /// <summary>
        /// errcode
        /// </summary>
        public long errcode { get; set; }

        /// <summary>
        /// success
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// 请求ID
        /// </summary>
        public string request_id { get; set; }
    }


    /// <summary>
    /// 钉钉分组内部信息
    /// </summary>
    public class DingAttendanceGroups {

        /// <summary>
        /// result
        /// </summary>
        public List<DingAttendanceGroupItem> result { get; set; }

        /// <summary>
        /// cursor
        /// </summary>
        public long cursor { get; set; }

        /// <summary>
        /// 是否还有更多
        /// </summary>
        public bool has_more { get; set; }

    }


    /// <summary>
    /// 钉钉分组内部信息
    /// </summary>
    public class DingAttendanceGroupItem
    {
        /// <summary>
        /// 分组Id
        /// </summary>
        public long id { get; set; }

        /// <summary>
        /// 分组名称
        /// </summary>
        public string  name { get; set; }

    }
}
