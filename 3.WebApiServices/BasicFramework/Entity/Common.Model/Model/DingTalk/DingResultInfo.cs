﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：钉钉通用接口调用返回信息
*
* 类 名：DingResultInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/9 11:36:11       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 钉钉通用接口调用返回信息
    /// </summary>
    public class DingResultInfo<T>
    {
        /// <summary>
        /// result
        /// </summary>
        public T result { get; set; }

        /// <summary>
        /// errcode
        /// </summary>
        public long errcode { get; set; }

        /// <summary>
        /// errmsg
        /// </summary>
        public string errmsg { get; set; }

        /// <summary>
        /// 请求ID
        /// </summary>
        public string request_id { get; set; }
    }
}
