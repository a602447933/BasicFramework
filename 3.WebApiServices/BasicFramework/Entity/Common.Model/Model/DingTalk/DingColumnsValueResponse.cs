﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：钉钉列对应值返回信息
*
* 类 名：DingColumnsValueResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/11 17:01:02       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 钉钉列对应值返回信息
    /// </summary>
    public class DingColumnsValueResponse
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public string userid { get; set; }

        /// <summary>
        /// 列信息与列值数据
        /// </summary>
        public List<ColumnValForTopVo> column_vals { get; set; }

    }

    /// <summary>
    /// 列信息与列值数据
    /// </summary>
    public class ColumnValForTopVo
    {
        /// <summary>
        /// 列值数据
        /// </summary>
        public List<ColumnDayAndVal> column_vals { get; set; }

        /// <summary>
        /// 列信息
        /// </summary>
        public ColumnForTopVo column_vo { get; set; }

        /// <summary>
        /// 固定值
        /// </summary>
        public string fixed_value { get; set; }
    }

    /// <summary>
    /// 列值数据
    /// </summary>
    public class ColumnDayAndVal
    {

        /// <summary>
        /// 日期
        /// </summary>
        public DateTime date { get; set; }

        /// <summary>
        /// 列值
        /// </summary>
        public string value { get; set; }

    }

    /// <summary>
    /// 列信息
    /// </summary>
    public class ColumnForTopVo
    {
        /// <summary>
        /// 报表列ID
        /// </summary>
        public long id { get; set; }

    }
}
