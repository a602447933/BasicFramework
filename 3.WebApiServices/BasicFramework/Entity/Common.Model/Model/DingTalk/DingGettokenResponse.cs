﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间:  Common.Model
*
* 功 能：获取企业内部应用的access_token，返回的信息
*
* 类 名：DingGettokenResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/3/9 9:10:52       LW      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 获取企业内部应用的access_token，返回的信息
    /// </summary>
    public class DingGettokenResponse
    {
        /// <summary>
        /// access_token
        /// </summary>
        public string access_token { get; set; }

        /// <summary>
        /// errcode
        /// </summary>
        public long errcode { get; set; }

        /// <summary>
        /// 返回码描述
        /// </summary>
        public string errmsg { get; set; }

        /// <summary>
        ///access_token的过期时间，单位秒。
        /// </summary>
        public long expires_in { get; set; }
    }
}
