﻿/*
* 命名空间: Common.Model
*
* 功 能： 控制器返回路指定径界面信息
*
* 类 名： ResultActionInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/13 15:57:22 Harvey     创建
*
* Copyright (c) 2020 SCZH Corporation. All rights reserved.
*/
using Newtonsoft.Json;
using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 控制器返回路指定径界面信息
    /// </summary>
    public class ResultActionInfo
    {
        /// <summary>
        /// 接口内部处理 返回码
        /// </summary>
        [Description("接口内部处理 返回码")]
        public ActionCodes Code { get; set; } = ActionCodes.InvalidOperation;

        /// <summary>
        /// 重定位路径
        /// </summary>
        public string RedirectPath { get; set; }

        /// <summary>
        /// 请求接口成功/失败 
        /// </summary>
        [Description("请求接口成功/失败 ")]
        public bool Success { get; set; } = true;

        /// <summary>
        /// 返回的操作消息
        /// </summary>
        [Description("返回的操作消息")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]//空值的处理
        public string Msg { get; set; } = null;

        /// <summary>
        /// 异常信息
        /// </summary>
        public string ExcepMessage { get; set; } = string.Empty;

    }
}
