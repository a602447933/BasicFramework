﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间: Common.Model
*
* 功 能： Echarts中2D坐标系返回信息
*
* 类 名： Echarts2DCoordinatesInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/14 15:50:09 	Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// Echarts中2D坐标系返回信息
    /// </summary>
    public class Echarts2DInfo<xT, yT>
    {
        /// <summary>
        /// X轴信息
        /// </summary>
        public List<xT> xAxis { get; set; }

        /// <summary>
        /// Y轴信息
        /// </summary>
        public List<yT> yAxis { get; set; }

    }
}
