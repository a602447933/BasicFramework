﻿/*
* 命名空间: Common.Model
*
* 功 能： 返回给客户端的信息实体
*
* 类 名： ResultJsonInfo<T>
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/13 15:57:22 Harvey     创建
*
* Copyright (c) 2020 SCZH Corporation. All rights reserved.
*/
using Newtonsoft.Json;
using System;
using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 返回给客户端的信息实体
    /// </summary>
    public class ResultJsonInfo<T>
    {
        /// <summary>
        /// 异常操作
        /// </summary>
        /// <param name="resultInfo"></param>
        /// <param name="ex">Exception</param>
        /// <param name="msg">错误信息【业务相关的描述】</param>
        public void SystemExc(ResultJsonInfo<T> resultInfo, Exception ex, string msg)
        {
            if (ex.GetType().Name == "ValidateException")
            {
                resultInfo.Msg = ex.Message;
                resultInfo.Code = ActionCodes.ArgumentInvalid;
            }
            else
            {
                resultInfo.Msg = msg;
            }
            resultInfo.Success = false;
            resultInfo.ExcepMessage = ex.Message;
            resultInfo.Code = ActionCodes.SystemError;
        }

        /// <summary>
        /// 返回客户端数据
        /// </summary>
        [Description("返回客户端数据")]
        public T Data { get; set; } = default(T);

        /// <summary>
        /// 接口内部处理 返回码
        /// </summary>
        [Description("接口内部处理 返回码")]
        public ActionCodes Code { get; set; } = ActionCodes.InvalidOperation;

        /// <summary>
        /// 请求接口成功/失败 
        /// </summary>
        [Description("请求接口成功/失败 ")]
        public bool Success { get; set; } = true;

        /// <summary>
        /// 返回的操作消息
        /// </summary>
        [Description("返回的操作消息")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]//空值的处理
        public string Msg { get; set; } = "无效操作";

        /// <summary>
        /// 额外信息
        /// </summary>
        [Description("额外信息")]
        public string ExtraInfo { get; set; } = string.Empty;

        /// <summary>
        /// 总的行数
        /// </summary>
        [Description("总的行数")]
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]//空值的处理
        public int? Count { get; set; } = null;


        /// <summary>
        /// 异常信息
        /// </summary>
        public string  ExcepMessage { get; set; } = string.Empty;
    }
}
