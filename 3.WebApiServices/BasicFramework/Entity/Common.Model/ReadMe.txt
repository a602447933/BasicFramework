﻿作用：
Common.Model专门放整个项目中，不同业务都会用到的实体和枚举类型
结构：
Enum 文件夹下存放枚举。
Model 文件夹下，存放公用实体。

注意：
1、备注类型

/*
* 命名空间: XXXXX
*
* 功 能： XXXXXXX
*
* 类 名： ResultFileInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/09 15:57:22 AA     创建
* V1.0.2    2020/03/09 16:57:22 XX     修改，增加什么字段，删除什么字段，修改什么字段等等
*
* Copyright (c) 2020 SCZH Corporation. All rights reserved.
*/

2、命名空间，统一且只能为：Common.Model