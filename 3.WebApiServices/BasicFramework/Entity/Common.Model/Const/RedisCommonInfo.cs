﻿/*
* 命名空间: Common.Model
*
* 功 能： 公用模块对应Redis设置
*
* 类 名： RedisCommonInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/10 10:44:12 	Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Common.Model
{
    /// <summary>
    /// 公用模块对应Redis设置
    /// </summary>
    public class RedisCommonInfo
    {
        /// <summary>
        ///系统分类
        /// </summary>
        public static string SysGroupName = "Harvey.InnerSystem";

        /// <summary>
        /// 构造函数
        /// </summary>
        static RedisCommonInfo()
        {
            try
            {
                //string rootDic  = AppContext.BaseDirectory;
                string rootDic = AppDomain.CurrentDomain.BaseDirectory;
                using (StreamReader sr = new StreamReader(Path.Combine(rootDic, "Config\\Redis\\redis.config.json")))
                {
                    var configStr = sr.ReadToEnd();
                    var configInfo = JsonConvert.DeserializeObject<Dictionary<string, object>>(configStr);
                    object sValue = null;
                    if (configInfo.TryGetValue("SysGroupName", out sValue))
                    {
                        SysGroupName = sValue.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///Hangfire存放数据库节点
        /// </summary>
        public const int RedisHangfireDbLog = 3;

        /// <summary>
        ///公用信息存放数据库节点
        /// </summary>
        public const int RedisCommonDbLog = 4;

        #region Hangfire定时任务相关

        /// <summary>
        ///HangfireJobHashId
        /// </summary>
        public const string RedisHangfireJobHashId = "{hangfire}:job:";

        /// <summary>
        ///HangfireSucceededListId
        /// </summary>
        public const string RedisHangfireSucceededListId = "{hangfire}:succeeded";

        /// <summary>
        ///RedisHangfire
        /// </summary>
        public const string RedisHangfire = "{hangfire}";
        #endregion

        #region 参数设置

        /// <summary>
        ///参数设置HASH对应的ID
        /// </summary>
        public static string RedisParameterSettingHashId = $"{RedisCommonInfo.SysGroupName}:ParameterSetting";

        #endregion

        #region 【内务系统】员工考勤信息设置

        /// <summary>
        /// 钉钉access_token失效时间 7200 提前200秒
        /// </summary>
        public const int AccessTokenTimeOut = 7000;

        /// <summary>
        /// 钉钉access_token存放区间
        /// </summary>
        public static string RedisAttendance = $"{RedisCommonInfo.SysGroupName}:AttendanceAccessToken";


        /// <summary>
        /// 钉钉员工信息存放区间
        /// </summary>
        public static string RedisDingEmployees = $"{RedisCommonInfo.SysGroupName}:DingEmployees";

        /// <summary>
        /// 钉钉员工补卡申请通过信息信息存放区间
        /// </summary>
        public static string RedisDingEmployeeFillCardApply = $"{RedisCommonInfo.SysGroupName}:DingEmployeeFillCardApply";

        #endregion

    }
}
