﻿namespace Common.Model
{
    /// <summary>
    /// 授权业务Redis全局变量
    /// </summary>
    public class RedisAuthorityInfo
    {

        /// <summary>
        ///Web界面Redis数据节点
        /// </summary>
        public const int RedisDbWeb = 0;

        /// <summary>
        ///Web界面菜单与按钮/图层的Redis数据节点
        /// </summary>
        public const int RedisDbWebOfPermission = 1;

        #region  登录

        /// <summary>
        /// 登录验证码Redis地址
        /// </summary>
        public static string RedisValidateCode = $"{RedisCommonInfo.SysGroupName}:ValidateCode";

        /// <summary>
        /// 验证码失效时间
        /// </summary>
        public const int TimeOut = 300;

        /// <summary>
        /// 用户登录信息 Redis地址
        /// </summary>
        public static string RedisSessionCode = $"{RedisCommonInfo.SysGroupName}:SessionCode";

        /// <summary>
        /// SessionCode失效时间【账户信息是永久存储，RefreshToken使用该失效时间】
        /// </summary>
        public const int SessionTimeOut = 2 * 24 * 60 * 60;

        #endregion

        #region 菜单和按钮

        /// <summary>
        /// Menus Redis地址
        /// </summary>
        public static string RedisMenusCode = $"{RedisCommonInfo.SysGroupName}:MenusCode";

        /// <summary>
        /// Buttons Redis地址
        /// </summary>
        public static string RedisButtonsCode = $"{RedisCommonInfo.SysGroupName}:ButtonsCode";

        #endregion


    }
}
