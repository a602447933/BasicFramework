﻿/*
* 命名空间: Common.Model
*
* 功 能： 基础管理逻辑模块对应Redis设置
*
* 类 名： BasicRedisInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/10 10:44:12 	Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 基础管理逻辑模块对应Redis设置
    /// </summary>
    public class RedisLogInfo
    {
        /// <summary>
        ///日志相关存放数据库节点【与redis配置文件中的ExceptionConfig信息一致】
        /// </summary>
        public const int RedisLogDbLog = 2;

        /// <summary>
        /// 防止重放攻击记录数据库空间节点
        /// </summary>
        public const int RedisTimestampDbLog = 5;

        #region 异常日志

        /// <summary>
        ///异常日志HASH对应的ID【与redis配置文件中的ExceptionConfig信息一致】
        /// </summary>
        public static string RedisExceptionHashId = $"{RedisCommonInfo.SysGroupName}:ExceptionLog";

        #endregion

        #region 安全相关-访问频率警告线
        /// <summary>
        ///  单个IP访问同一个接口访问警告频率上线【可以手动进黑名单】
        /// </summary>
        public const int WarningOnLine = 20;

        /// <summary>
        /// 单个IP访问同一个接口访问禁止频率上线【直接进黑名单】
        /// </summary>
        public const int NoAccessOnLine = 40;
        #endregion

        #region 接口信息
        /// <summary>
        ///接口HASH对应的ID
        /// </summary>
        public static string RedisApiHashId = $"{RedisCommonInfo.SysGroupName}:Api";

        #endregion

        #region 接口访问记录

        /// <summary>
        ///接口访问记录HASH对应的ID
        /// </summary>
        public static string RedisApiMonitorHashId = $"{RedisCommonInfo.SysGroupName}:ApiMonitor";

        /// <summary>
        /// 接口访问记录失效时间
        /// </summary>
        public const int RedisApiMonitorTimeOut = 10 * 60;

        #endregion

        #region 接口访问记录统计

        /// <summary>
        ///接口访问记录统计信息HASH对应的ID
        /// </summary>
        public static string RedisApiMonitorStatisticsHashId = $"{RedisCommonInfo.SysGroupName}:ApiMonitorStatistics";

        /// <summary>
        ///接口访问记录Path统计信息HASH对应的ID
        /// </summary>
        public static string RedisApiMonitorPathStatisticsHashId = $"{RedisCommonInfo.SysGroupName}:ApiMonitorPathStatistics";

        /// <summary>
        ///接口访问记录PathIp统计信息HASH对应的ID
        /// </summary>
        public static string RedisApiMonitorPathIpStatisticsHashId = $"{RedisCommonInfo.SysGroupName}:ApiMonitorPathIpStatistics";

        /// <summary>
        ///接口访问黑名单和警告名单
        /// </summary>
        public static string RedisApiMonitorNameListHashId = $"{RedisCommonInfo.SysGroupName}:ApiMonitorNameList";

        #endregion

        #region 接口访问次数

        /// <summary>
        ///接口访问次数HASH对应的ID
        /// </summary>
        public static string RedisApiMonitorNumberHashId = $"{RedisCommonInfo.SysGroupName}:ApiMonitorNumber";

        #endregion

        #region IP+时间戳请求记录

        /// <summary>
        /// 时间戳请求记录对应的ID
        /// </summary>
        public static string RedisApiTimestampId = $"{RedisCommonInfo.SysGroupName}:ApiTimestamp";
        #endregion
    }
}
