﻿/*
* 命名空间: System.Model
*
* 功 能： SysServerApiInfo实体类
*
* 类 名： SysServerApiInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/12/29 18:21:00 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 服务接口信息表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_SERVER_API_INFO")]
    public class SysServerApiInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SysServerApiInfoEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //接口服务名称
            this.server_name = string.Empty;

            //swagger地址
            this.swagger_url = string.Empty;

            //创建时间
            this.create_time = DateTime.Now;

            //备注
            this.remarks = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 接口服务名称
        /// </summary>
        [DBFieldInfo(ColumnName = "SERVER_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string server_name
        {
            get; set;
        }

        /// <summary>
        /// swagger地址
        /// </summary>
        [DBFieldInfo(ColumnName = "SWAGGER_URL", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string swagger_url
        {
            get; set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_TIME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public DateTime create_time
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string remarks
        {
            get; set;
        }
    }
}
