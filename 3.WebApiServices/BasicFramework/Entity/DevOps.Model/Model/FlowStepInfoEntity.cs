/*
* 命名空间: DevOps.Model
*
* 功 能： FlowFlowStep实体类
*
* 类 名： FlowFlowStepEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/01/18 17:52:41 LW     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 流程步骤表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "FLOW_STEP_INFO")]
    public class FlowStepInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowStepInfoEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //步骤名称
            this.name = string.Empty;

            //上一个步骤ID
            this.parent_id = string.Empty;

            //流程ID
            this.flow_id = string.Empty;

            //流程名称
            this.flow_name = string.Empty;

            //类型 task 100起始节点 110过程节点 120结束节点
            this.type = 0;

            //左侧坐标
            this.left = string.Empty;

            //顶部坐标
            this.top = string.Empty;

            //icon标签
            this.icon = string.Empty;

            //是否仅用于浏览，true: 不可拖拽
            this.view_only = true;

            //是否可以选择 100 可选择【 handler_type=100所有人员 110选择人员】 110不可选【110不可选 handler_type=120 发起者部门领导 130发起者上级部门领导 140发起者所有上级部门领导 】
            this.is_choosable = 0;

            //选择可操作人员【在handler_type==110的情况下，选择的具体人员】
            this.choose_person = string.Empty;

            //选择可操作人员名称
            this.person_name = string.Empty;

            //处理者类型 100所有人员 110选择人员 120发起者部门领导 130发起者上级部门领导  140发起者所有上级部门领导 
            this.handler_type = 0;

            //默认处理者[只有一个人]
            this.default_handler = string.Empty;

            //默认处理者名称
            this.handler_name = string.Empty;

            //处理策略 100所有人必须同意  110一人同意即可 120依据人数比例
            this.processing_strategy = 0;

            //处理策略百分比
            this.processing_percent = 0;

            //会签策略 100不会签 110所有步骤同意 120一个步骤同意即可 130依据比例
            this.sign_strategy = 0;

            //会签比例
            this.sign_percent = 0;

            //退回类型100退回上一步 110退回第一步 120退回指定步骤
            this.return_type = 0;

            //退回至步骤ID
            this.return_step = string.Empty;

            //步骤按钮
            this.button_ids = string.Empty;

            //步骤按钮
            this.button_names = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 步骤名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 上一个步骤ID
        /// </summary>
        [DBFieldInfo(ColumnName = "PARENT_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 流程ID
        /// </summary>
        [DBFieldInfo(ColumnName = "FLOW_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string flow_id
        {
            get; set;
        }

        /// <summary>
        /// 流程名称
        /// </summary>
        [DBFieldInfo(ColumnName = "FLOW_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string flow_name
        {
            get; set;
        }

        /// <summary>
        /// 类型 task 100起始节点 110过程节点 120结束节点
        /// </summary>
        [DBFieldInfo(ColumnName = "TYPE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int type
        {
            get; set;
        }

        /// <summary>
        /// 左侧坐标
        /// </summary>
        [DBFieldInfo(ColumnName = "LEFT", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string left
        {
            get; set;
        }

        /// <summary>
        /// 顶部坐标
        /// </summary>
        [DBFieldInfo(ColumnName = "TOP", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string top
        {
            get; set;
        }

        /// <summary>
        /// icon标签
        /// </summary>
        [DBFieldInfo(ColumnName = "ICON", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string icon
        {
            get; set;
        }

        /// <summary>
        /// 是否仅用于浏览，true: 不可拖拽
        /// </summary>
        [DBFieldInfo(ColumnName = "VIEW_ONLY", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public bool view_only
        {
            get; set;
        }

        /// <summary>
        /// 是否可以选择 100 可选择【 handler_type=100所有人员 110选择人员】 110不可选【110不可选 handler_type=120 发起者部门领导 130发起者上级部门领导 140发起者所有上级部门领导 】
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_CHOOSABLE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int is_choosable
        {
            get; set;
        }

        /// <summary>
        /// 选择可操作人员【在handler_type==110的情况下，选择的具体人员】
        /// </summary>
        [DBFieldInfo(ColumnName = "CHOOSE_PERSON", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string choose_person
        {
            get; set;
        }

        /// <summary>
        /// 选择可操作人员名称
        /// </summary>
        [DBFieldInfo(ColumnName = "PERSON_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string person_name
        {
            get; set;
        }

        /// <summary>
        /// 处理者类型 100所有人员 110选择人员 120发起者部门领导 130发起者上级部门领导  140发起者所有上级部门领导 
        /// </summary>
        [DBFieldInfo(ColumnName = "HANDLER_TYPE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int handler_type
        {
            get; set;
        }

        /// <summary>
        /// 默认处理者[只有一个人]
        /// </summary>
        [DBFieldInfo(ColumnName = "DEFAULT_HANDLER", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string default_handler
        {
            get; set;
        }

        /// <summary>
        /// 默认处理者名称
        /// </summary>
        [DBFieldInfo(ColumnName = "HANDLER_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string handler_name
        {
            get; set;
        }

        /// <summary>
        /// 处理策略 100所有人必须同意  110一人同意即可 120依据人数比例
        /// </summary>
        [DBFieldInfo(ColumnName = "PROCESSING_STRATEGY", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int processing_strategy
        {
            get; set;
        }

        /// <summary>
        /// 处理策略百分比
        /// </summary>
        [DBFieldInfo(ColumnName = "STRATEGY_PERCENT", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public double processing_percent
        {
            get; set;
        }

        /// <summary>
        /// 会签策略 100不会签 110所有步骤同意 120一个步骤同意即可 130依据比例
        /// </summary>
        [DBFieldInfo(ColumnName = "SIGN_STRATEGY", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int sign_strategy
        {
            get; set;
        }

        /// <summary>
        /// 会签比例
        /// </summary>
        [DBFieldInfo(ColumnName = "SIGN_PERCENT", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public double sign_percent
        {
            get; set;
        }


        /// <summary>
        /// 退回类型100退回上一步 110退回第一步 120退回指定步骤
        /// </summary>
        [DBFieldInfo(ColumnName = "RETURN_TYPE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int return_type
        {
            get; set;
        }

        /// <summary>
        /// 退回至步骤ID
        /// </summary>
        [DBFieldInfo(ColumnName = "RETURN_STEP", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string return_step
        {
            get; set;
        }

        /// <summary>
        /// 步骤按钮
        /// </summary>
        [DBFieldInfo(ColumnName = "BUTTON_IDS", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string button_ids
        {
            get; set;
        }
        /// <summary>
        /// 步骤按钮
        /// </summary>
        [DBFieldInfo(ColumnName = "BUTTON_NAMES", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string button_names
        {
            get; set;
        }
    }
}
