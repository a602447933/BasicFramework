/*
* 命名空间: DevOps.Model
*
* 功 能： SysPostInfo实体类
*
* 类 名： SysPostInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
     using Dapper.Library;

    /// <summary>
    /// 岗位表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_POST_INFO")]
    public class SysPostInfoEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysPostInfoEntity()
     {

           //名称
           this.name = string.Empty;

           //备注
           this.remarks = string.Empty;

           //修改者
           this.modifier_id = string.Empty;

           //唯一标识符
           this.id = Guid.NewGuid().ToString().Replace("-", "");

            //排序
            this.sort = 0;

           //简码，CEO,CTO
           this.brevity_code = string.Empty;

           //创建者
           this.creator_id = string.Empty;

           //创建者姓名
           this.creator_name = string.Empty;

           //创建日期
           this.create_date = DateTime.Now;

           //修改者姓名
           this.modifier_name = string.Empty;

           //修改者日期
           this.modifier_date = DateTime.Now;

           //是否有效
           this.is_valid = true;

           //是否被逻辑删除
           this.is_deleted = false;
      }

        /// <summary>
        /// 名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 修改者
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 简码，CEO,CTO
        /// </summary>
        [DBFieldInfo(ColumnName = "BREVITY_CODE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string brevity_code
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATOR_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATOR_NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_DATE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_NAME",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 修改者日期
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_DATE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_VALID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_deleted
        {
            get; set;
        }
    }
}
