/*
* 命名空间: DevOps.Model
*
* 功 能： SysDepartmentFunctionRe实体类
*
* 类 名： SysDepartmentFunctionReEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 部门与功能关联表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_DEPARTMENT_FUNCTION_RE")]
    public class SysDepartmentFunctionReEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysDepartmentFunctionReEntity()
     {

           //用户组唯一标识符
           this.group_id = string.Empty;

           //功能唯一标识符
           this.function_id = string.Empty;

           //唯一标识符
           this.id = Guid.NewGuid().ToString().Replace("-", "");
        }

        /// <summary>
        /// 用户组唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "GROUP_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string group_id
        {
            get; set;
        }

        /// <summary>
        /// 功能唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "FUNCTION_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string function_id
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }
    }
}
