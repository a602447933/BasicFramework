/*
* 命名空间: DevOps.Model
*
* 功 能： SysFunctionCfg实体类
*
* 类 名： SysFunctionCfgEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 系统菜单功能配置表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_FUNCTION_CFG")]
    public class SysFunctionCfgEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SysFunctionCfgEntity()
        {

            //修改者
            this.modifier_id = string.Empty;

            //父节点唯一标识符
            this.parent_id = string.Empty;

            //菜单名称
            this.name = string.Empty;

            //系统图标
            this.icon_font = string.Empty;

            //级别 10：一级 20：二级 30：三级
            this.level = 1;

            //链接路由地址【用于Web端】
            this.link_url = string.Empty;

            //所属应用ids【有很多应用，通过","区分】
            this.application_ids = string.Empty;

            //所属应用名称【有很多应用，通过","区分】
            this.application_name = string.Empty;

            //功能类型
            this.function_type = string.Empty;

            //排序编号
            this.sort = 0;

            //描述
            this.describe = string.Empty;

            //创建者
            this.creator_id = string.Empty;

            //创建者姓名
            this.creator_name = string.Empty;

            //创建日期
            this.create_date = DateTime.Now;

            //修改者姓名
            this.modifier_name = string.Empty;

            //修改者日期
            this.modifier_date = DateTime.Now;

            //是否有效
            this.is_valid = true;

            //是否被逻辑删除
            this.is_deleted = false;

            //功能快捷键
            this.shortcut = string.Empty;

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("-", "");

            //是否展开
            this.is_spread = false;

            //展开方式
            this.target = string.Empty;

         
        }

        /// <summary>
        /// 修改者
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_ID", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 父节点唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "PARENT_ID", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 菜单名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 系统图标
        /// </summary>
        [DBFieldInfo(ColumnName = "ICON_FONT", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string icon_font
        {
            get; set;
        }

        /// <summary>
        /// 级别 10：一级 20：二级 30：三级
        /// </summary>
        [DBFieldInfo(ColumnName = "LEVEL", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int level
        {
            get; set;
        }

        /// <summary>
        /// 链接路由地址【用于Web端】
        /// </summary>
        [DBFieldInfo(ColumnName = "LINK_URL", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string link_url
        {
            get; set;
        }

        /// <summary>
        /// 所属应用名称【有很多应用，通过","区分】
        /// </summary>
        [DBFieldInfo(ColumnName = "APPLICATION_IDS", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string application_ids
        {

            get; set;
        }

        /// <summary>
        /// 所属应用【有很多应用，通过","区分】
        /// </summary>
        [DBFieldInfo(ColumnName = "APPLICATION_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string application_name
        {
            get; set;
        }


        /// <summary>
        /// 功能类型
        /// </summary>
        [DBFieldInfo(ColumnName = "FUNCTION_TYPE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string function_type
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 描述
        /// </summary>
        [DBFieldInfo(ColumnName = "DESCRIBE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string describe
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATOR_ID", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATOR_NAME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_DATE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_NAME", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 修改者日期
        /// </summary>
        [DBFieldInfo(ColumnName = "MODIFIER_DATE", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public DateTime modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_VALID", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public bool is_deleted
        {
            get; set;
        }

        /// <summary>
        /// 功能快捷键
        /// </summary>
        [DBFieldInfo(ColumnName = "SHORTCUT", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string shortcut
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 是否展开
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_SPREAD", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public bool is_spread
        {
            get; set;
        }

        /// <summary>
        /// 展开方式
        /// </summary>
        [DBFieldInfo(ColumnName = "TARGET", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string target
        {
            get; set;
        }


    }
}
