/*
* 命名空间: DevOps.Model
*
* 功 能： FlowLine实体类
*
* 类 名： FlowLineInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/01/18 10:38:41 LW     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 流程连线表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "FLOW_LINE_INFO")]
    public class FlowLineInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowLineInfoEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //起始步骤ID
            this.from = string.Empty;

            //结束步骤ID
            this.to = string.Empty;

            //流程id
            this.flow_id = string.Empty;

            //流程名称
            this.flow_name = string.Empty;

            //条件信息标题【流程主键需要】
            this.label = string.Empty;

            //条件信息
            this.condition = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 起始步骤ID
        /// </summary>
        [DBFieldInfo(ColumnName = "FROM", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string from
        {
            get; set;
        }

        /// <summary>
        /// 结束步骤ID
        /// </summary>
        [DBFieldInfo(ColumnName = "TO", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string to
        {
            get; set;
        }

        /// <summary>
        /// 流程id
        /// </summary>
        [DBFieldInfo(ColumnName = "FLOW_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string flow_id
        {
            get; set;
        }

        /// <summary>
        /// 流程名称
        /// </summary>
        [DBFieldInfo(ColumnName = "FLOW_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string flow_name
        {
            get; set;
        }

        /// <summary>
        /// 条件信息标题【流程主键需要】
        /// </summary>
        [DBFieldInfo(ColumnName = "LABEL", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string label
        {
            get; set;
        }

        /// <summary>
        /// 条件信息
        /// </summary>
        [DBFieldInfo(ColumnName = "CONDITION", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string condition
        {
            get; set;
        }
    }
}
