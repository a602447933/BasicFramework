﻿/*
* 命名空间: DevOps.Model
*
* 功 能：  SafeApiMonitorNumberEntity 实体类
*
* 类 名： SafeApiMonitorNumberEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/15 11:05:50 	Harvey    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    /// <summary>
    /// 接口访问次数实体
    /// </summary>
    public class SafeApiMonitorNumberEntity
    {

        /// <summary>
        /// 请求路径
        /// </summary>
        public string request_path
        {
            get; set;
        }

        /// <summary>
        /// Http请求总的次数
        /// </summary>
        public long request_number
        {
            get; set;
        }

    }
}
