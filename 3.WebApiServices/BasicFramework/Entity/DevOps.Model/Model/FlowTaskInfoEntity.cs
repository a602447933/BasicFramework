/*
* 命名空间: EIM.Model
*
* 功 能： FlowTaskInfo实体类
*
* 类 名： FlowTaskInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/01/20 11:43:29 LW     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 流程任务表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "FLOW_TASK_INFO")]
    public class FlowTaskInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowTaskInfoEntity()
        {

            //唯一标识符
            this.id = string.Empty;

            //上一个任务ID
            this.parent_id = string.Empty;

            //上一步骤ID
            this.parent_step_id = string.Empty;

            //流程Id
            this.flow_id = string.Empty;

            //流程名称
            this.flow_name = string.Empty;

            //步骤ID
            this.step_id = string.Empty;

            //步骤名称
            this.step_name = string.Empty;

            //对应业务表主键值
            this.instance_id = string.Empty;

            //任务类型 100常规 110指派 120委托 130转交 140退回 150抄送 160前加签 170并签 180后加签
            this.task_type = 0;

            //任务标题
            this.title = string.Empty;

            //发送人ID
            this.sender_id = string.Empty;

            //发送人姓名
            this.sender_name = string.Empty;

            //接收人ID
            this.receive_id = string.Empty;

            //接收人姓名
            this.receive_name = string.Empty;

            //接收时间
            this.receive_time = DateTime.Now;

            //完成时间
            this.finish_time = DateTime.Now;

            //处理意见
            this.comments = string.Empty;

            //备注
            this.remarks = string.Empty;

            //任务状态 100等待中 110未处理 120处理中 130已完成
            this.status = 0;

            //任务顺序
            this.sort = 0;

            //处理类型 100等待中 110未处理 120处理中 130已完成 140已退回 150他人已处理 160他人已退回 170已转交 180已委托 190已阅知
            this.execute_type = 0;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 上一个任务ID
        /// </summary>
        [DBFieldInfo(ColumnName = "PARENT_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 上一步骤ID
        /// </summary>
        [DBFieldInfo(ColumnName = "PARENT_STEP_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string parent_step_id
        {
            get; set;
        }

        /// <summary>
        /// 流程Id
        /// </summary>
        [DBFieldInfo(ColumnName = "FLOW_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string flow_id
        {
            get; set;
        }

        /// <summary>
        /// 流程名称
        /// </summary>
        [DBFieldInfo(ColumnName = "FLOW_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string flow_name
        {
            get; set;
        }

        /// <summary>
        /// 步骤ID
        /// </summary>
        [DBFieldInfo(ColumnName = "STEP_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string step_id
        {
            get; set;
        }

        /// <summary>
        /// 步骤名称
        /// </summary>
        [DBFieldInfo(ColumnName = "STEP_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string step_name
        {
            get; set;
        }

        /// <summary>
        /// 对应业务表主键值
        /// </summary>
        [DBFieldInfo(ColumnName = "INSTANCE_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string instance_id
        {
            get; set;
        }

        /// <summary>
        /// 任务类型 100常规 110指派 120委托 130转交 140退回 150抄送 160前加签 170并签 180后加签
        /// </summary>
        [DBFieldInfo(ColumnName = "TASK_TYPE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int task_type
        {
            get; set;
        }

        /// <summary>
        /// 任务标题
        /// </summary>
        [DBFieldInfo(ColumnName = "TITLE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string title
        {
            get; set;
        }

        /// <summary>
        /// 发送人ID
        /// </summary>
        [DBFieldInfo(ColumnName = "SENDER_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string sender_id
        {
            get; set;
        }

        /// <summary>
        /// 发送人姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "SENDER_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string sender_name
        {
            get; set;
        }

        /// <summary>
        /// 接收人ID
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVE_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string receive_id
        {
            get; set;
        }

        /// <summary>
        /// 接收人姓名
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVE_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string receive_name
        {
            get; set;
        }

        /// <summary>
        /// 接收时间
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVE_TIME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public DateTime receive_time
        {
            get; set;
        }

        /// <summary>
        /// 完成时间
        /// </summary>
        [DBFieldInfo(ColumnName = "FINISH_TIME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public DateTime? finish_time
        {
            get; set;
        }

        /// <summary>
        /// 处理意见
        /// </summary>
        [DBFieldInfo(ColumnName = "COMMENTS", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string comments
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 任务状态 100等待中 110未处理 120处理中 130已完成 140未生效
        /// </summary>
        [DBFieldInfo(ColumnName = "STATUS", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int status
        {
            get; set;
        }

        /// <summary>
        /// 任务顺序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int sort
        {
            get; set;
        }

        /// <summary>
        /// 处理类型 100等待中 110未处理 120处理中 130已完成 140已退回 150他人已处理 160他人已退回 170已转交 180已委托 190已阅知
        /// </summary>
        [DBFieldInfo(ColumnName = "EXECUTE_TYPE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int execute_type
        {
            get; set;
        }


    }
}
