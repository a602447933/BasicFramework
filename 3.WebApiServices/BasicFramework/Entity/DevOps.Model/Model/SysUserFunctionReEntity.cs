/*
* 命名空间: DevOps.Model
*
* 功 能： SysUserFunctionRe实体类
*
* 类 名： SysUserFunctionReEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 用户与功能关联表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_USER_FUNCTION_RE")]
    public class SysUserFunctionReEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysUserFunctionReEntity()
     {

           //用户唯一标识符
           this.user_id = string.Empty;

           //唯一标识符
           this.id = Guid.NewGuid().ToString().Replace("-", "");

            //功能唯一标识符
            this.function_id = string.Empty;
      }

        /// <summary>
        /// 用户唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "USER_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string user_id
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 功能唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "FUNCTION_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string function_id
        {
            get; set;
        }
    }
}
