/*
* 命名空间: DevOps.Model
*
* 功 能： SysRolePermissionRe实体类
*
* 类 名： SysRolePermissionReEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 角色-权限关系表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "devops", TableName = "SYS_ROLE_PERMISSION_RE")]
    public class SysRolePermissionReEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysRolePermissionReEntity()
     {

           //角色编号
           this.role_id = string.Empty;

           //图层树或功能编号
           this.permission_id = string.Empty;

           //权限类别 100图层 200功能
           this.permission_type = 0;

           //唯一标识符
           this.id = Guid.NewGuid().ToString().Replace("-", "");
        }

        /// <summary>
        /// 角色编号
        /// </summary>
        [DBFieldInfo(ColumnName = "ROLE_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string role_id
        {
            get; set;
        }

        /// <summary>
        /// 图层树或功能编号
        /// </summary>
        [DBFieldInfo(ColumnName = "PERMISSION_ID",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string permission_id
        {
            get; set;
        }

        /// <summary>
        /// 权限类别 100图层 200功能
        /// </summary>
        [DBFieldInfo(ColumnName = "PERMISSION_TYPE",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public int permission_type
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }
    }
}
