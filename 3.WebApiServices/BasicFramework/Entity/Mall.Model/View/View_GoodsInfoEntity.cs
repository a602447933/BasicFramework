/*
* 命名空间: Mall.Model
*
* 功 能： View_GoodsInfo视图实体类
*
* 类 名： View_GoodsInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/08/05 13:20:38 Harvey     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using Dapper.Library;
    using System;

    /// <summary>
    /// 商品视图
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "view_goods_info")]
    public class View_GoodsInfoEntity
    {

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID")]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 分类唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "CATEGORY_ID")]
        public string category_id
        {
            get; set;
        }

        /// <summary>
        /// 分类名称
        /// </summary>
        [DBFieldInfo(ColumnName = "CATEGORY_NAME")]
        public string category_name
        {
            get; set;
        }

        /// <summary>
        /// 商品名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 商品副标题
        /// </summary>
        [DBFieldInfo(ColumnName = "TITLE")]
        public string title
        {
            get; set;
        }

        /// <summary>
        /// 商品货号
        /// </summary>
        [DBFieldInfo(ColumnName = "GOODS_NO")]
        public string goods_no
        {
            get; set;
        }

        /// <summary>
        /// 售卖单价
        /// </summary>
        [DBFieldInfo(ColumnName = "SELLING_PRICE")]
        public decimal selling_price
        {
            get; set;
        }

        /// <summary>
        /// 商品原价
        /// </summary>
        [DBFieldInfo(ColumnName = "ORIGINAL_PRICE")]
        public decimal original_price
        {
            get; set;
        }

        /// <summary>
        ///是否热卖(100-是 110-不是) 
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_HOT_STYLE")]
        public int is_hot_style
        {
            get; set;
        }

        /// <summary>
        /// 是否新款(100-是 110-不是)
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_NEW_STYLE")]
        public int is_new_style
        {
            get; set;
        }

        /// <summary>
        /// 是否推荐(100-是 110-不是)
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_RECOMMEND")]
        public int is_recommend
        {
            get; set;
        }

        /// <summary>
        /// 是否为特价商品(100-是 110-不是)
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_BARGAIN")]
        public int is_bargain
        {
            get; set;
        }

        /// <summary>
        /// 是否上架(100-是 110-不是)
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_SHELVES")]
        public int is_shelves
        {
            get; set;
        }

        /// <summary>
        /// 总销量
        /// </summary>
        [DBFieldInfo(ColumnName = "SALES_VOLUME")]
        public int sales_volume
        {
            get; set;
        }

        /// <summary>
        /// 添加时间
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_TIME")]
        public string create_time
        {
            get; set;
        }

        /// <summary>
        /// 商品详情描述,富文本框
        /// </summary>
        [DBFieldInfo(ColumnName = "INTRODUCTION")]
        public string introduction
        {
            get; set;
        }

        /// <summary>
        /// 商品排序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT")]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 是否被删除 100正常 110删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED")]
        public int is_deleted
        {
            get; set;
        }
    }
}
