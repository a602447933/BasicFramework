/*
* 命名空间: Mall.Model
*
* 功 能： GoodsStandard实体类
*
* 类 名： GoodsStandardEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 商品规格表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "GOODS_STANDARD")]
    public class GoodsStandardEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GoodsStandardEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //商品关键码
            this.goods_id = string.Empty;

            //商品货号
            this.goods_no = string.Empty;

            //规格值
            this.standard_name = string.Empty;

            //价格
            this.price = 0;

            //数量
            this.amount = 0;

            //商品条码
            this.bar_code = string.Empty;

            //添加时间
            this.create_time = DateTime.Now;

            //是否被删除 100正常 110删除
            this.is_deleted = 0;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 商品关键码
        /// </summary>
        [DBFieldInfo(ColumnName = "GOODS_ID", Required = false, IsPrimarykey = false)]
        public string goods_id
        {
            get; set;
        }

        /// <summary>
        /// 商品货号
        /// </summary>
        [DBFieldInfo(ColumnName = "GOODS_NO", Required = false, IsPrimarykey = false)]
        public string goods_no
        {
            get; set;
        }

        /// <summary>
        /// 规格值
        /// </summary>
        [DBFieldInfo(ColumnName = "STANDARD_NAME", Required = false, IsPrimarykey = false)]
        public string standard_name
        {
            get; set;
        }

        /// <summary>
        /// 价格
        /// </summary>
        [DBFieldInfo(ColumnName = "PRICE", Required = false, IsPrimarykey = false)]
        public decimal price
        {
            get; set;
        }

        /// <summary>
        /// 数量
        /// </summary>
        [DBFieldInfo(ColumnName = "AMOUNT", Required = false, IsPrimarykey = false)]
        public int amount
        {
            get; set;
        }

        /// <summary>
        /// 商品条码
        /// </summary>
        [DBFieldInfo(ColumnName = "BAR_CODE", Required = false, IsPrimarykey = false)]
        public string bar_code
        {
            get; set;
        }

        /// <summary>
        /// 添加时间
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_TIME", Required = false, IsPrimarykey = false)]
        public DateTime create_time
        {
            get; set;
        }

        /// <summary>
        /// 是否被删除 100正常 110删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED", Required = false, IsPrimarykey = false)]
        public int is_deleted
        {
            get; set;
        }
    }
}
