/*
* 命名空间: Mall.Model
*
* 功 能： ClientBalanceDetail实体类
*
* 类 名： ClientBalanceDetailEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 客户金额变动明细表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "CLIENT_BALANCE_DETAIL")]
    public class ClientBalanceDetailEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public ClientBalanceDetailEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //用户唯一标识符
            this.client_id = string.Empty;

            //订单唯一标识符
            this.order_id = string.Empty;

            //订单编号
            this.order_no = string.Empty;

            //变动时间
            this.create_time = DateTime.Now;

            //变动金额
            this.price = 0;

            //100购买，110退款
            this.operation_type = 0;

            //备注
            this.remarks = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "CLIENT_ID", Required = true, IsPrimarykey = false)]
        public string client_id
        {
            get; set;
        }

        /// <summary>
        /// 订单唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ORDER_ID", Required = false, IsPrimarykey = false)]
        public string order_id
        {
            get; set;
        }

        /// <summary>
        /// 订单编号
        /// </summary>
        [DBFieldInfo(ColumnName = "ORDER_NO", Required = false, IsPrimarykey = false)]
        public string order_no
        {
            get; set;
        }

        /// <summary>
        /// 变动时间
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_TIME", Required = false, IsPrimarykey = false)]
        public DateTime create_time
        {
            get; set;
        }

        /// <summary>
        /// 变动金额
        /// </summary>
        [DBFieldInfo(ColumnName = "PRICE", Required = false, IsPrimarykey = false)]
        public decimal price
        {
            get; set;
        }

        /// <summary>
        /// 100购买，110退款
        /// </summary>
        [DBFieldInfo(ColumnName = "OPERATION_TYPE", Required = false, IsPrimarykey = false)]
        public int operation_type
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS", Required = false, IsPrimarykey = false)]
        public string remarks
        {
            get; set;
        }
    }
}
