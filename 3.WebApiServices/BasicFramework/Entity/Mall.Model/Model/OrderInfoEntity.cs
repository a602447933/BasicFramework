/*
* 命名空间: Mall.Model
*
* 功 能： OrderInfo实体类
*
* 类 名： OrderInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 订单信息表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "ORDER_INFO")]
    public class OrderInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public OrderInfoEntity()
        {

            //订单唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //客户关键码
            this.client_id = string.Empty;

            //客户名
            this.client_name = string.Empty;

            //订单号
            this.order_no = string.Empty;

            //交易流水号
            this.trade_no = string.Empty;

            //订单支付原价
            this.original_price = 0;

            //订单物流占用价格
            this.logistics_price = 0;

            //订单实际支付价格
            this.pay_price = 0;

            //订单下单时间
            this.book_time = DateTime.Now;

            //客户备注
            this.remarks = string.Empty;

            //付款时间
            this.pay_time = DateTime.Now;

            //发货时间
            this.send_time = DateTime.Now;

            //发货备注
            this.send_remarks = string.Empty;

            //订单完成时间
            this.finish_time = DateTime.Now;

            //收货人
            this.receiver = string.Empty;

            //收货人电话
            this.receiver_phone = string.Empty;

            //收货地区 - 省
            this.receive_province = string.Empty;

            //收货地区 - 市
            this.receive_city = string.Empty;

            //收货地区- 区县
            this.receive_region = string.Empty;

            //收货详细地址
            this.receiver_address = string.Empty;

            //订单状态  100:待付款 110:已付款/待发货 120:已发货 130:已完成（待评价） 140:已完成（已经评价） 150:已取消 160:售后申请中
            this.status = 0;

            //物流配送名称
            this.logistics_name = string.Empty;

            //物流单号
            this.logistics_num = string.Empty;

            //100'微信', 110'支付宝',120 '银联', 130'余额'
            this.pay_type = 0;

            //是否被删除 100正常 110删除
            this.is_deleted = 0;
        }

        /// <summary>
        /// 订单唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 客户关键码
        /// </summary>
        [DBFieldInfo(ColumnName = "CLIENT_ID", Required = false, IsPrimarykey = false)]
        public string client_id
        {
            get; set;
        }

        /// <summary>
        /// 客户名
        /// </summary>
        [DBFieldInfo(ColumnName = "CLIENT_NAME", Required = false, IsPrimarykey = false)]
        public string client_name
        {
            get; set;
        }

        /// <summary>
        /// 订单号
        /// </summary>
        [DBFieldInfo(ColumnName = "ORDER_NO", Required = false, IsPrimarykey = false)]
        public string order_no
        {
            get; set;
        }

        /// <summary>
        /// 交易流水号
        /// </summary>
        [DBFieldInfo(ColumnName = "TRADE_NO", Required = false, IsPrimarykey = false)]
        public string trade_no
        {
            get; set;
        }

        /// <summary>
        /// 订单支付原价
        /// </summary>
        [DBFieldInfo(ColumnName = "ORIGINAL_PRICE", Required = false, IsPrimarykey = false)]
        public decimal original_price
        {
            get; set;
        }

        /// <summary>
        /// 订单物流占用价格
        /// </summary>
        [DBFieldInfo(ColumnName = "LOGISTICS_PRICE", Required = false, IsPrimarykey = false)]
        public decimal logistics_price
        {
            get; set;
        }

        /// <summary>
        /// 订单实际支付价格
        /// </summary>
        [DBFieldInfo(ColumnName = "PAY_PRICE", Required = false, IsPrimarykey = false)]
        public decimal pay_price
        {
            get; set;
        }

        /// <summary>
        /// 订单下单时间
        /// </summary>
        [DBFieldInfo(ColumnName = "BOOK_TIME", Required = false, IsPrimarykey = false)]
        public DateTime book_time
        {
            get; set;
        }

        /// <summary>
        /// 客户备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS", Required = false, IsPrimarykey = false)]
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 付款时间
        /// </summary>
        [DBFieldInfo(ColumnName = "PAY_TIME", Required = false, IsPrimarykey = false)]
        public DateTime pay_time
        {
            get; set;
        }

        /// <summary>
        /// 发货时间
        /// </summary>
        [DBFieldInfo(ColumnName = "SEND_TIME", Required = false, IsPrimarykey = false)]
        public DateTime send_time
        {
            get; set;
        }

        /// <summary>
        /// 发货备注
        /// </summary>
        [DBFieldInfo(ColumnName = "SEND_REMARKS", Required = false, IsPrimarykey = false)]
        public string send_remarks
        {
            get; set;
        }

        /// <summary>
        /// 订单完成时间
        /// </summary>
        [DBFieldInfo(ColumnName = "FINISH_TIME", Required = false, IsPrimarykey = false)]
        public DateTime finish_time
        {
            get; set;
        }

        /// <summary>
        /// 收货人
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVER", Required = false, IsPrimarykey = false)]
        public string receiver
        {
            get; set;
        }

        /// <summary>
        /// 收货人电话
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVER_PHONE", Required = false, IsPrimarykey = false)]
        public string receiver_phone
        {
            get; set;
        }

        /// <summary>
        /// 收货地区 - 省
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVE_PROVINCE", Required = false, IsPrimarykey = false)]
        public string receive_province
        {
            get; set;
        }

        /// <summary>
        /// 收货地区 - 市
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVE_CITY", Required = false, IsPrimarykey = false)]
        public string receive_city
        {
            get; set;
        }

        /// <summary>
        /// 收货地区- 区县
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVE_REGION", Required = false, IsPrimarykey = false)]
        public string receive_region
        {
            get; set;
        }

        /// <summary>
        /// 收货详细地址
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIVER_ADDRESS", Required = false, IsPrimarykey = false)]
        public string receiver_address
        {
            get; set;
        }

        /// <summary>
        /// 订单状态  100:待付款 110:已付款/待发货 120:已发货 130:已完成（待评价） 140:已完成（已经评价） 150:已取消 160:售后申请中
        /// </summary>
        [DBFieldInfo(ColumnName = "STATUS", Required = false, IsPrimarykey = false)]
        public int status
        {
            get; set;
        }

        /// <summary>
        /// 物流配送名称
        /// </summary>
        [DBFieldInfo(ColumnName = "LOGISTICS_NAME", Required = false, IsPrimarykey = false)]
        public string logistics_name
        {
            get; set;
        }

        /// <summary>
        /// 物流单号
        /// </summary>
        [DBFieldInfo(ColumnName = "LOGISTICS_NUM", Required = false, IsPrimarykey = false)]
        public string logistics_num
        {
            get; set;
        }

        /// <summary>
        /// 100'微信', 110'支付宝',120 '银联', 130'余额'
        /// </summary>
        [DBFieldInfo(ColumnName = "PAY_TYPE", Required = false, IsPrimarykey = false)]
        public int pay_type
        {
            get; set;
        }

        /// <summary>
        /// 是否被删除 100正常 110删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED", Required = false, IsPrimarykey = false)]
        public int is_deleted
        {
            get; set;
        }
    }
}
