/*
* 命名空间: Mall.Model
*
* 功 能： ShoppingCart实体类
*
* 类 名： ShoppingCartEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 购物车表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "SHOPPING_CART")]
    public class ShoppingCartEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public ShoppingCartEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //商品关键码
            this.goods_id = string.Empty;

            //规格关键码
            this.standard_id = string.Empty;

            //客户关键码
            this.client_id = string.Empty;

            //商品在购物车中的价格
            this.cart_price = 0;

            //商品在购物车中的数量
            this.cart_amount = 0;

            //插入时间
            this.create_time = DateTime.Now;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 商品关键码
        /// </summary>
        [DBFieldInfo(ColumnName = "GOODS_ID", Required = false, IsPrimarykey = false)]
        public string goods_id
        {
            get; set;
        }

        /// <summary>
        /// 规格关键码
        /// </summary>
        [DBFieldInfo(ColumnName = "STANDARD_ID", Required = false, IsPrimarykey = false)]
        public string standard_id
        {
            get; set;
        }

        /// <summary>
        /// 客户关键码
        /// </summary>
        [DBFieldInfo(ColumnName = "CLIENT_ID", Required = false, IsPrimarykey = false)]
        public string client_id
        {
            get; set;
        }

        /// <summary>
        /// 商品在购物车中的价格
        /// </summary>
        [DBFieldInfo(ColumnName = "CART_PRICE", Required = false, IsPrimarykey = false)]
        public decimal cart_price
        {
            get; set;
        }

        /// <summary>
        /// 商品在购物车中的数量
        /// </summary>
        [DBFieldInfo(ColumnName = "CART_AMOUNT", Required = false, IsPrimarykey = false)]
        public int cart_amount
        {
            get; set;
        }

        /// <summary>
        /// 插入时间
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATE_TIME", Required = false, IsPrimarykey = false)]
        public DateTime create_time
        {
            get; set;
        }
    }
}
