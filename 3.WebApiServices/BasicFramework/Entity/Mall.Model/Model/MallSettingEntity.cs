/*
* 命名空间: Mall.Model
*
* 功 能： MallSetting实体类
*
* 类 名： MallSettingEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 商城设置
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "MALL_SETTING")]
    public class MallSettingEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public MallSettingEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //商城名称
            this.name = string.Empty;

            //热线电话
            this.phone = string.Empty;

            //轮播图1地址
            this.banner_one = string.Empty;

            //轮播图2的超级链接地址
            this.banner_two = string.Empty;

            //轮播图3的超级链接地址
            this.banner_three = string.Empty;

            //商城Logo
            this.logo = string.Empty;

            //订单自动取消时长:单位小时,不设置，为关闭自动取消
            this.cancel_time = string.Empty;

            //订单自动收货时长:单位小时,不设置为，关闭自动收货
            this.receipt_time = string.Empty;

            //订单允许退货时长:单位小时,不设置，为随时可以申请售后
            this.allowed_return_time = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 商城名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME", Required = false, IsPrimarykey = false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 热线电话
        /// </summary>
        [DBFieldInfo(ColumnName = "PHONE", Required = false, IsPrimarykey = false)]
        public string phone
        {
            get; set;
        }

        /// <summary>
        /// 轮播图1地址
        /// </summary>
        [DBFieldInfo(ColumnName = "BANNER_ONE", Required = false, IsPrimarykey = false)]
        public string banner_one
        {
            get; set;
        }

        /// <summary>
        /// 轮播图2的超级链接地址
        /// </summary>
        [DBFieldInfo(ColumnName = "BANNER_TWO", Required = false, IsPrimarykey = false)]
        public string banner_two
        {
            get; set;
        }

        /// <summary>
        /// 轮播图3的超级链接地址
        /// </summary>
        [DBFieldInfo(ColumnName = "BANNER_THREE", Required = false, IsPrimarykey = false)]
        public string banner_three
        {
            get; set;
        }

        /// <summary>
        /// 商城Logo
        /// </summary>
        [DBFieldInfo(ColumnName = "LOGO", Required = false, IsPrimarykey = false)]
        public string logo
        {
            get; set;
        }

        /// <summary>
        /// 订单自动取消时长:单位小时,不设置，为关闭自动取消
        /// </summary>
        [DBFieldInfo(ColumnName = "CANCEL_TIME", Required = false, IsPrimarykey = false)]
        public string cancel_time
        {
            get; set;
        }

        /// <summary>
        /// 订单自动收货时长:单位小时,不设置为，关闭自动收货
        /// </summary>
        [DBFieldInfo(ColumnName = "RECEIPT_TIME", Required = false, IsPrimarykey = false)]
        public string receipt_time
        {
            get; set;
        }

        /// <summary>
        /// 订单允许退货时长:单位小时,不设置，为随时可以申请售后
        /// </summary>
        [DBFieldInfo(ColumnName = "ALLOWED_RETURN_TIME", Required = false, IsPrimarykey = false)]
        public string allowed_return_time
        {
            get; set;
        }
    }
}
