/*
* 命名空间: Mall.Model
*
* 功 能： WechatOfficialAccounts实体类
*
* 类 名： WechatOfficialAccountsEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/07/03 17:34:57 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 微信公众号信息
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "WECHAT_OFFICIAL_ACCOUNTS")]
    public class WechatOfficialAccountsEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public WechatOfficialAccountsEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //原始编码
            this.original_id = string.Empty;

            //应用ID
            this.appid_v3 = string.Empty;

            //商户ID
            this.mchid = string.Empty;

            //应用密钥
            this.app_secret = string.Empty;

            //API密钥
            this.pay_sign_key = string.Empty;

            //公众号名称
            this.account_name = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 原始编码
        /// </summary>
        [DBFieldInfo(ColumnName = "ORIGINAL_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string original_id
        {
            get; set;
        }

        /// <summary>
        /// 应用ID
        /// </summary>
        [DBFieldInfo(ColumnName = "APPID_V3", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string appid_v3
        {
            get; set;
        }

        /// <summary>
        /// 商户ID
        /// </summary>
        [DBFieldInfo(ColumnName = "MCHID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string mchid
        {
            get; set;
        }

        /// <summary>
        /// 应用密钥
        /// </summary>
        [DBFieldInfo(ColumnName = "APP_SECRET", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string app_secret
        {
            get; set;
        }

        /// <summary>
        /// API密钥
        /// </summary>
        [DBFieldInfo(ColumnName = "PAY_SIGN_KEY", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string pay_sign_key
        {
            get; set;
        }

        /// <summary>
        /// 公众号名称
        /// </summary>
        [DBFieldInfo(ColumnName = "ACCOUNT_NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string account_name
        {
            get; set;
        }
    }
}
