/*
* 命名空间: Mall.Model
*
* 功 能： GoodsPicture实体类
*
* 类 名： GoodsPictureEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/07/13 22:07:48 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 商品图片表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "GOODS_PICTURE")]
    public class GoodsPictureEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GoodsPictureEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //商品标识符
            this.goods_id = string.Empty;

            //文件名
            this.name = string.Empty;

            //文件地址
            this.url = string.Empty;

            //文件大小
            this.size = 0;

            //媒体类型  image  video  voice file
            this.media_type = string.Empty;

            //排序
            this.sort = 0;

            //备注
            this.remark = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 商品标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "GOODS_ID", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string goods_id
        {
            get; set;
        }

        /// <summary>
        /// 文件名
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 文件地址
        /// </summary>
        [DBFieldInfo(ColumnName = "URL", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string url
        {
            get; set;
        }

        /// <summary>
        /// 文件大小
        /// </summary>
        [DBFieldInfo(ColumnName = "SIZE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public int size
        {
            get; set;
        }

        /// <summary>
        /// 媒体类型  image  video  voice file
        /// </summary>
        [DBFieldInfo(ColumnName = "MEDIA_TYPE", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string media_type
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public long sort
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARK", Required = false, IsPrimarykey = false, IsIncrease = false)]
        public string remark
        {
            get; set;
        }
    }
}
