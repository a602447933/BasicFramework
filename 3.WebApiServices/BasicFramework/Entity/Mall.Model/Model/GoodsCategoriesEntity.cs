/*
* 命名空间: Mall.Model
*
* 功 能： GoodsCategories实体类
*
* 类 名： GoodsCategoriesEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 商品分类
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "GOODS_CATEGORIES")]
    public class GoodsCategoriesEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GoodsCategoriesEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //父级节点编码
            this.parent_id = string.Empty;

            //类名
            this.name = string.Empty;

            //分类图片
            this.img_url = string.Empty;

            //排序
            this.sort = 0;

            //备注
            this.remarks = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 父级节点编码
        /// </summary>
        [DBFieldInfo(ColumnName = "PARENT_ID", Required = false, IsPrimarykey = false)]
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 类名
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME", Required = false, IsPrimarykey = false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 分类图片
        /// </summary>
        [DBFieldInfo(ColumnName = "IMG_URL", Required = false, IsPrimarykey = false)]
        public string img_url
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT", Required = false, IsPrimarykey = false)]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        [DBFieldInfo(ColumnName = "REMARKS", Required = false, IsPrimarykey = false)]
        public string remarks
        {
            get; set;
        }
    }
}
