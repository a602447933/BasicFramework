/*
* 命名空间: Mall.Model
*
* 功 能： GoodsInfo实体类
*
* 类 名： GoodsInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 商品信息表
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "GOODS_INFO")]
    public class GoodsInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public GoodsInfoEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //分类唯一标识符
            this.category_id = string.Empty;

            //商品名称
            this.name = string.Empty;

            //商品副标题
            this.title = string.Empty;

            //商品货号
            this.goods_no = string.Empty;

            //售卖单价
            this.selling_price = 0;

            //商品原价
            this.original_price = 0;

            //是否热卖(100-是 110-不是)
            this.is_hot_style = 0;

            //是否新款(100-是 110-不是)
            this.is_new_style = 0;

            //是否推荐(100-是 110-不是)
            this.is_recommend = 0;

            //是否为特价商品(100-是 110-不是)
            this.is_bargain = 0;

            //是否上架(100-是 110-不是)
            this.is_shelves = 0;

            //总销量
            this.sales_volume = 0;

            //添加时间
            this.create_time = DateTime.Now;

            //商品详情描述,富文本框

            //商品排序
            this.sort = 0;

            //是否被删除 100正常 110删除
            this.is_deleted = 0;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 分类唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "CATEGORY_ID", Required = false, IsPrimarykey = false)]
        public string category_id
        {
            get; set;
        }

        /// <summary>
        /// 商品名称
        /// </summary>
        [DBFieldInfo(ColumnName = "NAME", Required = false, IsPrimarykey = false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 商品副标题
        /// </summary>
        [DBFieldInfo(ColumnName = "TITLE", Required = false, IsPrimarykey = false)]
        public string title
        {
            get; set;
        }

        /// <summary>
        /// 商品货号
        /// </summary>
        [DBFieldInfo(ColumnName = "GOODS_NO", Required = true, IsPrimarykey = false)]
        public string goods_no
        {
            get; set;
        }

        /// <summary>
        /// 售卖单价
        /// </summary>
        [DBFieldInfo(ColumnName = "SELLING_PRICE", Required = false, IsPrimarykey = false)]
        public decimal selling_price
        {
            get; set;
        }

        /// <summary>
        /// 商品原价
        /// </summary>
        [DBFieldInfo(ColumnName = "ORIGINAL_PRICE", Required = false, IsPrimarykey = false)]
        public decimal original_price
        {
            get; set;
        }

        /// <summary>
        /// 是否热卖(100-是 110-不是)
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_HOT_STYLE", Required = false, IsPrimarykey = false)]
        public int is_hot_style
        {
            get; set;
        }

        /// <summary>
        /// 是否新款(100-是 110-不是)
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_NEW_STYLE", Required = false, IsPrimarykey = false)]
        public int is_new_style
        {
            get; set;
        }

        /// <summary>
        /// 是否推荐(100-是 110-不是)
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_RECOMMEND", Required = false, IsPrimarykey = false)]
        public int is_recommend
        {
            get; set;
        }

        /// <summary>
        /// 是否为特价商品(100-是 110-不是)
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_BARGAIN", Required = false, IsPrimarykey = false)]
        public int is_bargain
        {
            get; set;
        }

        /// <summary>
        /// 是否上架(100-是 110-不是)
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_SHELVES", Required = false, IsPrimarykey = false)]
        public int is_shelves
        {
            get; set;
        }

        /// <summary>
        /// 总销量
        /// </summary>
        [DBFieldInfo(ColumnName = "SALES_VOLUME", Required = true, IsPrimarykey = false)]
        public int sales_volume
        {
            get; set;
        }

        /// <summary>
        /// 添加时间
        /// </summary>
        [DBFieldInfo(ColumnName = "CREATET_TIME", Required = false, IsPrimarykey = false)]
        public DateTime create_time
        {
            get; set;
        }

        /// <summary>
        /// 商品详情描述,富文本框
        /// </summary>
        [DBFieldInfo(ColumnName = "INTRODUCTION", Required = true, IsPrimarykey = false)]
        public string introduction
        {
            get; set;
        }

        /// <summary>
        /// 商品排序
        /// </summary>
        [DBFieldInfo(ColumnName = "SORT", Required = false, IsPrimarykey = false)]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 是否被删除 100正常 110删除
        /// </summary>
        [DBFieldInfo(ColumnName = "IS_DELETED", Required = false, IsPrimarykey = false)]
        public int is_deleted
        {
            get; set;
        }
    }
}
