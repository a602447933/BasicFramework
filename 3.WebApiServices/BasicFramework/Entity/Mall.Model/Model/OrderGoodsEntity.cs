/*
* 命名空间: Mall.Model
*
* 功 能： OrderGoods实体类
*
* 类 名： OrderGoodsEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/10/30 11:36:42 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace Mall.Model
{
    using System;
    using Dapper.Library;

    /// <summary>
    /// 订单商品信息
    /// </summary>
    [Serializable]
    [DBTableInfo(Schema = "mall", TableName = "ORDER_GOODS")]
    public class OrderGoodsEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public OrderGoodsEntity()
        {

            //唯一标识符
            this.id = Guid.NewGuid().ToString().Replace("- ", "");

            //客户唯一标识符
            this.client_id = string.Empty;

            //订单唯一标识符
            this.order_id = string.Empty;

            //订单号
            this.order_no = string.Empty;

            //商品ID
            this.goods_id = string.Empty;

            //团购规格关键码
            this.standard_id = string.Empty;

            //商品名称
            this.goods_name = string.Empty;

            //团购图片
            this.goods_pic = string.Empty;

            //售卖单价
            this.selling_price = 0;

            //商品原价
            this.original_price = 0;

            //商品数量
            this.amount = 0;

            //分行字段
            this.substrings = string.Empty;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ID", Required = true, IsPrimarykey = true)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 客户唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "CLIENT_ID", Required = false, IsPrimarykey = false)]
        public string client_id
        {
            get; set;
        }

        /// <summary>
        /// 订单唯一标识符
        /// </summary>
        [DBFieldInfo(ColumnName = "ORDER_ID", Required = false, IsPrimarykey = false)]
        public string order_id
        {
            get; set;
        }

        /// <summary>
        /// 订单号
        /// </summary>
        [DBFieldInfo(ColumnName = "ORDER_NO", Required = false, IsPrimarykey = false)]
        public string order_no
        {
            get; set;
        }

        /// <summary>
        /// 商品ID
        /// </summary>
        [DBFieldInfo(ColumnName = "GOODS_ID", Required = false, IsPrimarykey = false)]
        public string goods_id
        {
            get; set;
        }

        /// <summary>
        /// 团购规格关键码
        /// </summary>
        [DBFieldInfo(ColumnName = "STANDARD_ID", Required = false, IsPrimarykey = false)]
        public string standard_id
        {
            get; set;
        }

        /// <summary>
        /// 商品名称
        /// </summary>
        [DBFieldInfo(ColumnName = "GOODS_NAME", Required = false, IsPrimarykey = false)]
        public string goods_name
        {
            get; set;
        }

        /// <summary>
        /// 团购图片
        /// </summary>
        [DBFieldInfo(ColumnName = "GOODS_PIC", Required = false, IsPrimarykey = false)]
        public string goods_pic
        {
            get; set;
        }

        /// <summary>
        /// 售卖单价
        /// </summary>
        [DBFieldInfo(ColumnName = "SELLING_PRICE", Required = false, IsPrimarykey = false)]
        public decimal selling_price
        {
            get; set;
        }

        /// <summary>
        /// 商品原价
        /// </summary>
        [DBFieldInfo(ColumnName = "ORIGINAL_PRICE", Required = false, IsPrimarykey = false)]
        public decimal original_price
        {
            get; set;
        }

        /// <summary>
        /// 商品数量
        /// </summary>
        [DBFieldInfo(ColumnName = "AMOUNT", Required = false, IsPrimarykey = false)]
        public int amount
        {
            get; set;
        }

        /// <summary>
        /// 分行字段
        /// </summary>
        [DBFieldInfo(ColumnName = "SUBSTRINGS", Required = false, IsPrimarykey = false)]
        public string substrings
        {
            get; set;
        }
    }
}
