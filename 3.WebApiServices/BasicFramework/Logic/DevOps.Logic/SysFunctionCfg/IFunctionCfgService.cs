﻿using System.Collections.Generic;
using DevOps.Logic;
using Common.Library;
using Common.Model;
using DevOps.Model;

namespace DevOps.Logic
{
    /// <summary>
    /// 菜单功能管理逻辑接口
    /// </summary>
    public interface IFunctionCfgService
    {

        #region 主界面菜单逻辑方法

        /// <summary>
        /// 获取用户对应系统的可操作的菜单信息
        /// </summary>
        /// <param name="appSecret">应用appSecret</param>
        /// <returns></returns>
        ResultJsonInfo<List<MenuInfo>> LoadMenuOfUserList(string appSecret);

        /// <summary>
        /// 获取用户对应系统的可操作的一级菜单信息
        /// </summary>
        /// <param name="appSecret">应用appSecret</param>
        /// <param name="level">层级级别 0：所有 10：一级 20：二级 30：三级</param>
        /// <returns></returns>
        ResultJsonInfo<List<SysFunctionCfgEntity>> LoadTopMenuOfUserList(string appSecret, int level);

        /// <summary>
        /// 获取用户对应系统的可操作的一级菜单信息【ExtraInfo返回pathname对应一级菜单节点ID】
        /// </summary>
        /// <param name="appSecret">应用appSecret</param>
        /// <param name="pathname">子节点相对地址</param>
        /// <param name="level">层级级别 0：所有 10：一级 20：二级 30：三级</param>
        /// <returns></returns>
        ResultJsonInfo<List<SysFunctionCfgEntity>> LoadTopMenuOfUserList(string appSecret,string pathname, int level);

        /// <summary>
        /// 获取左侧二级或三级菜单信息
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="appSecret">应用appSecret</param>
        /// <returns></returns>
        ResultJsonInfo<List<MenuInfo>> LoadSubMenus(string appSecret, string parentId);

        /// <summary>
        /// 获取用户可以操作的第一个菜单界面
        /// </summary>
        /// <param name="userInfo"></param>
        /// <param name="appSecret"></param>
        /// <returns></returns>
        string LoadFirstMenuOfUser(UserLoginInfo userInfo, string appSecret);

        #endregion

        #region 菜单基础信息管理

        #region 查询
        /// <summary>
        /// 根据菜单名称或系统类型获取菜单列表信息
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FunctionCfgInfoResponse>> LoadListInfo(ParametersInfo<FunctionCfgQueryRequest> queryInfo);

        /// <summary>
        /// 根据菜单名称或系统类型获取菜单树状列表信息
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<MenuInfo>> LoadTreeListInfo(FunctionCfgQueryRequest queryInfo);
        #endregion

        #region 更新
        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <param name="addInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> AddInfo(FunctionCfgAddRequest addInfo);

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Modify(FunctionCfgModifyRequest modifyInfo);

        /// <summary>
        /// 移动顺序操作
        /// </summary>
        /// <param name="moveInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> MoveSort(FunctionCfgMoveRequest moveInfo);

        /// <summary>
        /// 菜单启用与停用操作
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ForbiddenInfo(string id);

        /// <summary>
        /// 删除节点
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(string[] ids);

        #endregion

        #endregion

        #region 权限配置相关

        /// <summary>
        /// 根据角色ID，获取所有可操作菜单已赋权限情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FunctionCfgRoleMenuResponse>> LoadListInfoByRoleId(FunctionCfgRoleQuery queryInfo);

        /// <summary>
        /// 根据角色ID，获取所有可操作菜单已赋权限情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<MenuTreeInfo>> LoadTreeListByRoleId(FunctionCfgRoleQuery queryInfo);

        /// <summary>
        /// 修改对应角色赋值的菜单操作权限
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyRoleMenuInfo(FunctionCfgRoleModifyRequest modifyInfo);

        #endregion

        #region 公用功能

        /// <summary>
        /// 获取功能类型Select列表信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadFunctionTypeList();

        #endregion
    }
}
