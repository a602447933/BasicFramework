﻿using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 根据角色ID获取所有可操作菜单 已赋权限情况，请求实体
    /// </summary>
    public class FunctionCfgRoleQuery
    {

        /// <summary>
        /// 角色唯一标识符【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色唯一标识符")]
        public string roleId
        {
            get; set;
        }

    }
}
