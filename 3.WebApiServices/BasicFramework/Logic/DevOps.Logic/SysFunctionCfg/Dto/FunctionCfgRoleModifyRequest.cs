﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 修改对应角色赋值的菜单操作权限请求实体
    /// </summary>
    public class FunctionCfgRoleModifyRequest
    {
        /// <summary>
        ///角色唯一标识符
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色唯一标识符")]
        public string roleid
        {
            get; set;
        }

        /// <summary>
        ///菜单唯一标识符列表
        /// </summary>
        public List<string> menuIdList
        {
            get; set;
        }

    }
}
