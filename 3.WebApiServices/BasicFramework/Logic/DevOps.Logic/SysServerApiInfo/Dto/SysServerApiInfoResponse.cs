﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：接口服务相关逻辑返回实体
*
* 类 名： SysServerApiInfoResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/12/30 15:15:01  Harvey      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 接口服务相关逻辑返回实体
    /// </summary>
    public class SysServerApiInfoResponse
    {

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 接口服务名称
        /// </summary>
        public string server_name
        {
            get; set;
        }

        /// <summary>
        /// swagger地址
        /// </summary>
        public string swagger_url
        {
            get; set;
        }

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_time
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }
    }
}
