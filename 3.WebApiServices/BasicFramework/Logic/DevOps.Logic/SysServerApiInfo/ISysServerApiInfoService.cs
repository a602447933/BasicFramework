﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：接口服务相关逻辑接口
*
* 类 名： ISysServerApiInfoService
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/12/30 15:04:36               Harvey      创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 接口服务相关逻辑接口
    /// </summary>
    public interface ISysServerApiInfoService
    {
        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<SysServerApiInfoResponse>> LoadPageList(ParametersInfo<string> inputInfo);

        #endregion

        #region 更新

        /// <summary>
        /// 更新
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Save(SysServerApiInfoSaveRequest inputInfo);

        /// <summary>
        /// 删除接口服务
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);

        #endregion
    }
}
