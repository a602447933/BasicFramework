﻿using DevOps.Model;
using Common.Model;
using Redis.Library;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 参数管理Redis操作
    /// </summary>
    internal static class ParameterSettingServiceRedis
    {

        /// <summary>
        /// 将所有参数管理信息插入redis
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SaveAllParameterSetting(List<string> key, List<SysParameterSettingEntity> value)
        {
            RedisClient.GetRedisDb(RedisCommonInfo.RedisCommonDbLog).HashSets(RedisCommonInfo.RedisParameterSettingHashId, key, value);
        }


        /// <summary>
        /// 插入参数管理信息
        /// </summary>
        /// <param name="ParameterSettingInfo"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void SaveParameterSetting(SysParameterSettingEntity ParameterSettingInfo, Action successAction = null, Action failAction = null)
        {
            bool result = RedisClient.GetRedisDb(RedisCommonInfo.RedisCommonDbLog).HashSet(RedisCommonInfo.RedisParameterSettingHashId, ParameterSettingInfo.parameters_key, ParameterSettingInfo);
            if (result)
            {
                successAction?.Invoke();
            }
            else
            {
                failAction?.Invoke();
            }
        }


        /// <summary>
        /// 获取参数管理信息
        /// </summary>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void GetParameterSetting(Action<List<SysParameterSettingEntity>> successAction = null, Action failAction = null)
        {
            try
            {
                var resultInfo = RedisClient.GetRedisDb(RedisCommonInfo.RedisCommonDbLog).HashGetAll<SysParameterSettingEntity>(RedisCommonInfo.RedisParameterSettingHashId);
                successAction?.Invoke(resultInfo);
            }
            catch
            {
                failAction?.Invoke();
            }
        }


        /// <summary>
        /// 批量删除监控日志信息
        /// </summary>
        /// <param name="keys"></param>
        public static void RemoveParameterSetting(List<string> keys)
        {
            RedisClient.GetRedisDb(RedisCommonInfo.RedisCommonDbLog).HashDeletes(RedisCommonInfo.RedisParameterSettingHashId, keys);
        }


    }
}
