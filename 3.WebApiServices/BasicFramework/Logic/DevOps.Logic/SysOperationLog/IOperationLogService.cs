﻿using Common.Library;
using Common.Model;
using System;
using System.Collections.Generic;
/*
* 命名空间: DevOps.Logic
*
* 功 能： 操作记录日志逻辑接口
*
* 类 名： IOperationLogService
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 操作记录日志逻辑接口
    /// </summary>
    public interface IOperationLogService
    {

        #region 日志基础模块管理

        #region 查询
        /// <summary>
        /// 根据日志条件分页获取岗位列表 关键字【业务标题】【操作用户名称】【操作明细】
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<OperationLogInfoResponse>> LoadOperationList(ParametersInfo<OperationLogQuery> inputInfo);
        #endregion


        /// <summary>
        /// 获取日志类型
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadLogTypeList();


        /// <summary>
        /// 批量删除日志
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(PoerationLogRequest request);
        #endregion

        #region 主页操作日志相关逻辑

        /// <summary>
        /// 获取对应时间段内，操作日志数量
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        ResultJsonInfo<OperationCountResponse> LoadOperationCount(DateTime beginTime, DateTime endTime);

        /// <summary>
        /// 获取最新5条操作信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<OperationLogInfoResponse>> LoadLatestList();

        #endregion
    }
}
