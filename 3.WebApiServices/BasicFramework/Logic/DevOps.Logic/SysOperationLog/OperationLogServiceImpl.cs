﻿using System;
using System.Collections.Generic;
using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using DevOps.Model;
using Serialize.Library;

/*
* 命名空间: DevOps.Logic
*
* 功 能： 操作记录日志逻辑类
*
* 类 名： OperationLogServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/

namespace DevOps.Logic
{
    /// <summary>
    /// 操作记录日志逻辑类
    /// </summary>
    public class OperationLogServiceImpl : OperationLogicImpl, IOperationLogService
    {
        #region 日志基础模块管理

        #region 查询
        /// <summary>
        /// 根据日志条件分页获取列表 关键字【业务标题】【操作用户名称】【操作明细】
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<OperationLogInfoResponse>> LoadOperationList(ParametersInfo<OperationLogQuery> inputInfo)
        {

            var resultInfo = new ResultJsonInfo<List<OperationLogInfoResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysOperationLogInfoEntity>();

                if (inputInfo.parameters.func_type > 0)
                {
                    result.Where(p => p.func_type.Equals(inputInfo.parameters.func_type));
                }
                if (inputInfo.parameters.sKeyWords.IsNotNullOrEmpty())
                {
                    result.Where(a => a.business_title.Contains(inputInfo.parameters.sKeyWords) || a.detail.Contains(inputInfo.parameters.sKeyWords) || a.user_name.Contains(inputInfo.parameters.sKeyWords));
                }

                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = " create_date";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = " asc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);
                if (listInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<OperationLogInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }
        #endregion

        /// <summary>
        /// 获取日志类型Select列表信息
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<EnumToSelectItem>> LoadLogTypeList()
        {

            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();
            resultInfo.Data = EnumToSelectItem.GetEnumDescriptionList(typeof(OperationLogType), true);
            resultInfo.Code = ActionCodes.Success;
            resultInfo.Msg = "获取成功！";

            return resultInfo;
        }
        /// <summary>
        /// 批量删除日志
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(PoerationLogRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            var result = 0;

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var time = request.endTime.AddDays(1);
                result = con.CommandSet<SysOperationLogInfoEntity>().Where(p => p.create_date >= request.startTime && p.create_date < time)
                 .Delete();
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = result;
                    resultInfo.Msg = "操作成功！";

                    AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.LogManage, $"删除信息:{JsonHelper.ToJson(request)}");
                }
                else
                {
                    resultInfo.Msg = "操作失败！";
                }
            }
            return resultInfo;
        }
        #endregion


        #region 主页操作日志相关逻辑

        /// <summary>
        /// 获取对应时间段内，操作日志数量
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public ResultJsonInfo<OperationCountResponse> LoadOperationCount(DateTime beginTime, DateTime endTime)
        {

            var resultInfo = new ResultJsonInfo<OperationCountResponse>();
            //获取用户登录信息
            var user = GetLoginUserInfo();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysOperationLogInfoEntity>();

                if (beginTime == null)
                {
                    beginTime = DateTime.Now.AddDays(-7);
                }
                if (endTime == null)
                {
                    endTime = DateTime.Now;
                }
                var operationInfo = result.Where(p => p.create_date >= beginTime && p.create_date <= endTime  && p.user_code == user.id)//p.create_date.Between(beginTime, endTime)
                                        .ToList(p => new SysOperationLogInfoEntity()
                                        {
                                            id = p.id,
                                            func_type = p.func_type
                                        });
                if (operationInfo.Count > 0)
                {
                    resultInfo.Data = new OperationCountResponse()
                    {
                        operation_times = operationInfo.Count,
                        login_times = operationInfo.FindAll(p => p.func_type == (int)OperationLogType.LoginOperation).Count
                    };
                    resultInfo.Code = ActionCodes.Success;
                }
                else
                {
                    resultInfo.Data = new OperationCountResponse();
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }


        /// <summary>
        /// 获取最新5条操作信息
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<OperationLogInfoResponse>> LoadLatestList()
        {
            var resultInfo = new ResultJsonInfo<List<OperationLogInfoResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysOperationLogInfoEntity>();

                var listInfo = result.OrderByDescing(p=>p.create_date).PageList(1, 5);

                if (listInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<OperationLogInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        #endregion
    }
}
