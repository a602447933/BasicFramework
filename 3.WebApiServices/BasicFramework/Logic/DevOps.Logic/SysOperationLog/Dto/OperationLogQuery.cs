﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 日志查询条件类
    /// </summary>
    public class OperationLogQuery
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string sKeyWords
        {
            get; set;
        }
        /// <summary>
        /// 功能类型 100：插入操作  101：更新操作 102：删除操作
        /// </summary>
        public int func_type
        {
            get; set;
        }

    
    }

}
