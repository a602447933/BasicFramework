﻿using System;


/*
* 命名空间: DevOps.Logic
*
* 功 能： 消息列表返回信息【增加了一个是否是自己发送的信息字段】
*
* 类 名： MessageInfoResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/8 11:43:42 				Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 消息列表返回信息【增加了两个字段，是否是发送者，是否是接受者】
    /// </summary>
    public class MessageInfoResponse
    {

        /// <summary>
        /// 唯一编码
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 信息标题
        /// </summary>
        public string title
        {
            get; set;
        }

        /// <summary>
        /// 信息类型 100：对话信息 101：系统消息:
        /// </summary>
        public int msg_type
        {
            get; set;
        }

        /// <summary>
        /// 信息类容
        /// </summary>
        public string content
        {
            get; set;
        }

        /// <summary>
        /// 状态 100-未读，101-已读 ，102-删除
        /// </summary>
        public int status
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 发送者名称
        /// </summary>
        public string sender_name
        {
            get; set;
        }

        /// <summary>
        /// 发送者ID
        /// </summary>
        public string sender_id
        {
            get; set;
        }

        /// <summary>
        /// 接受者名称
        /// </summary>
        public string receiver_name
        {
            get; set;
        }

        /// <summary>
        /// 接受者唯一编码
        /// </summary>
        public string receiver_id
        {
            get; set;
        }

        /// <summary>
        /// 是否是发送者
        /// </summary>
        public bool is_sender
        {
            get; set;
        }

        /// <summary>
        /// 是否是接收者
        /// </summary>
        public bool is_receiver
        {
            get; set;
        }

    }
}
