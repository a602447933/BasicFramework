﻿using AutoMapper;
using DevOps.Model;

namespace DevOps.Logic
{
    /// <summary>
    /// 消息相关映射
    /// </summary>
    public class MessageProfile : Profile
    {
        /// <summary>
        /// 数据实体与传输实体映射
        /// </summary>
        public MessageProfile()
        {
            CreateMap<SysMessageEntity, MessageInfoResponse>();
        }
    }
}
