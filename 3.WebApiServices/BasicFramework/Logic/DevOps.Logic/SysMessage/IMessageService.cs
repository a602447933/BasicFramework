﻿using System;
using System.Collections.Generic;
using Common.Model;

/*
* 命名空间: DevOps.Logic
*
* 功 能： 消息处理接口类
*
* 类 名： IMessageService
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/8 9:44:36 				Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 消息处理接口类
    /// </summary>
    public interface IMessageService
    {

        #region 消息管理相关逻辑

        #region 查询

        /// <summary>
        /// 获取消息分页列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<MessageInfoResponse>> LoadPageList(ParametersInfo<MessageQuery> inputInfo);


        #endregion

        #region 更新

        /// <summary>
        /// 发送信息
        /// </summary>
        /// <param name="sendInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> SendMessage(MessageSendRequest sendInfo);

        /// <summary>
        /// 读取信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<MessageInfoResponse> ReadMessage(string id);

        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Modify(MessageModifyRequest modifyInfo);

        /// <summary>
        /// 物理删除消息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);
        #endregion

        #endregion


        #region 主页面消息相关逻辑

        /// <summary>
        /// 获取对应时间段类，自己的总的消息，未读消息数量
        /// </summary>
        /// <param name="beginTime">起始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        ResultJsonInfo<MessageMyselfCountResponse> LoadMyselfMessageCount(DateTime beginTime, DateTime endTime);

        /// <summary>
        /// 获取对应时间段内，自己未读消息
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        ResultJsonInfo<List<MessageInfoResponse>> LoadUnreadByTime(DateTime beginTime, DateTime endTime);

        #endregion

    }
}
