﻿using Common.Model;
using Document.Library;
using Encrypt.Library;
using Network.Library;
using Common.Library;
using Container.Library;
using Dapper.Library;
using System;
using System.Collections.Generic;
using Serialize.Library;
using System.Linq;
using System.Linq.Expressions;
using DevOps.Model;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

/*
* 命名空间: DevOps.Logic
*
* 功 能： 管理员操作逻辑类
*
* 类 名： SysUserServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 Harvey     创建
* V1.0.2    2020/03/18 11:12:00 Harvey     增加用户登录操作和主界面用户操作
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 管理员操作逻辑类
    /// </summary>
    public class SysUserServiceImpl : OperationLogicImpl, ISysUserService
    {

        #region 用户登录操作和主界面用户操作

        /// <summary>
        /// 钉钉移动端自动登录
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<UserLoginInfo> GetDingDingUserinfo(string code)
        {
            var resultInfo = new ResultJsonInfo<UserLoginInfo>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                string accessToken = GetDingAccessToken();
                var getUserid = "";// HttpProtocolHelper.SendGetRequestAsString(string.Format(DindinHttpUrls.GetConsumerUseridUrl, accessToken, code));
                JObject jObject2 = JObject.Parse(getUserid);
                string userid = jObject2["userid"].ToString();
                var postData = new
                {
                    language = "zh_CN",
                    userid = userid
                };
                var dingdingInfo = "";// HttpProtocolHelper.SendPostRequestData(string.Format(DindinHttpUrls.GetConsumerUserInfoUrl, accessToken), postData.ToJson());

                JObject jObject = JObject.Parse(dingdingInfo);
                string dingdingresult = jObject["result"].ToString();

                JObject jObject1 = JObject.Parse(dingdingresult);
                string mobile = jObject1["mobile"].ToString();


                var result = con.QuerySet<View_SysUserEntity>().Where(p => p.mobile_phone.Equals(mobile)).Get();
                if (result != null)
                {
                    if (result.is_valid)
                    {
                        var ticket = result.MapTo<UserLoginInfo>();
                        string refreshToken = Guid.NewGuid().ToString();
                        SysUserServiceRedis.SaveUserInfo(refreshToken, ticket, () =>
                        {
                            ticket.token = refreshToken;
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "操作成功";
                            resultInfo.Data = ticket;
                            AddOperationLog(OperationLogType.LoginOperation, BusinessTitleType.UserManage, $"钉钉移动端登录成功！", result.id);
                        },
                        () =>
                        {
                            resultInfo.Msg = "缓存用户信息失败！";
                            resultInfo.Code = ActionCodes.CacheFailure;
                        });
                    }
                    else
                    {
                        resultInfo.Msg = "用户已被冻结，请与管理员联系！";
                        resultInfo.Code = ActionCodes.InvalidOperation;
                    }
                }
                else
                {
                    resultInfo.Msg = "手机号错误！";
                }
            }
            return resultInfo;

        }




        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <returns></returns>
        public ResultFileInfo LoadValidateCode(string uniqueIdentifier)
        {
            var resultInfo = new ResultFileInfo();
            string code = ValidateCode.CreateValidateCode(4);
            string md5 = SecurityHelper.GetMD5Hash(uniqueIdentifier + HttpHelper.GetIP());

            SysUserServiceRedis.SaveValidateCode(md5, code, () =>
             {
                 byte[] bytes = ValidateCode.CreateValidateGraphic(code);
                 if (bytes.Length > 0)
                 {
                     resultInfo.Code = ActionCodes.Success;
                     resultInfo.FileContents = bytes;
                     resultInfo.ContentType = @"image/jpeg";
                     resultInfo.Msg = "获取成功";
                 }
             },
            () =>
            {
                resultInfo.Code = ActionCodes.CacheFailure;
                resultInfo.Msg = "获取失败";
            });
            return resultInfo;
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<UserLoginInfo> UserLogin(UserInfoLoginRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<UserLoginInfo>();
            //redis获取验证码
            string md5 = SecurityHelper.GetMD5Hash(inputInfo.uniqueIdentifier + HttpHelper.GetIP());

            SysUserServiceRedis.GetValidateCode(md5, (ValidateCode) =>
            {
                if (ValidateCode == inputInfo.vialdcode)
                {
                    using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
                    {
                        inputInfo.password = SecurityHelper.GetMD5Hash(inputInfo.password);

                        //先查询是否有该账号存在
                        var info = con.QuerySet<View_SysUserEntity>().Where(p => p.name == inputInfo.name).ToList();

                        if (info != null && info.Count > 0)
                        {
                            //再核对密码是否正确
                            var result = info.Find(p => p.name == inputInfo.name && p.password == inputInfo.password);
                            if (result != null)
                            {
                                if (result.is_valid)
                                {

                                    var ticket = result.MapTo<UserLoginInfo>();

                                    //string filesManageUrl = GepParameterSettingByKey(ParameterSettingInfo.FilesManageUrl);
                                    //ticket.picture_url = filesManageUrl + ticket.picture_url;
                                    //判断是否是超级管理员
                                    //ticket.is_super_admin = JudgeIsSuperAdmin(result.name);

                                    string refreshToken = Guid.NewGuid().ToString();
                                    SysUserServiceRedis.SaveUserInfo(refreshToken, ticket, () =>
                                    {
                                        ticket.token = refreshToken;
                                        resultInfo.Code = ActionCodes.Success;
                                        resultInfo.Msg = "操作成功";
                                        resultInfo.Data = ticket;

                                        AddOperationLog(OperationLogType.LoginOperation, BusinessTitleType.UserManage, $"登录成功！", result.id);
                                    },
                                    () =>
                                    {
                                        resultInfo.Msg = "缓存用户信息失败！";
                                        resultInfo.Code = ActionCodes.CacheFailure;
                                    });

                                }
                                else
                                {
                                    resultInfo.Msg = "用户已被冻结，请与管理员联系！";
                                    resultInfo.Code = ActionCodes.InvalidOperation;
                                }
                            }
                            else
                            {
                                resultInfo.Msg = "密码错误！";
                            }
                        }
                        else
                        {
                            resultInfo.Msg = "账号不存在！";
                            resultInfo.Code = ActionCodes.ObjectNotFound;
                        }
                    }
                }
                else
                {
                    resultInfo.Msg = "验证码不正确！";
                }
            }, () =>
            {
                resultInfo.Msg = "验证码失效，请重新获取！";
                resultInfo.Code = ActionCodes.InfoExpiration;
            });
            return resultInfo;
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<UserLoginInfo> LoadToken(string appSecret)
        {
            var resultInfo = new ResultJsonInfo<UserLoginInfo>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<View_SysUserEntity>().Where(p => p.id.Equals(appSecret)).Get();
                if (result != null)
                {
                    if (result.is_valid)
                    {
                        var ticket = result.MapTo<UserLoginInfo>();
                        string refreshToken = Guid.NewGuid().ToString();
                        SysUserServiceRedis.SaveUserInfo(refreshToken, ticket, () =>
                        {
                            ticket.token = refreshToken;
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "操作成功";
                            resultInfo.Data = ticket;
                            AddOperationLog(OperationLogType.LoginOperation, BusinessTitleType.UserManage, $"移动端登录成功！", result.id);
                        },
                        () =>
                        {
                            resultInfo.Msg = "缓存用户信息失败！";
                            resultInfo.Code = ActionCodes.CacheFailure;
                        });

                    }
                    else
                    {
                        resultInfo.Msg = "用户已被冻结，请与管理员联系！";
                        resultInfo.Code = ActionCodes.InvalidOperation;
                    }
                }
                else
                {
                    resultInfo.Msg = "appSecret错误！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 钉钉移动端登录
        /// </summary>
        /// <param name="mobile">手机号</param>
        /// <returns></returns>
        public ResultJsonInfo<UserLoginInfo> LoadTokenMobile(string mobile)
        {
            var resultInfo = new ResultJsonInfo<UserLoginInfo>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<View_SysUserEntity>().Where(p => p.mobile_phone.Equals(mobile)).Get();
                if (result != null)
                {
                    if (result.is_valid)
                    {
                        var ticket = result.MapTo<UserLoginInfo>();
                        string refreshToken = Guid.NewGuid().ToString();
                        SysUserServiceRedis.SaveUserInfo(refreshToken, ticket, () =>
                        {
                            ticket.token = refreshToken;
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "操作成功";
                            resultInfo.Data = ticket;
                            AddOperationLog(OperationLogType.LoginOperation, BusinessTitleType.UserManage, $"钉钉移动端登录成功！", result.id);
                        },
                        () =>
                        {
                            resultInfo.Msg = "缓存用户信息失败！";
                            resultInfo.Code = ActionCodes.CacheFailure;
                        });

                    }
                    else
                    {
                        resultInfo.Msg = "用户已被冻结，请与管理员联系！";
                        resultInfo.Code = ActionCodes.InvalidOperation;
                    }
                }
                else
                {
                    resultInfo.Msg = "手机号错误！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 客户端用户登录
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<UserLoginInfo> UserLogin(ClientSideLoginRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<UserLoginInfo>();

            //redis获取验证码
            string md5 = SecurityHelper.GetMD5Hash(inputInfo.uniqueIdentifier + HttpHelper.GetIP());
            SysUserServiceRedis.GetValidateCode(md5, (ValidateCode) =>
            {
                if (ValidateCode == inputInfo.vialdcode)
                {
                    using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
                    {
                        inputInfo.password = SecurityHelper.GetMD5Hash(inputInfo.password);
                        //先查询是否有该账号存在
                        var info = con.QuerySet<View_SysUserEntity>().Where(p => p.name == inputInfo.name).ToList();

                        if (info != null && info.Count > 0)
                        {
                            //再核对密码是否正确
                            var result = info.Find(p => p.name == inputInfo.name && p.password == inputInfo.password);
                            if (result != null)
                            {
                                if (result.is_valid)
                                {
                                    var ticket = result.MapTo<UserLoginInfo>();
                                    string refreshToken = Guid.NewGuid().ToString();
                                    SysUserServiceRedis.SaveUserInfo(refreshToken, ticket, () =>
                                    {
                                        ticket.token = refreshToken;
                                        resultInfo.Code = ActionCodes.Success;
                                        resultInfo.Msg = "操作成功";
                                        resultInfo.Data = ticket;

                                        AddOperationLog(OperationLogType.LoginOperation, BusinessTitleType.UserManage, $"登录成功！");
                                    },
                                    () =>
                                    {
                                        resultInfo.Msg = "缓存用户信息失败！";
                                        resultInfo.Code = ActionCodes.CacheFailure;
                                    });
                                }
                                else
                                {
                                    resultInfo.Msg = "用户已被冻结，请与管理员联系！";
                                    resultInfo.Code = ActionCodes.InvalidOperation;
                                }
                            }
                            else
                            {
                                resultInfo.Msg = "密码错误！";
                            }
                        }
                        else
                        {
                            resultInfo.Msg = "账号不存在！";
                            resultInfo.Code = ActionCodes.ObjectNotFound;
                        }
                    }
                }
                else
                {
                    resultInfo.Msg = "验证码不正确！";
                }
            }, () =>
            {
                resultInfo.Msg = "验证码失效，请重新获取！";
                resultInfo.Code = ActionCodes.InfoExpiration;
            });
            return resultInfo;
        }

        /// <summary>
        /// 通过RefreshToken获取用户信息
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<UserLoginInfo> LoadUserInfoByRefreshToken(UserInfoRefreshTokenRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<UserLoginInfo>();

            SysUserServiceRedis.GetUserInfo(inputInfo.refreshToken, (userInfo) =>
            {
                var userLoginInfo = JsonHelper.JsonToObject<UserLoginInfo>(userInfo);

                using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
                {
                    //先查询是否有该账号存在
                    var info = con.QuerySet<View_SysUserEntity>().Where(p => p.name == inputInfo.name && p.id == userLoginInfo.id).Get();

                    if (info != null)
                    {
                        if (info.is_valid)
                        {
                            var ticket = info.MapTo<UserLoginInfo>();

                            //判断是否是超级管理员
                            //ticket.is_super_admin = JudgeIsSuperAdmin(info.name);

                            string refreshToken = Guid.NewGuid().ToString();
                            SysUserServiceRedis.SaveUserInfo(refreshToken, ticket, () =>
                            {
                                ticket.token = refreshToken;
                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Msg = "操作成功";
                                resultInfo.Data = ticket;
                            },
                            () =>
                            {
                                resultInfo.Msg = "缓存用户信息失败！";
                                resultInfo.Code = ActionCodes.CacheFailure;
                            });
                        }
                        else
                        {
                            resultInfo.Msg = "用户已被冻结，请与管理员联系！";
                            resultInfo.Code = ActionCodes.InvalidOperation;
                        }
                    }
                    else
                    {
                        resultInfo.Msg = "账户不存在！";
                        resultInfo.Code = ActionCodes.ObjectNotFound;
                    }
                }
            },
            () =>
            {
                resultInfo.Msg = "RefreshToken无效，请重新登录！";
                resultInfo.Code = ActionCodes.RefreshTokeTokenInvalid;
            });
            return resultInfo;
        }

        /// <summary>
        /// 查询单个用户的数据
        /// 【先查询Redis,如果Redis没有，再查询数据库，最后将信息放入到Redis】
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultJsonInfo<UserLoginInfo> LoadSingleById(string userId)
        {
            var resultInfo = new ResultJsonInfo<UserLoginInfo>();

            SysUserServiceRedis.GetUserInfo(userId, (userInfo) =>
            {
                resultInfo.Code = ActionCodes.Success;
                resultInfo.Data = JsonHelper.JsonToObject<UserLoginInfo>(userInfo);
            },
            () =>
            {
                using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
                {
                    var result = con.QuerySet<View_SysUserEntity>().Where(a => a.id == userId).Get();
                    if (result != null)
                    {
                        var userInfo = result.MapTo<UserLoginInfo>();
                        //判断是否是超级管理员
                        //userInfo.is_super_admin = JudgeIsSuperAdmin(userInfo.name);
                        Task.Run(()=> {
                            SysUserServiceRedis.SaveUserInfo(userId, userInfo);
                        });
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Data = userInfo;
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.ObjectNotFound;
                        resultInfo.Msg = "账户不存在！";
                    }
                }
            });
            return resultInfo;
        }

        /// <summary>
        /// 修改用户自身的数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ModifyOneself(UserInfoModifyOneselfRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            var user = GetLoginUserInfo();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var userinfo = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == user.id).Get();
                if (userinfo != null)
                {
                    userinfo.email = request.email;
                    userinfo.realname = request.realname;
                    userinfo.qq = request.qq;
                    userinfo.mobile_phone = request.mobile_phone;
                    userinfo.telphone = request.telphone;
                    userinfo.office_phone = request.office_phone;
                    userinfo.idnumber = request.idnumber;
                    userinfo.picture_url = request.picture_url;
                    userinfo.signature_url = request.signature_url;
                    userinfo.gender = request.gender;
                    userinfo.nickname = request.nickname;
                    userinfo.remarks = request.remarks;
                    userinfo.user_describe = request.user_describe;
                    userinfo.modifier_id = user.id;
                    userinfo.modifier_date = DateTime.Now;

                    var result = con.CommandSet<SysUserInfoEntity>().Update(userinfo);
                    if (result > 0)
                    {

                        #region 修改存在redis中的信息
                        var info = con.QuerySet<View_SysUserEntity>().Where(p => p.id == userinfo.id).Get();
                        if (info != null)
                        {
                            var ticket = info.MapTo<UserLoginInfo>();
                            SysUserServiceRedis.SaveUserInfo(ticket);
                        }
                        #endregion

                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "修改成功";
                        AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"修改用户自身的数据成功，修改用户数据：{JsonHelper.ToJson(userinfo)}");
                    }
                    else
                    {
                        resultInfo.Msg = "修改失败！";
                    }
                }
                else
                {
                    resultInfo.Msg = "参数无效！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 修改自己的密码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ChangePassword(UserChangePasswordRequest request)
        {
            var user = GetLoginUserInfo();
            var resultInfo = new ResultJsonInfo<int>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var userInfo = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == user.id).Get();
                if (userInfo == null)
                {
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
                if (userInfo.password != SecurityHelper.GetMD5Hash(request.oldpassword))
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "旧密码错误！";
                    return resultInfo;
                }
                userInfo.password = SecurityHelper.GetMD5Hash(request.password);

                var result = con.CommandSet<SysUserInfoEntity>().Update(userInfo);
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "修改成功";

                    AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"修改自己的密码成功！");
                }
                else
                {
                    resultInfo.Msg = "修改失败";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 修改用户自身的主题
        /// </summary>
        /// <param name="themeCss"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ModifyTheme(string themeCss)
        {
            var resultInfo = new ResultJsonInfo<int>();

            var user = GetLoginUserInfo();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var userinfo = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == user.id).Get();
                if (userinfo != null)
                {

                    userinfo.modifier_id = user.id;
                    userinfo.modifier_date = DateTime.Now;
                    userinfo.theme = themeCss;
                    var result = con.CommandSet<SysUserInfoEntity>().Update(userinfo);
                    if (result > 0)
                    {
                        #region 修改存在redis中的信息
                        var info = con.QuerySet<View_SysUserEntity>().Where(p => p.id == userinfo.id).Get();
                        if (info != null)
                        {
                            var ticket = info.MapTo<UserLoginInfo>();
                            SysUserServiceRedis.SaveUserInfo(ticket);
                        }
                        #endregion

                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "修改成功";
                        AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"修改用户自身的主题成功，修改用户数据：{JsonHelper.ToJson(userinfo)}");
                    }
                    else
                    {
                        resultInfo.Msg = "修改失败！";
                    }
                }
                else
                {
                    resultInfo.Msg = "参数无效！";
                }
            }
            return resultInfo;
        }
        /// <summary>
        /// 获取用户自身的主题
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<string> GetMyTheme()
        {
            var resultInfo = new ResultJsonInfo<string>();
            resultInfo.Code = ActionCodes.Success;
            resultInfo.Data = GetLoginUserInfo().theme;
            resultInfo.Msg = "获取成功";
            return resultInfo;
        }

        #endregion

        #region 用户基础信息管理操作

        #region 查询

        /// <summary>
        /// 根据条件分页查询用户数据
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<UserInfoResponse>> LoadList(ParametersInfo<UserInfoQuery> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<View_SysUserEntity>();

                if (inputInfo.parameters.sKeyWords.IsNotNullOrEmpty())
                {
                    result.Where(a => a.name.Contains(inputInfo.parameters.sKeyWords)
                    || a.mobile_phone.Contains(inputInfo.parameters.sKeyWords)
                    || a.post_names.Contains(inputInfo.parameters.sKeyWords)
                    || a.department_names.Contains(inputInfo.parameters.sKeyWords)
                    || a.realname.Contains(inputInfo.parameters.sKeyWords)
                    );
                }

                #region 表达式拼装 或OrElse

                if (inputInfo.parameters.postIds.IsNotNullOrEmpty())
                {
                    var postIds = inputInfo.parameters.postIds.Split(',');

                    ParameterExpression pa = Expression.Parameter(typeof(View_SysUserEntity), "user");
                    MyExpressionVisitor visitor = new MyExpressionVisitor(pa);//统一参数类型

                    Expression<Func<View_SysUserEntity, bool>> lambdaOne = user => user.post_ids.Contains(postIds[0]);
                    Expression lambdaOneBody = visitor.Visit(lambdaOne.Body);

                    foreach (var item in postIds)
                    {
                        Expression<Func<View_SysUserEntity, bool>> lambdaItem = user => user.post_ids.Contains(item);
                        Expression lambdaBody = visitor.Visit(lambdaItem.Body);

                        lambdaOneBody = Expression.OrElse(lambdaOneBody, lambdaBody);
                    }
                    Expression<Func<View_SysUserEntity, bool>> newEx = Expression.Lambda<Func<View_SysUserEntity, bool>>(lambdaOneBody, pa);
                    result.Where(newEx);
                }
                #endregion

                //根据岗位查询
                if (inputInfo.parameters.postIds.IsNotNullOrEmpty())
                {
                    result.Where(a => a.post_ids.Contains(inputInfo.parameters.postIds));
                }
                //根据部门查询
                if (inputInfo.parameters.departmentId.IsNotNullOrEmpty())
                {
                    var departmentResult = con.QuerySet<SysDepartmentInfoEntity>().Where(P => P.is_deleted == false).ToList();
                    var department_ids = departmentResult.GetChildInfo(inputInfo.parameters.departmentId).ToArray();

                    result.Where(a => a.department_ids.In(department_ids)|| a.department_ids.Contains(inputInfo.parameters.departmentId));
                }

                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = " sort";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = " asc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);

                if (listInfo.Items.Count > 0)
                {
                    //string filesManageUrl = GepParameterSettingByKey(ParameterSettingInfo.FilesManageUrl);
                    //foreach (var item in listInfo.Items)
                    //{
                    //    item.picture_url = filesManageUrl + item.picture_url;
                    //}
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<UserInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Data = new List<UserInfoResponse>();
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 查询所有用户数据
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<UserInfoResponse>> LoadAllList()
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var listInfo = con.QuerySet<View_SysUserEntity>().ToList();
                if (listInfo.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.MapToList<UserInfoResponse>();
                }
                else
                {
                    resultInfo.Data = new List<UserInfoResponse>();
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 根据账号查询单个用户的数据
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        public ResultJsonInfo<UserInfoResponse> LoadSingleName(string loginName)
        {
            var resultInfo = new ResultJsonInfo<UserInfoResponse>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysUserInfoEntity>().Where(a => a.name == loginName).Get();

                if (result != null)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = result.MapTo<UserInfoResponse>();
                }
                else
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
            }
            return resultInfo;

        }

        /// <summary>
        /// 查询单个用户的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultJsonInfo<UserInfoResponse> LoadSingle(string userId)
        {
            var resultInfo = new ResultJsonInfo<UserInfoResponse>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == userId).Get();

                if (result != null)
                {
                    //string filesManageUrl = GepParameterSettingByKey(ParameterSettingInfo.FilesManageUrl);
                    //result.picture_url = filesManageUrl + result.picture_url;

                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = result.MapTo<UserInfoResponse>();
                }
                else
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
            }
            return resultInfo;

        }
        
        /// <summary>
        /// 查询自己的数据视图版
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<UserInfoResponse> LoadUserViewSingle()
        {
            var user = GetLoginUserInfo();
            var resultInfo = new ResultJsonInfo<UserInfoResponse>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<View_SysUserEntity>().Where(a => a.id == user.id).Get();
                if (result != null)
                {

                    //string filesManageUrl = GepParameterSettingByKey(ParameterSettingInfo.FilesManageUrl);
                    //result.picture_url = filesManageUrl + result.picture_url;

                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "获取成功";
                    resultInfo.Data = result.MapTo<UserInfoResponse>();
                }
                else
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 查询自己的数据
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<UserInfoResponse> LoadUserSingle()
        {
            var user = GetLoginUserInfo();
            var resultInfo = new ResultJsonInfo<UserInfoResponse>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == user.id).Get();
                if (result != null)
                {

                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "获取成功";
                    resultInfo.Data = result.MapTo<UserInfoResponse>();
                }
                else
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
            }
            return resultInfo;
        }

        #endregion

        #region 添加

        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="userAdd"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> AddUser(UserInfoAddRequest userAdd)
        {
            var resultInfo = new ResultJsonInfo<int>();

            var user = GetLoginUserInfo();

            //判断两次密码是否输入一致
            if (userAdd.password == userAdd.repassword)
            {
                using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
                {
                    //事务功能
                    con.Transaction(tran =>
                    {
                        //判断是否存在相同登录名
                        var userInfoList = tran.QuerySet<SysUserInfoEntity>().ToList();
                        var userInfo = userInfoList.Find(p => p.name == userAdd.name);
                        if (userInfo == null)
                        {
                            //插入数据
                            var sysUser = userAdd.MapTo<SysUserInfoEntity>();
                            sysUser.password = SecurityHelper.GetMD5Hash(userAdd.password);
                            sysUser.creator_id = user.id;
                            sysUser.creator_name = user.name;
                            sysUser.modifier_date = DateTime.Now;
                            sysUser.modifier_id = user.id;
                            sysUser.modifier_name = user.name;
                            sysUser.sort = userInfoList.Max(p => p.sort) + 1;


                            var result = tran.CommandSet<SysUserInfoEntity>().Insert(sysUser);

                            if (userAdd.departmentList.Count > 0)
                            {
                                SysUserDepartmentReEntity departmentReEntity = null;
                                //配置部门
                                foreach (var item in userAdd.departmentList)
                                {
                                    departmentReEntity = new SysUserDepartmentReEntity();
                                    departmentReEntity.id = GuidHelper.GetGuid();
                                    departmentReEntity.user_id = sysUser.id;
                                    departmentReEntity.department_id = item;
                                    tran.CommandSet<SysUserDepartmentReEntity>().Insert(departmentReEntity);
                                }
                            }

                            if (userAdd.postList.Count > 0)
                            {
                                SysUserPostReEntity userPostReEntity = null;
                                //配置职位
                                foreach (var item in userAdd.postList)
                                {
                                    userPostReEntity = new SysUserPostReEntity();
                                    userPostReEntity.id = GuidHelper.GetGuid();
                                    userPostReEntity.user_id = sysUser.id;
                                    userPostReEntity.post_id = item;
                                    tran.CommandSet<SysUserPostReEntity>().Insert(userPostReEntity);
                                }
                            }
                            if (userAdd.roleList.Count > 0)
                            {
                                SysUserRoleReEntity userRoleReEntity = null;
                                //配置角色
                                foreach (var item in userAdd.roleList)
                                {
                                    userRoleReEntity = new SysUserRoleReEntity();
                                    userRoleReEntity.id = GuidHelper.GetGuid();
                                    userRoleReEntity.user_id = sysUser.id;
                                    userRoleReEntity.role_id = item;
                                    tran.CommandSet<SysUserRoleReEntity>().Insert(userRoleReEntity);
                                }
                            }

                            if (result > 0)
                            {
                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Msg = "添加成功！";
                                AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.UserManage, $"添加用户成功,新增用户信息：{JsonHelper.ToJson(sysUser)}");
                            }
                            else
                            {
                                resultInfo.Msg = "添加失败！";
                            }
                        }
                        else
                        {
                            resultInfo.Msg = "添加失败，存在相同登录名！";
                        }
                    }, ex =>
                    {
                        throw ex;
                    });
                }
            }
            else
            {
                resultInfo.Msg = "两次输入密码不一致！";
            }
            return resultInfo;
        }

        /// <summary>
        /// 同步编辑用户
        /// </summary>
        /// <param name="userAdd"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> SynModifyUser(UserInfoRequest userAdd)
        {
            var resultInfo = new ResultJsonInfo<int>();

            var user = GetLoginUserInfo();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //事务功能
                con.Transaction(tran =>
                {
                    
                    var info = tran.QuerySet<SysUserInfoEntity>().Where(a => a.id.Equals(userAdd.id)).Get();

                    if (info != null)//修改
                    {
                        info.idnumber = userAdd.idnumber;
                        info.realname = userAdd.realname;
                        info.email = userAdd.email;
                        info.gender = userAdd.gender;
                        info.mobile_phone = userAdd.mobile_phone;
                        info.modifier_date = DateTime.Now;
                        info.modifier_id = user.id;
                        info.modifier_name = user.name;
                        if (info.name != userAdd.name && userAdd.name.IsNotNullOrEmpty())
                        {
                            info.name = userAdd.name;
                            var newPasw = SecurityHelper.GetMD5Hash(userAdd.name + "123").ToLower();
                            info.password = SecurityHelper.GetMD5Hash(newPasw);
                        }
                        var modyResult = tran.CommandSet<SysUserInfoEntity>().Update(info);
                        if (modyResult > 0)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "修改成功！";
                            AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"修改用户成功,修改用户信息：{JsonHelper.ToJson(info)}");
                        }
                        else
                        {
                            resultInfo.Msg = "修改失败！";
                        }
                    }
                    else //新增
                    {
                        //判断是否存在相同登录名
                        var userInfoList = tran.QuerySet<SysUserInfoEntity>().ToList();
                        var ifExistUserNameResult = IfExistUserName(userAdd.name);
                        //插入数据
                        var sysUser = userAdd.MapTo<SysUserInfoEntity>();
                        int namNum = 0;
                        var rName = userAdd.name;
                        while (ifExistUserNameResult.Code == ActionCodes.Success && ifExistUserNameResult.Data == true)
                        {
                            namNum = namNum + 1;
                            rName = userAdd.name + namNum;
                            ifExistUserNameResult = IfExistUserName(rName);
                        }
                        sysUser.name = rName;
                        var newPasw = SecurityHelper.GetMD5Hash(sysUser.name + "123").ToLower();
                        sysUser.password = SecurityHelper.GetMD5Hash(newPasw);
                        sysUser.creator_id = user.id;
                        sysUser.is_valid = true;
                        sysUser.creator_name = user.name;
                        sysUser.modifier_date = DateTime.Now;
                        sysUser.modifier_id = user.id;
                        sysUser.modifier_name = user.name;
                        sysUser.sort = userInfoList.Max(p => p.sort) + 1;
                        var result = tran.CommandSet<SysUserInfoEntity>().Insert(sysUser);

                        SysUserRoleReEntity userRoleReEntity = null;
                        userRoleReEntity = new SysUserRoleReEntity();
                        userRoleReEntity.id = GuidHelper.GetGuid();
                        userRoleReEntity.user_id = sysUser.id;
                        //userRoleReEntity.role_id = GepParameterSettingByKey(ParameterSettingInfo.RoleCommon);
                        tran.CommandSet<SysUserRoleReEntity>().Insert(userRoleReEntity);

                        if (result > 0)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "添加成功！";
                            AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.UserManage, $"添加用户成功,新增用户信息：{JsonHelper.ToJson(sysUser)}");
                        }
                        else
                        {
                            resultInfo.Msg = "添加失败！";
                        }
                    }
                    #region 同步到部门/岗位
                    //配置部门
                    //清空该用户之前保存的部门信息
                    tran.CommandSet<SysUserDepartmentReEntity>().Where(a => a.user_id.Contains(userAdd.id)).Delete();
                    if (userAdd.department_id.IsNotNullOrEmpty())
                    {
                        SysUserDepartmentReEntity departmentReEntity= new SysUserDepartmentReEntity();
                        departmentReEntity.id = GuidHelper.GetGuid();
                        departmentReEntity.user_id = userAdd.id;
                        departmentReEntity.department_id = userAdd.department_id;
                        tran.CommandSet<SysUserDepartmentReEntity>().Insert(departmentReEntity);

                    }
                    //清空岗位
                    tran.CommandSet<SysUserPostReEntity>().Where(a => a.user_id.Contains(userAdd.id)).Delete();
                    var  postList =  userAdd.post_ids.Split(',');                 
                    if (postList.Length > 0)
                    {
                        SysUserPostReEntity userPostReEntity = null;
                        //配置职位
                        for (var j=0;j< postList.Length;j++)
                        {
                            userPostReEntity = new SysUserPostReEntity();
                            userPostReEntity.id = GuidHelper.GetGuid();
                            userPostReEntity.user_id = userAdd.id;
                            userPostReEntity.post_id = postList[j];
                            tran.CommandSet<SysUserPostReEntity>().Insert(userPostReEntity);
                        }
                    }
                    #endregion
                }, ex =>
                {
                    throw ex;
                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 同步编辑用户名称和手机号
        /// </summary>
        /// <param name="userAdd"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> SynUserNameAndMobile(UserInfoRequest userAdd)
        {
            var resultInfo = new ResultJsonInfo<int>();

            var user = GetLoginUserInfo();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //事务功能
                con.Transaction(tran =>
                {

                    var info = tran.QuerySet<SysUserInfoEntity>().Where(a => a.id.Equals(userAdd.id)).Get();
                    if (info != null)//修改
                    {
                        info.realname = userAdd.realname;
                        info.mobile_phone = userAdd.mobile_phone;
                        info.modifier_date = DateTime.Now;
                        info.modifier_id = user.id;
                        info.modifier_name = user.name;
                        if (userAdd.realname.Contains("(离职)"))
                        {
                            info.is_valid = false;
                            info.is_deleted = true;
                        }

                        #region 部门操作
                        //先删除用户-部门关联信息
                        tran.CommandSet<SysUserDepartmentReEntity>().Where(p => p.user_id == userAdd.id).Delete();
                        var departmentResult = tran.QuerySet<SysDepartmentInfoEntity>().Where(p => p.is_deleted == false && p.name.Equals(userAdd.department_name)).Get();
                        //插入用户-部门关联信息
                        SysUserDepartmentReEntity userDepartment = null;
                        if (userAdd.department_name != null && departmentResult != null)
                        {
                            userDepartment = new SysUserDepartmentReEntity();
                            userDepartment.id = GuidHelper.GetGuid();
                            userDepartment.user_id = userAdd.id;
                            userDepartment.department_id = departmentResult.id;
                            tran.CommandSet<SysUserDepartmentReEntity>().Insert(userDepartment);
                        }
                        #endregion

                        var modyResult = tran.CommandSet<SysUserInfoEntity>().Update(info);
                        if (modyResult > 0)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "修改成功！";
                            AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"修改用户成功,修改用户信息：{JsonHelper.ToJson(info)}");
                        }
                        else
                        {
                            resultInfo.Msg = "修改失败！";
                        }
                    }
                    else //新增
                    {
                        //判断是否存在相同登录名
                        var userInfoList = tran.QuerySet<SysUserInfoEntity>().ToList();
                        var ifExistUserNameResult = IfExistUserName(userAdd.name);
                        //插入数据
                        var sysUser = userAdd.MapTo<SysUserInfoEntity>();
                        int namNum = 0;
                        var rName = userAdd.name;
                        while (ifExistUserNameResult.Code == ActionCodes.Success && ifExistUserNameResult.Data == true)
                        {
                            namNum = namNum + 1;
                            rName = userAdd.name + namNum;
                            ifExistUserNameResult = IfExistUserName(rName);
                        }
                        sysUser.name = rName;
                        var newPasw = SecurityHelper.GetMD5Hash(sysUser.name + "123").ToLower();
                        sysUser.password = SecurityHelper.GetMD5Hash(newPasw);
                        sysUser.creator_id = user.id;
                        sysUser.is_valid = true;
                        sysUser.creator_name = user.name;
                        sysUser.modifier_date = DateTime.Now;
                        sysUser.modifier_id = user.id;
                        sysUser.modifier_name = user.name;
                        sysUser.sort = userInfoList.Max(p => p.sort) + 1;
                        sysUser.gender = "男";
                        sysUser.idnumber = "-";
                        var result = tran.CommandSet<SysUserInfoEntity>().Insert(sysUser);

                        SysUserRoleReEntity userRoleReEntity = null;
                        userRoleReEntity = new SysUserRoleReEntity();
                        userRoleReEntity.id = GuidHelper.GetGuid();
                        userRoleReEntity.user_id = sysUser.id;
                        //userRoleReEntity.role_id = GepParameterSettingByKey(ParameterSettingInfo.RoleCommon);
                        tran.CommandSet<SysUserRoleReEntity>().Insert(userRoleReEntity);

                        #region 部门操作
                        var departmentResult = tran.QuerySet<SysDepartmentInfoEntity>().Where(p => p.is_deleted == false && p.name.Equals(userAdd.department_name)).Get();
                        //插入用户-部门关联信息
                        SysUserDepartmentReEntity userDepartment = null;
                        if (userAdd.department_name != null && departmentResult != null)
                        {
                            userDepartment = new SysUserDepartmentReEntity();
                            userDepartment.id = GuidHelper.GetGuid();
                            userDepartment.user_id = userAdd.id;
                            userDepartment.department_id = departmentResult.id;
                            tran.CommandSet<SysUserDepartmentReEntity>().Insert(userDepartment);
                        }

                        #endregion

                        if (result > 0)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "添加成功！";
                            AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.UserManage, $"添加用户成功,新增用户信息：{JsonHelper.ToJson(sysUser)}");
                        }
                        else
                        {
                            resultInfo.Msg = "添加失败！";
                        }
                    }

                }, ex =>
                {
                    throw ex;
                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 批量同步上传用户
        /// </summary>
        /// <param name="userAddList"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> SynUploadUser(List<UserLoginInfo> userAddList)
        {
            var resultInfo = new ResultJsonInfo<int>();
            var user = GetLoginUserInfo();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //事务功能
                con.Transaction(tran =>
                {
                    var userInfoList = tran.QuerySet<SysUserInfoEntity>().ToList();
                    var sort = userInfoList.Max(p => p.sort);
                    for (var i = 0; i < userAddList.Count; i++)
                    {
                        //判断是否存在相同登录名

                        var ifExistUserNameResult = IfExistUserName(userAddList[i].name);
                        //插入数据
                        SysUserInfoEntity sysUser = new SysUserInfoEntity();
                        sysUser.id = userAddList[i].id;
                        sysUser.name = StringHelper.GetFirstPinyin(userAddList[i].name);
                        sysUser.mobile_phone = userAddList[i].mobile_phone;
                        sysUser.realname = userAddList[i].realname;
                        sysUser.email = userAddList[i].email;
                        sysUser.gender = userAddList[i].gender;
                        sysUser.idnumber = userAddList[i].idnumber;
                        int namNum = 0;
                        var rName = userAddList[i].name;
                        while (ifExistUserNameResult.Code == ActionCodes.Success && ifExistUserNameResult.Data == true)
                        {
                            namNum = namNum + 1;
                            rName = userAddList[i].name + namNum;
                            ifExistUserNameResult = IfExistUserName(rName);
                        }
                        sysUser.name = rName;
                        var newPasw = SecurityHelper.GetMD5Hash(sysUser.name + "123").ToLower();
                        sysUser.password = SecurityHelper.GetMD5Hash(newPasw);
                        sysUser.creator_id = user.id;
                        sysUser.is_valid = true;
                        sysUser.creator_name = user.name;
                        sysUser.modifier_date = DateTime.Now;
                        sysUser.modifier_id = user.id;
                        sysUser.modifier_name = user.name;
                        sysUser.sort = sort + i + 1;
                        var result = tran.CommandSet<SysUserInfoEntity>().Insert(sysUser);
                        #region 配置部门
                        //配置部门
                        if (userAddList[i].department_ids.IsNotNullOrEmpty())
                        {
                            SysUserDepartmentReEntity departmentReEntity = new SysUserDepartmentReEntity();
                            departmentReEntity.id = GuidHelper.GetGuid();
                            departmentReEntity.user_id = sysUser.id;
                            departmentReEntity.department_id = userAddList[i].department_ids;
                            tran.CommandSet<SysUserDepartmentReEntity>().Insert(departmentReEntity);
                        }
                        #endregion
                        #region 配置岗位
                        var postList = userAddList[i].post_ids.Split(',');

                        if (postList.Length > 0)
                        {
                            SysUserPostReEntity userPostReEntity = null;
                            //配置职位
                            for (var j = 0; j < postList.Length; j++)
                            {
                                userPostReEntity = new SysUserPostReEntity();
                                userPostReEntity.id = GuidHelper.GetGuid();
                                userPostReEntity.user_id = sysUser.id;
                                userPostReEntity.post_id = postList[j];
                                tran.CommandSet<SysUserPostReEntity>().Insert(userPostReEntity);
                            }
                        }
                        #endregion
                        SysUserRoleReEntity userRoleReEntity = null;
                        userRoleReEntity = new SysUserRoleReEntity();
                        userRoleReEntity.id = GuidHelper.GetGuid();
                        userRoleReEntity.user_id = sysUser.id;
                        //userRoleReEntity.role_id = GepParameterSettingByKey(ParameterSettingInfo.RoleCommon);
                        tran.CommandSet<SysUserRoleReEntity>().Insert(userRoleReEntity);
                    }
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "添加成功！";
                    AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.UserManage, $"添加用户成功,新增用户信息：{JsonHelper.ToJson(userAddList)}");
                }, ex =>
                {
                    throw ex;
                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 判断用户名是否存在。
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ResultJsonInfo<bool> IfExistUserName(string name)
        {
            var resultInfo = new ResultJsonInfo<bool>();

            var user = GetLoginUserInfo();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {

                //判断是否存在相同登录名
                var userInfoList = con.QuerySet<SysUserInfoEntity>().ToList();
                var userInfo = userInfoList.Find(p => p.name == name);
                if (userInfo == null)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = false;
                }
                else
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = true;
                }
            }
            return resultInfo;
        }
        /// <summary>
        /// 用户配置部门
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> UserDepartment(UserInfoDepartmentRequest department)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var userInfo = con.QuerySet<SysUserInfoEntity>().Where(p => p.id == department.userId).Get();
                if (!userInfo.is_valid)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "该用户被禁用";
                    return resultInfo;
                }
                var depInfo = con.QuerySet<SysDepartmentInfoEntity>().Where(p => p.id == department.departmentId).Get();
                if (depInfo == null)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "该部门不存在";
                    return resultInfo;
                }
                //事务功能
                con.Transaction(tran =>
                {
                    //删除原来配置的数据
                    var delete = tran.CommandSet<SysUserDepartmentReEntity>().Where(a => a.user_id == userInfo.id).Delete();

                    SysUserDepartmentReEntity reEntity = new SysUserDepartmentReEntity
                    {
                        id = GuidHelper.GetGuid(),
                        user_id = department.userId,
                        department_id = department.departmentId
                    };
                    var result = tran.CommandSet<SysUserDepartmentReEntity>()
                        .Insert(reEntity);
                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "配置成功！";
                        AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.UserManage, "配置用户部门成功");
                    }
                    else
                    {
                        resultInfo.Msg = "配置失败！";
                    }
                }, ex =>
                {
                    throw ex;
                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 用户配置岗位
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> UserPost(UserInfoPositionRequset position)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var userInfo = con.QuerySet<SysUserInfoEntity>().Where(p => p.id == position.userId).Get();
                if (!userInfo.is_valid)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "该用户被禁用";
                    return resultInfo;
                }
                var depInfo = con.QuerySet<SysPostInfoEntity>().Where(p => p.id == position.postId).Get();
                if (depInfo == null)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "该岗位不存在";
                    return resultInfo;
                }
                //事务功能
                con.Transaction(tran =>
                {
                    //删除原来配置的数据
                    var delete = tran.CommandSet<SysUserPostReEntity>().Where(a => a.user_id == userInfo.id).Delete();

                    SysUserPostReEntity reEntity = new SysUserPostReEntity
                    {
                        id = GuidHelper.GetGuid(),
                        user_id = position.userId,
                        post_id = position.postId
                    };
                    var result = tran.CommandSet<SysUserPostReEntity>()
                        .Insert(reEntity);
                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "配置成功！";
                        AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.UserManage, "配置用户岗位成功");
                    }
                    else
                    {
                        resultInfo.Msg = "配置失败！";
                    }
                }, ex =>
                {
                    throw ex;
                });
            }
            return resultInfo;
        }

        #endregion

        #region 修改

        /// <summary>
        /// 修改用户电话号码数据
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mobilePhone"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ModifyMobilePhone(string id,string mobilePhone)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //添加功能，直接添加
                con.Transaction(tran =>
                {
                    var userinfo = tran.QuerySet<SysUserInfoEntity>().Where(a => a.id == id).Get();
                    if (userinfo != null)
                    {
                        userinfo.mobile_phone = mobilePhone;
                        tran.CommandSet<SysUserInfoEntity>().Update(userinfo);

                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "操作成功";
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.ArgumentInvalid;
                        resultInfo.Msg = "参数无效！";
                    }
                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 修改用户数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Modify(UserInfoModifyRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //添加功能，直接添加
                con.Transaction(tran =>
                {
                    var user = GetLoginUserInfo();
                    request.id = request.id.IsNullOrEmpty() ? user.id : request.id;

                    var userinfo = tran.QuerySet<SysUserInfoEntity>().Where(a => a.id == request.id).Get();

                    userinfo.modifier_id = user.id;
                    userinfo.picture_url = request.picture_url;
                    userinfo.email = request.email;
                    userinfo.gender = request.gender;
                    userinfo.idnumber = request.idnumber;
                    userinfo.mobile_phone = request.mobile_phone;
                    userinfo.nickname = request.nickname;
                    userinfo.office_phone = request.office_phone;
                    userinfo.qq = request.qq;
                    userinfo.realname = request.realname;
                    userinfo.user_describe = request.user_describe;
                    userinfo.telphone = request.telphone;
                    userinfo.name = request.name;
                    userinfo.modifier_date = null;

                    var result = tran.CommandSet<SysUserInfoEntity>().Update(userinfo);
                    if (result > 0)
                    {
                        //先删除用户-角色关联信息
                        tran.CommandSet<SysUserRoleReEntity>().Where(p => p.user_id == userinfo.id).Delete();

                        //插入用户-角色关联信息
                        SysUserRoleReEntity sysUserRole = null;
                        if (request.roleList.Count > 0)
                        {
                            foreach (var item in request.roleList)
                            {
                                sysUserRole = new SysUserRoleReEntity();
                                sysUserRole.id = GuidHelper.GetGuid();
                                sysUserRole.user_id = userinfo.id;
                                sysUserRole.role_id = item;
                                tran.CommandSet<SysUserRoleReEntity>().Insert(sysUserRole);
                            }
                        }

                        //先删除用户-部门关联信息
                        tran.CommandSet<SysUserDepartmentReEntity>().Where(p => p.user_id == userinfo.id).Delete();

                        //插入用户-部门关联信息
                        SysUserDepartmentReEntity userDepartment = null;
                        if (request.departmentList.Count > 0)
                        {
                            foreach (var item in request.departmentList)
                            {
                                userDepartment = new SysUserDepartmentReEntity();
                                userDepartment.id = GuidHelper.GetGuid();
                                userDepartment.user_id = userinfo.id;
                                userDepartment.department_id = item;
                                tran.CommandSet<SysUserDepartmentReEntity>().Insert(userDepartment);
                            }
                        }

                        //先删除用户-岗位关联信息
                        tran.CommandSet<SysUserPostReEntity>().Where(p => p.user_id == userinfo.id).Delete();
                        //插入用户-岗位关联信息
                        SysUserPostReEntity userPost = null;
                        if (request.postList.Count > 0)
                        {
                            foreach (var item in request.postList)
                            {
                                userPost = new SysUserPostReEntity();
                                userPost.id = GuidHelper.GetGuid();
                                userPost.user_id = userinfo.id;
                                userPost.post_id = item;
                                tran.CommandSet<SysUserPostReEntity>().Insert(userPost);
                            }
                        }

                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "修改成功！";
                        AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"修改用户数据成功，修改用户数据：{JsonHelper.ToJson(userinfo)}");

                        #region 修改存在redis中的信息
                        var info = con.QuerySet<View_SysUserEntity>().Where(p => p.id == userinfo.id).Get();
                        if (info != null)
                        {
                            var ticket = info.MapTo<UserLoginInfo>();
                            SysUserServiceRedis.SaveUserInfo(ticket);
                        }
                        #endregion

                    }
                    else
                    {
                        resultInfo.Msg = "修改失败！";
                    }
                });
            }

            return resultInfo;
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(List<string> userIds)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var ids = userIds.ToArray();
                var userinfo = con.QuerySet<SysUserInfoEntity>().Where(a => a.id.In(ids)).ToList();
                if (userinfo.Count != userIds.Count)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
                userinfo.ForEach(w => w.is_deleted = true);

                //事务功能
                con.Transaction(tran =>
                {
                    var result = tran.CommandSet<SysUserInfoEntity>().Update(userinfo);

                    //删除原来配置的部门数据
                    var deletebu = tran.CommandSet<SysUserDepartmentReEntity>().Where(a => a.user_id.In(ids)).Delete();

                    //删除原来配置的岗位数据
                    var deletezhi = tran.CommandSet<SysUserPostReEntity>().Where(a => a.user_id.In(ids)).Delete();

                    //删除原来配置的角色数据
                    var deleterole = tran.CommandSet<SysUserRoleReEntity>().Where(a => a.user_id.In(ids)).Delete();

                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "删除成功！";
                        AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.UserManage, $"删除用户成功，删除用户数据：{JsonHelper.ToJson(userIds)}");
                    }
                    else
                    {
                        resultInfo.Msg = "删除失败！";
                    }
                }, ex =>
                {
                    throw ex;
                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 禁用/启用用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ForbidOrEnable(string userId)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {

                var userinfo = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == userId).Get();
                if (userinfo == null)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
                if (userinfo.is_valid)
                {
                    userinfo.is_valid = false;
                }
                else
                {
                    userinfo.is_valid = true;
                }
                var result = con.CommandSet<SysUserInfoEntity>().Update(userinfo);
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "禁用/启用用户！";
                    AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"禁用/启用用户成功，禁用/启用用户数据：{JsonHelper.ToJson(userinfo)}");
                }
                else
                {
                    resultInfo.Msg = "禁用/启用用户！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 密码重置
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> PswReset(string userId)
        {

            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {

                var userinfo = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == userId).Get();
                if (userinfo == null)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
                var newPasw = SecurityHelper.GetMD5Hash(userinfo.name + "123").ToLower();
                userinfo.password = SecurityHelper.GetMD5Hash(newPasw);

                var result = con.CommandSet<SysUserInfoEntity>().Update(userinfo);
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "密码重置成功！";
                    AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"密码重置成功，用户数据：{JsonHelper.ToJson(userinfo)}");
                }
                else
                {
                    resultInfo.Msg = "密码重置失败！";
                }
            }
            return resultInfo;
        }

        #endregion

        #endregion

        #region 信息发送相关功能

        /// <summary>
        /// 根据条件分页查询除自己以外的用户数据
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<UserInfoResponse>> LoadListExceptMyself(ParametersInfo<UserInfoQuery> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();

            var user = GetLoginUserInfo();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<View_SysUserEntity>();

                if (inputInfo.parameters.sKeyWords.IsNotNullOrEmpty())
                {
                    result.Where(a => a.name.Contains(inputInfo.parameters.sKeyWords)
                    || a.mobile_phone.Contains(inputInfo.parameters.sKeyWords)
                    || a.post_names.Contains(inputInfo.parameters.sKeyWords)
                    || a.department_names.Contains(inputInfo.parameters.sKeyWords)
                    );
                }

                #region 表达式拼装 或OrElse
                if (inputInfo.parameters.postIds.IsNotNullOrEmpty())
                {
                    var postIds = inputInfo.parameters.postIds.Split(',');

                    ParameterExpression pa = Expression.Parameter(typeof(View_SysUserEntity), "user");
                    MyExpressionVisitor visitor = new MyExpressionVisitor(pa);//统一参数类型

                    Expression<Func<View_SysUserEntity, bool>> lambdaOne = p => p.post_ids.Contains(postIds[0]);
                    Expression lambdaOneBody = visitor.Visit(lambdaOne.Body);

                    foreach (var item in postIds)
                    {
                        Expression<Func<View_SysUserEntity, bool>> lambdaItem = p => p.post_ids.Contains(item);
                        Expression lambdaBody = visitor.Visit(lambdaItem.Body);

                        lambdaOneBody = Expression.OrElse(lambdaOneBody, lambdaBody);
                    }
                    Expression<Func<View_SysUserEntity, bool>> newEx = Expression.Lambda<Func<View_SysUserEntity, bool>>(lambdaOneBody, pa);
                    result.Where(newEx);
                }
                #endregion

                if (inputInfo.parameters.departmentId.IsNotNullOrEmpty())
                {
                    result.Where(a => a.department_ids.Contains(inputInfo.parameters.departmentId));
                }

                //排除自己
                result.Where(p => p.id != user.id);

                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = " sort";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = " asc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);

                if (listInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<UserInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Data = new List<UserInfoResponse>();
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 根据条件查询除自己以外的用户ID
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<string>> LoadIdListExceptMyself(UserInfoQuery inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<string>>();

            var user = GetLoginUserInfo();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<View_SysUserEntity>();

                if (inputInfo.sKeyWords.IsNotNullOrEmpty())
                {
                    result.Where(a => a.name.Contains(inputInfo.sKeyWords)
                    || a.mobile_phone.Contains(inputInfo.sKeyWords)
                    || a.post_names.Contains(inputInfo.sKeyWords)
                    || a.department_names.Contains(inputInfo.sKeyWords)
                    );
                }

                #region 表达式拼装 或OrElse
                if (inputInfo.postIds.IsNotNullOrEmpty())
                {
                    var postIds = inputInfo.postIds.Split(',');

                    ParameterExpression pa = Expression.Parameter(typeof(View_SysUserEntity), "user");
                    MyExpressionVisitor visitor = new MyExpressionVisitor(pa);//统一参数类型

                    Expression<Func<View_SysUserEntity, bool>> lambdaOne = p => p.post_ids.Contains(postIds[0]);
                    Expression lambdaOneBody = visitor.Visit(lambdaOne.Body);

                    foreach (var item in postIds)
                    {
                        Expression<Func<View_SysUserEntity, bool>> lambdaItem = p => p.post_ids.Contains(item);
                        Expression lambdaBody = visitor.Visit(lambdaItem.Body);

                        lambdaOneBody = Expression.OrElse(lambdaOneBody, lambdaBody);
                    }
                    Expression<Func<View_SysUserEntity, bool>> newEx = Expression.Lambda<Func<View_SysUserEntity, bool>>(lambdaOneBody, pa);
                    result.Where(newEx);
                }
                #endregion

                if (inputInfo.departmentId.IsNotNullOrEmpty())
                {
                    result.Where(a => a.department_ids.Contains(inputInfo.departmentId));
                }
                //排除自己
                var listInfo = result.Where(p => p.id != user.id).ToList(p => p.id);

                if (listInfo.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo;
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }
        #endregion

        #region 批量导入导出
        /// <summary>
        /// 验证数据
        /// </summary>
        /// <param name="selects"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<UserInfoFileRequest>> VerifyThatTheFile(List<UserInfoFileRequest> selects)
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoFileRequest>>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                foreach (UserInfoFileRequest item in selects)
                {
                    if (item.role.IsNotNullOrEmpty())
                    {
                        var rl = item.role.Split(',').ToList();
                        var newrl = new List<string>();
                        for (int i = 0; i < rl.Count; i++)
                        {
                            var rlid = con.QuerySet<SysRoleInfoEntity>().Where(a => a.name.Equals(rl[i])).Get();
                            if (rlid != null)
                            {
                                newrl.Add(rlid.id);
                            }

                        }

                        item.roleid = string.Join(",", newrl.ToArray());
                    }
                    if (item.depart.IsNotNullOrEmpty())
                    {
                        var depar = item.depart.Split(',').ToList();
                        var newdepar = new List<string>();
                        for (int i = 0; i < depar.Count; i++)
                        {
                            var deparid = con.QuerySet<SysDepartmentInfoEntity>().Where(a => a.name.Equals(depar[i])).Get();
                            if (deparid != null)
                            {
                                newdepar.Add(deparid.id);
                            }

                        }

                        item.departid = string.Join(",", newdepar.ToArray());
                    }

                    if (item.post.IsNotNullOrEmpty())
                    {
                        var post = item.post.Split(',').ToList();
                        var newpost = new List<string>();
                        for (int i = 0; i < post.Count; i++)
                        {
                            var postid = con.QuerySet<SysPostInfoEntity>().Where(a => a.name.Equals(post[i])).Get();
                            if (postid != null)
                            {
                                newpost.Add(postid.id);
                            }

                        }

                        item.postid = string.Join(",", newpost.ToArray());
                    }
                }
            }
            resultInfo.Code = ActionCodes.Success;
            resultInfo.Data = selects;
            return resultInfo;
        }



        /// <summary>
        /// 事务批量导入用户信息
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<int> TranBulkImportRole(List<UserInfoFileRequest> selects)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                con.Transaction(tran =>
                {
                    var result = 0;

                    for (int i = 0; i < selects.Count; i++)
                    {
                        SysUserInfoEntity sysUser = new SysUserInfoEntity();
                        sysUser.name = selects[i].name;
                        sysUser.mobile_phone = selects[i].mobile_phone;
                        sysUser.gender = selects[i].gender;
                        sysUser.email = selects[i].email;
                        sysUser.is_valid = selects[i].ifis_valid == "已启用" ? true : false;

                        tran.CommandSet<SysUserInfoEntity>().Insert(sysUser);

                        if (selects[i].departid.IsNotNullOrEmpty())
                        {
                            var deper = selects[i].departid.Split(',').ToList();
                            for (int j = 0; j < deper.Count; j++)
                            {
                                SysUserDepartmentReEntity sysUserDepartment = new SysUserDepartmentReEntity();
                                sysUserDepartment.department_id = deper[j];
                                sysUserDepartment.user_id = sysUser.id;
                                result += tran.CommandSet<SysUserDepartmentReEntity>().Insert(sysUserDepartment);
                            }
                        }
                        if (selects[i].roleid.IsNotNullOrEmpty())
                        {
                            var roleid = selects[i].roleid.Split(',').ToList();
                            for (int j = 0; j < roleid.Count; j++)
                            {
                                SysUserRoleReEntity sysUserRoleRe = new SysUserRoleReEntity();
                                sysUserRoleRe.role_id = roleid[j];
                                sysUserRoleRe.user_id = sysUser.id;
                                result += tran.CommandSet<SysUserRoleReEntity>().Insert(sysUserRoleRe);
                            }
                        }
                        if (selects[i].postid.IsNotNullOrEmpty())
                        {
                            var postid = selects[i].postid.Split(',').ToList();
                            for (int j = 0; j < postid.Count; j++)
                            {
                                SysUserPostReEntity sysUserPostRe = new SysUserPostReEntity();
                                sysUserPostRe.post_id = postid[j];
                                sysUserPostRe.user_id = sysUser.id;
                                result += tran.CommandSet<SysUserPostReEntity>().Insert(sysUserPostRe);
                            }
                        }

                    }

                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "导入成功！";
                        AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.RoleManage, $"事务导入角色成功，导入角色信息：{JsonHelper.ToJson(selects)}");
                    }

                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 批量导出所有
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<UserInfoResponse>> ListAll()
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<View_SysUserEntity>().ToList();


                if (result.Count > 0)
                {
                    var outuser = result.MapToList<UserInfoResponse>();
                    outuser.ForEach(a => a.ifis_valid = a.is_valid ? "已启用" : "未启用");
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = outuser;
                }
                else
                {
                    resultInfo.Data = new List<UserInfoResponse>();
                    resultInfo.Msg = "无对应信息！";
                }
            }

            return resultInfo;
        }

        #endregion

        #region 其他用户操作功能

        #region 查询

        /// <summary>
        /// 根据角色获取用户列表信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<UserInfoResponse>> LoadListByRole(ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<View_SysUserEntity>();

                if (inputInfo.parameters.IsNotNullOrEmpty())
                {
                    result.Where(a => a.role_ids.Contains(inputInfo.parameters));
                }
                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = " sort";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = " asc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);

                if (listInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<UserInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Data = new List<UserInfoResponse>();
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }
        #endregion

        #endregion

    }
}