﻿using Common.Model;
using System.Collections.Generic;

namespace DevOps.Logic
{
    /// <summary>
    /// 管理员管理接口
    /// </summary>
    public interface ISysUserService
    {

        #region 用户登录操作和主界面用户操作

        /// <summary>
        /// 钉钉移动端自动登录
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<UserLoginInfo> GetDingDingUserinfo(string code);

        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        ResultFileInfo LoadValidateCode(string uniqueIdentifier);

        /// <summary>
        /// 用户登录
        /// </summary>
        ResultJsonInfo<UserLoginInfo> UserLogin(UserInfoLoginRequest inputInfo);

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<UserLoginInfo> LoadToken(string appSecret);

        /// <summary>
        /// 钉钉移动端登录
        /// </summary>
        /// <param name="mobile">手机号</param>
        /// <returns></returns>
        ResultJsonInfo<UserLoginInfo> LoadTokenMobile(string mobile);
        /// <summary>
        /// 客户端用户登录
        /// </summary>
        ResultJsonInfo<UserLoginInfo> UserLogin(ClientSideLoginRequest inputInfo);
        
        /// <summary>
        /// 通过RefreshToken获取用户信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<UserLoginInfo> LoadUserInfoByRefreshToken(UserInfoRefreshTokenRequest inputInfo);

        /// <summary>
        /// 查询单个用户的数据
        /// 【先查询Redis,如果Redis没有，再查询数据库，最后将信息放入到Redis】
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ResultJsonInfo<UserLoginInfo> LoadSingleById(string userId);
        /// <summary>
        /// 修改用户自身的主题
        /// </summary>
        /// <param name="themeCss"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyTheme(string themeCss);
        /// <summary>
        /// 获取用户自身的主题
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<string> GetMyTheme();
        ///// <summary>
        ///// 根据登录账号获取信息
        ///// </summary>
        ///// <param name="loginName"></param>
        ///// <returns></returns>
        //ResultJsonInfo<SysUserEntity> LoadUserByLoginName(string loginName);

        ///// <summary>
        ///// 上传图片（后期把图片上传统一处理）
        ///// </summary>
        ///// <param name="parametersInfo"></param>
        ///// <returns></returns>
        //ResultJsonInfo<string> UploadImage(string filePath, Dictionary<string, object> inputInfo);

        #endregion

        #region 用户基础信息管理操作
        #region 添加
        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="userAdd"></param>
        /// <returns></returns>
        ResultJsonInfo<int> AddUser(UserInfoAddRequest userAdd);
        /// <summary>
        /// 同步新增用户
        /// </summary>
        /// <param name="userAdd"></param>
        /// <returns></returns>
        ResultJsonInfo<int> SynModifyUser(UserInfoRequest userAdd);

        /// <summary>
        /// 同步编辑用户名称和手机号
        /// </summary>
        /// <param name="userAdd"></param>
        /// <returns></returns>
        ResultJsonInfo<int> SynUserNameAndMobile(UserInfoRequest userAdd);

        /// <summary>
        /// 批量同步上传用户
        /// </summary>
        /// <param name="userAdd"></param>
        /// <returns></returns>
        ResultJsonInfo<int> SynUploadUser(List<UserLoginInfo> userAdd);        
        /// <summary>
        /// 用户配置部门
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        ResultJsonInfo<int> UserDepartment(UserInfoDepartmentRequest department);

        /// <summary>
        /// 用户配置岗位
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        ResultJsonInfo<int> UserPost(UserInfoPositionRequset position);

        #endregion

        #region 查询
        /// <summary>
        /// 根据条件分页查询用户数据
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        ResultJsonInfo<List<UserInfoResponse>> LoadList(ParametersInfo<UserInfoQuery> parameters);

        /// <summary>
        /// 查询所有用户数据
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<UserInfoResponse>> LoadAllList();

        /// <summary>
        /// 查询单个用户的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ResultJsonInfo<UserInfoResponse> LoadSingle(string userId);

        /// <summary>
        /// 根据账号查询单个用户的数据
        /// </summary>
        /// <param name="loginName"></param>
        /// <returns></returns>
        ResultJsonInfo<UserInfoResponse> LoadSingleName(string loginName);

        /// <summary>
        /// 查询自己的数据
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<UserInfoResponse> LoadUserSingle();

        /// <summary>
        /// 查询自己的数据视图版
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<UserInfoResponse> LoadUserViewSingle();
        #endregion


        #region 修改

        /// <summary>
        /// 修改用户电话号码数据
        /// </summary>
        /// <param name="id"></param>
        /// <param name="mobilePhone"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyMobilePhone(string id, string mobilePhone);

        /// <summary>
        /// 修改用户数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Modify(UserInfoModifyRequest request);

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> userIds);

        /// <summary>
        /// 禁用/启用用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ForbidOrEnable(string userId);

        /// <summary>
        /// 密码重置
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<int> PswReset(string id);
        /// <summary>
        /// 修改用户自身的数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyOneself(UserInfoModifyOneselfRequest request);

        /// <summary>
        /// 修改自己的密码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ChangePassword(UserChangePasswordRequest request);

        #endregion


        #endregion

        #region 信息发送相关功能

        /// <summary>
        /// 根据条件分页查询除自己以外的用户数据
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<UserInfoResponse>> LoadListExceptMyself(ParametersInfo<UserInfoQuery> inputInfo);

        /// <summary>
        /// 根据条件查询除自己以外的用户ID
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<string>> LoadIdListExceptMyself(UserInfoQuery inputInfo);
        #endregion

        #region 批量导入导出
        /// <summary>
        /// 验证数据
        /// </summary>
        /// <param name="selects"></param>
        /// <returns></returns>
        ResultJsonInfo<List<UserInfoFileRequest>> VerifyThatTheFile(List<UserInfoFileRequest> selects);
        /// <summary>
        /// 事务批量导入用户信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> TranBulkImportRole(List<UserInfoFileRequest> selects);

        /// <summary>
        /// 批量导出所有
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<UserInfoResponse>> ListAll();

        #endregion

        #region 其他用户操作功能

        #region 查询

        /// <summary>
        /// 根据角色获取用户列表信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<UserInfoResponse>> LoadListByRole(ParametersInfo<string> inputInfo);

        #endregion

        #endregion
    }
}
