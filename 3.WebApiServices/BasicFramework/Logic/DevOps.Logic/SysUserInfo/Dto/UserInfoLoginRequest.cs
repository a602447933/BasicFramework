﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 用户登录信息传输实体类
    /// </summary>
    public class UserInfoLoginRequest
    {
        /// <summary>
        /// 账号
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 25, Description = "账号")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 客户端请求物理机唯一标识符
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "客户端请求物理机唯一标识符")]
        public string uniqueIdentifier
        {
            get; set;
        }

        /// <summary>
        /// 验证码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 4, Description = "验证码")]
        public string vialdcode
        {
            get; set;
        }

        /// <summary>
        /// 密码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "密码")]
        public string password
        {
            get; set;
        }

    }
}
