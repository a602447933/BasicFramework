﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 用户数据返回类
    /// </summary>
    public class UserInfoResponse
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户名【登录名】
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 密码
        /// </summary>
        public string password
        {
            get; set;
        }

        /// <summary>
        /// 用户描述
        /// </summary>
        public string user_describe
        {
            get; set;
        }

        /// <summary>
        /// 用户家庭电话
        /// </summary>
        public string telphone
        {
            get; set;
        }

        /// <summary>
        /// 用户手机号码
        /// </summary>
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 性别
        /// </summary>
        public string gender
        {
            get; set;
        }

        /// <summary>
        /// QQ号
        /// </summary>
        public string qq
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 用户真实名
        /// </summary>
        public string realname
        {
            get; set;
        }
      
        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 办公电话
        /// </summary>
        public string office_phone
        {
            get; set;
        }

        /// <summary>
        /// 用户身份号码
        /// </summary>
        public string idnumber
        {
            get; set;
        }

        /// <summary>
        /// 用户头像地址
        /// </summary>
        public string picture_url
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者
        /// </summary>
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 修改者日期
        /// </summary>
        public DateTime modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否登录
        /// </summary>
        public bool is_login
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        public bool is_deleted
        {
            get; set;
        }

        /// <summary>
        /// 是否是超级管理员
        /// </summary>
        public bool is_super_user
        {
            get; set;
        }

        /// <summary>
        /// 所属角色
        /// </summary>
        public string role_ids
        {
            get; set;
        }

        /// <summary>
        /// 所属角色名称
        /// </summary>
        public string role_names
        {
            get; set;
        }

        /// <summary>
        /// 所属岗位
        /// </summary>
        public string post_ids
        {
            get; set;
        }

        /// <summary>
        /// 所属岗位名称
        /// </summary>
        public string post_names
        {
            get; set;
        }

        /// <summary>
        /// 所属部门
        /// </summary>
        public string department_ids
        {
            get; set;
        }

        /// <summary>
        /// 所属部门名称
        /// </summary>
        public string department_names
        {
            get; set;
        }

        /// <summary>
        /// 是否启用
        /// </summary>
        public string ifis_valid
        {
            get; set;
        }

        /// <summary>
        /// 用户签章图片地址
        /// </summary>
        public string signature_url
        {
            get; set;
        }
    }
}
