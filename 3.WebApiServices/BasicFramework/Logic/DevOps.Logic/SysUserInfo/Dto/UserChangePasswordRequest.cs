﻿using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 用户自己修改密码传输实体
    /// </summary>
    public class UserChangePasswordRequest
    {

        /// <summary>
        /// 旧密码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "旧密码")]
        public string oldpassword { get; set; }

        /// <summary>
        /// 新密码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "新密码")]
        public string password { get; set; }
    }
}
