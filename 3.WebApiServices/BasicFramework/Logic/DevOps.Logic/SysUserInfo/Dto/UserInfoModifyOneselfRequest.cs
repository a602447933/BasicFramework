﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace DevOps.Logic
{

    /// <summary>
    /// 修改用户自身数据请求传输实体
    /// </summary>
    public class UserInfoModifyOneselfRequest
    {

        /// <summary>
        /// 用户家庭电话
        /// </summary>
        public string telphone
        {
            get; set;
        }

        /// <summary>
        /// 用户手机号码【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户手机号码")]
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 性别【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "性别")]
        public string gender
        {
            get; set;
        }

        /// <summary>
        /// QQ号
        /// </summary>
        public string qq
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 用户真实名【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户真实名")]
        public string realname
        {
            get; set;
        }

        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 办公电话
        /// </summary>
        public string office_phone
        {
            get; set;
        }

        /// <summary>
        /// 用户身份号码【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户身份号码")]
        public string idnumber
        {
            get; set;
        }

        /// <summary>
        /// 用户头像地址【必填】
        /// </summary>
        //[Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 255, Description = "用户头像地址")]
        public string picture_url
        {
            get; set;
        }

        /// <summary>
        /// 用户描述
        /// </summary>
        public string user_describe
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 用户签章图片地址
        /// </summary>
        public string signature_url
        {
            get; set;
        }
    }
}
