﻿using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 刷新token
    /// </summary>
    public class UserInfoRefreshTokenRequest
    {
        /// <summary>
        /// 账号
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 25, Description = "账号")]
        public string name { get; set; }

        /// <summary>
        /// 刷新Token的刷新码
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 36, Description = "刷新Token的刷新码")]
        public string refreshToken { get; set; }
    }
}
