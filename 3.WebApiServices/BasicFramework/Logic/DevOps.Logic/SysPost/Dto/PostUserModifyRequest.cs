﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 用户岗位修改请求实体
    /// </summary>
    public class PostUserModifyRequest
    {  /// <summary>
       ///岗位唯一标识
       /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色唯一标识符")]
        public string post_id
        {
            get; set;
        }

        /// <summary>
        ///用户唯一标识符列表
        /// </summary>
        public List<string> idList
        {
            get; set;
        }
    }
}
