﻿using System;

namespace DevOps.Logic
{
    /// <summary>
    /// 返回的岗位数据
    /// </summary>
    public class PostResponse
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 简码，CEO,CTO
        /// </summary>
        public string brevity_code
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime create_date
        {
            get; set;
        }
        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        public bool is_deleted
        {
            get; set;
        }
    }
}
