﻿using System.Collections.Generic;
using DevOps.Logic;
using Common.Library;
using Common.Model;

/*
* 命名空间: DevOps.Logic
*
* 功 能： 岗位功能管理逻辑接口
*
* 类 名： ISysPostService
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 岗位功能管理逻辑接口
    /// </summary>
    public interface ISysPostService
    {
        #region 岗位基础信息管理模块

        #region 查询
        /// <summary>
        /// 根据岗位关键字 分页获取岗位列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<PostResponse>> LoadSysPostList(ParametersInfo<string> inputInfo);
        /// <summary>
        /// 获取所有岗位
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<PostResponse>> LoadSysPostAllList();

        /// <summary>
        /// 获取所有岗位树状数据
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<SelectListInfo>> LoadSysPostTreeList();

        /// <summary>
        /// 获取单个岗位的信息
        /// </summary>
        /// <param name="postId">岗位ID</param>
        /// <returns></returns>
        ResultJsonInfo<PostResponse> LoadSysPostListOne(string postId);
        /// <summary>
        /// 根据岗位id获取用户情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<PostUserResponse>> LoadListInfoByPostId(PostUserQuery queryInfo);
        #endregion

        #region 新增
        /// <summary>
        /// 新增一个岗位
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> AddSysPost(PostAddRequest postAdd);
        #endregion

        #region 修改
        /// <summary>
        /// 修改一个岗位
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> ModifySysPost(PostModifyRequest inputInfo);

        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> DeleteSysPost(List<string> postIds);

        /// <summary>
        /// 禁用/启用岗位
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> ForbiddenSysPost(string postId);
        /// <summary>
        /// 修改对应岗位下对应的用户
        /// </summary>
        /// <param name="post"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyPostUserInfo(PostUserModifyRequest post);
        #endregion

        #endregion
    }
}
