﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 部门树状菜单返回信息
    /// </summary>
    public class DepartmentTreeInfo
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 父节点唯一标识符
        /// </summary>
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 领导人名称
        /// </summary>
        public string leader
        {
            get; set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string phone
        {
            get; set;
        }

        /// <summary>
        /// 邮件地址
        /// </summary>
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否有兄长【同级别，排前面的】
        /// </summary>
        public bool have_elder
        {
            get; set;
        }

        /// <summary>
        /// 是否有兄弟【同级别，排后面的】
        /// </summary>
        public bool have_younger
        {
            get; set;
        }


        /// <summary>
        /// 子节点
        /// </summary>
        public List<DepartmentTreeInfo> children { get; set; }
    }
}
