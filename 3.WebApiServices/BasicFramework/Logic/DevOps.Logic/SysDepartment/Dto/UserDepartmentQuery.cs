﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 给与用户-部门请求信息
    /// </summary>
    public class UserDepartmentQuery
    {

        /// <summary>
        /// 获取类型 100所有 110给与的
        /// </summary>
        public int type { get; set; }

        /// <summary>
        /// 员工Ids
        /// </summary>
        public List<string> employeeIds { get; set; }
         
    }
}
