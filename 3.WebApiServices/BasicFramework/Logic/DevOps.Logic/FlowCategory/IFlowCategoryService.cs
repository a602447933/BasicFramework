﻿using Common.Model;
using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;
/*
* 命名空间:  DevOps.Logic
*
* 功 能：流程类型信息管理模块
*
* 类 名： FlowCategoryServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/14 9:34:35               Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程类型信息管理模块
    /// </summary>
    public interface IFlowCategoryService
    {
        #region 类型信息管理模块

        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FlowCategoryInfoEntity>> LoadPageList(ParametersInfo<string> inputInfo);

        /// <summary>
        ///获取所有列表
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<FlowCategoryInfoEntity>> LoadList();
        #endregion

        #region 更新

        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Save(FlowCategoryInfoEntity inputInfo);


        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);
        #endregion

        #endregion
    }
}
