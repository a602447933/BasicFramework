﻿using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using DevOps.Model;
using Serialize.Library;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：流程按钮信息管理模块
*
* 类 名： FlowButtonInfoServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/14 10:10:11  Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程按钮信息管理模块
    /// </summary>
    public class FlowButtonInfoServiceImpl : OperationLogicImpl, IFlowButtonInfoService
    {
        #region 按钮信息管理模块

        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<FlowButtonInfoEntity>> LoadPageList(ParametersInfo<string> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<FlowButtonInfoEntity>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = con.QuerySet<FlowButtonInfoEntity>();

                if (inputInfo.parameters.IsNotNullOrEmpty())
                {
                    result.Where(p => p.title.Contains(inputInfo.parameters) || p.note.Contains(inputInfo.parameters));
                }
                var queryInfo = result.OrderBy(p => p.sort).PageList(inputInfo.page, inputInfo.limit);

                if (queryInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = queryInfo.Items;
                    resultInfo.Count = queryInfo.Total;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "无对应值存在！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 流程步骤Id获取拥有的按钮
        /// </summary>
        /// <param name="stepId">流程步骤Id</param>
        /// <returns></returns>
        public ResultJsonInfo<List<FlowOwnButtonResponse>> LoadFlowOwnButtonList(string  stepId)
        {
            var resultInfo = new ResultJsonInfo<List<FlowOwnButtonResponse>>();

            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                con.Transaction(tran =>
                {
                    var buttonList = con.QuerySet<FlowButtonInfoEntity>().OrderBy(p => p.sort).ToList();
                    if (buttonList.Count>0)
                    {
                        var buttons = buttonList.MapToList<FlowOwnButtonResponse>();

                        var stepInfo = con.QuerySet<FlowStepInfoEntity>().Where(p => p.id.Equals(stepId)).Get();
                        if (stepInfo != null)
                        {
                            foreach (var item in buttons)
                            {
                                if (stepInfo.button_ids.Contains(item.id))
                                {
                                    item.is_checked = true;
                                }
                            }
                        }
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Data = buttons;
                        resultInfo.Msg = "获取成功！";
                    }
                    else
                    {
                        resultInfo.Msg = "无对应值存在！";
                    }
                  

                    
                });
            }
            return resultInfo;
        }
        #endregion

        #region 更新

        /// <summary>
        /// 新增
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<int> Save(FlowButtonInfoEntity inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            var user = GetLoginUserInfo();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                con.Transaction(tran =>
                {
                    //修改
                    if (inputInfo.id.IsNotNullOrEmpty())
                    {
                        #region 修改

                        var info = tran.QuerySet<FlowButtonInfoEntity>().Where(p => p.id.Equals(inputInfo.id)).Get();
                        if (info != null)
                        {
                            info.script = inputInfo.script;
                            info.note = inputInfo.note;
                            info.sort = inputInfo.sort;
                            info.sort = inputInfo.sort;
                            info.title = inputInfo.title;
                            info.icon = inputInfo.icon;
                            var result = tran.CommandSet<FlowButtonInfoEntity>().Update(info);
                            if (result > 0)
                            {

                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Msg = "修改成功！";
                                AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.ButtonManag, $"修改按钮信息，修改信息：{JsonHelper.ToJson(inputInfo)}");
                            }
                            else
                            {
                                resultInfo.Code = ActionCodes.InvalidOperation;
                                resultInfo.Msg = "修改失败！";
                            }
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "Id无对应信息！";
                        }
                        #endregion
                    }
                    else
                    {
                        #region 新增
                        inputInfo.id = GuidHelper.GetGuid();
                        var result = tran.CommandSet<FlowButtonInfoEntity>().Insert(inputInfo);
                        if (result > 0)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "添加成功！";
                            AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.ButtonManag, $"新增按钮信息，新增信息：{JsonHelper.ToJson(inputInfo)}");
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "修改失败！";
                        }
                        #endregion
                    }
                });
            }
            return resultInfo;

        }



        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //删除
                var result = con.CommandSet<FlowButtonInfoEntity>().Where(p => p.id.In(ids.ToArray())).Delete();
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "删除成功！";
                    AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.ButtonManag, $"删除按钮信息!{JsonHelper.ToJson(ids)}");
                }
                else
                {
                    resultInfo.Msg = "删除失败！";
                }
            }
            return resultInfo;
        }
        #endregion

        #endregion
    }
}
