﻿using AutoMapper;
using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  DevOps.Logic
*
* 功 能：XXX
*
* 类 名： FlowButtonInfoProfile
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/1/14 10:09:28               Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 流程按钮映射累
    /// </summary>
    public class FlowButtonInfoProfile : Profile
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public FlowButtonInfoProfile()
        {
            CreateMap<FlowButtonInfoEntity, FlowOwnButtonResponse>();
        }
    }
}
