﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{

    /// <summary>
    /// 流程步骤-同意操作请求信息
    /// </summary>
    public class FlowAgreeRequest
    {
        /// <summary>
        /// 流程任务ID
        /// </summary>
        public string taskId { set; get; }

        /// <summary>
        /// 处理意见
        /// </summary>
        public string comments { set; get; }

        /// <summary>
        /// 获取当前节点和后续节点信息
        /// </summary>
        public FlowStepCurrentInfo currentFlowInput { set; get; }
    }
}
