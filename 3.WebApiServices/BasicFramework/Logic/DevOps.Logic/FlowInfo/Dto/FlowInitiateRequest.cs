﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 流程发起
    /// </summary>
    public class FlowInitiateRequest
    {
        /// <summary>
        /// 对应业务表主键值
        /// </summary>
        public string instance_id { set; get; }

        /// <summary>
        /// 获取当前节点和后续节点信息
        /// </summary>
        public FlowStepCurrentInfo currentFlowInput { set; get; }

    }
}
