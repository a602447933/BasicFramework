﻿using Common.Library;
using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 流程信息管理接口
    /// </summary>
    public interface IFlowInfoService
    {
        #region 流程信息管理模块

        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FlowInfoResponse>> LoadPageList(ParametersInfo<string> inputInfo);

        /// <summary>
        /// 根据Id值获取单条信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<FlowDetailInfoResponse> LoadSingleById(string id);

        /// <summary>
        /// 获取所有用户机构树状信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<DepartmentUserTreeInfo>> LoadDepartmentUserTreeList();

        /// <summary>
        /// 获取流程处理者类型
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadFlowHandlerTypeList();

        /// <summary>
        /// 获取会签策略
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadFlowSignStrategyList();

        /// <summary>
        /// 获取流程退回类型
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadFlowReturnTypeList();

        /// <summary>
        /// 获取流程处理策略
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadFlowHandleStrategyList();

        #endregion

        #region 更新
        /// <summary>
        /// 编辑流程
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Save(FlowInfoRequest inputInfo);

        /// <summary>
        /// 编辑流程详情信息【报错流程图】
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> SaveFlowMapl(FlowDetailInfoRequest inputInfo);

        /// <summary>
        /// 安装流程
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> InstallFlowMapl(FlowDetailInfoRequest inputInfo);

        /// <summary>
        /// 安装/卸载
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<int> InstallUninstall(string id);

        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);

        #endregion

        #endregion

        #region 流程过程操作逻辑

        #region 查询

        /// <summary>
        /// 获取起始步骤的后一步的步骤【这一步主要找出步骤中，可以参与审批的人员】
        /// </summary>
        /// <param name="flowKey">流程ID在参数表中的Key</param>
        /// <returns></returns>
        ResultJsonInfo<FlowStepCurrentInfo> LoadBeginNextStepInfo(string flowKey);

        /// <summary>
        /// 根据步骤Id获取后一步的步骤
        /// </summary>
        /// <param name="stepId"></param>
        /// <returns></returns>
        ResultJsonInfo<FlowStepCurrentInfo> LoadNextStepInfo(string stepId);

        /// <summary>
        /// 发起流程
        /// </summary>
        /// <param name="inputInfo ">instance_id业务主键 currentFlowInput当前步骤和下个步骤的信息</param>
        /// <returns></returns>
        ResultJsonInfo<bool> InitiateFlow(FlowInitiateRequest inputInfo);

        /// <summary>
        /// 流程步骤-同意操作
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<bool> AgreeFlow(FlowAgreeRequest inputInfo);

        /// <summary>
        /// 流程步骤-退回操作
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        ResultJsonInfo<bool> ReturnFlow(string taskId, string comments);

        /// <summary>
        /// 流程步骤-完成操作
        /// </summary>
        /// <param name="taskId"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        ResultJsonInfo<bool> FinishFlow(string taskId, string comments);
        #endregion

        #endregion
    }
}
