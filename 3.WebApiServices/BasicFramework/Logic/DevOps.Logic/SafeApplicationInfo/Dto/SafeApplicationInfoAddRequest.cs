﻿using Validate.Library;

namespace DevOps.Logic
{
    /// <summary>
    /// 应用增加传输实体 
    /// </summary>
    public class SafeApplicationInfoAddRequest
    {
        /// <summary>
        /// 应用名称
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 32, Description = "应用名称")]
        public string applicatian_name
        {
            get; set;
        }

        /// <summary>
        /// 应用类型（100：App，200：微信，300：web，400：桌面）
        /// </summary>
        [Validate(ValidateType.NotEmpty, Regex = ValidateRegex.Number, Description = "应用类型")]
        public int applicatian_type
        {
            get; set;
        }

        /// <summary>
        /// 应用秘钥
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 500, Description = "应用秘钥")]
        public string appsecret
        {
            get; set;
        }

        /// <summary>
        /// 应用ID
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 32, Description = "应用ID")]
        public string unionid
        {
            get; set;
        }

        /// <summary>
        /// 应用登录名
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 32, Description = "应用登录名")]
        public string accountid
        {
            get; set;
        }
    }
}
