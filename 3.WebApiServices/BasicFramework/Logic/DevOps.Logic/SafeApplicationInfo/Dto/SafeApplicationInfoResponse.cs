﻿using DevOps.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 应用返回信息
    /// </summary>
    public class SafeApplicationInfoResponse: SafeApplicationInfoEntity
    {
       
        /// <summary>
        /// 应用类型（100：App，200：微信，300：web，400：桌面）
        /// </summary>
        public string applicatian_typename
        {
            get; set;
        }
      
    }
}
