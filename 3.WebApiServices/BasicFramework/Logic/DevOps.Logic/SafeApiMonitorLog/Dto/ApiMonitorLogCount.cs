﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 警告用户和黑名单用户数量
    /// </summary>
    public class ApiMonitorLogCount
    {
        /// <summary>
        /// 预警数
        /// </summary>
        public int warningCount
        {
            get; set;
        }

        /// <summary>
        /// 黑名单个数
        /// </summary>
        public int blacklistCount
        {
            get; set;
        }
    }
}
