﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DevOps.Logic
{
    /// <summary>
    /// 监控日志查询请求实体
    /// </summary>
    public class ApiMonitorLogRequest
    {
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime startTime { get; set; }
        /// <summary>
        /// 截止时间
        /// </summary>
        public DateTime endTime { get; set; }
    }
}
