﻿using Common.Library;
using Common.Model;
using System;
using System.Collections.Generic;
/*
* 命名空间: DevOps.Logic
*
* 功 能： 操作记录日志逻辑接口
*
* 类 名： IOperationLogService
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 Harvey     创建
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 异常日志逻辑接口
    /// </summary>
    public interface IExceptionLogService
    {

        /// <summary>
        /// 获取异常日志类型
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadLogTypeList();


        /// <summary>
        /// 获取服务名称Select列表信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadServiceNameList();


        #region 日志基础模块管理

        #region 查询
        /// <summary>
        /// 根据异常日志条件分页获取列表 关键字【业务标题】【操作用户名称】【操作明细】
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<ExceptionLogInfoResponse>> LoadExceptionList(ParametersInfo<ExceptionLogQuery> inputInfo);
        #endregion


        /// <summary>
        /// 批量删除异常日志
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(ExceptionLogRequest request);

        /// <summary>
        /// 解决完成异常
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<int> SolveException(string id);
        #endregion


        #region 异常日志统计相关逻辑

        /// <summary>
        /// 获取对应时间段内，异常信息数量
        /// </summary>
        /// <param name="beginTime">起始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        ResultJsonInfo<ExceptionCountResponse> LoadExceptionCount(DateTime beginTime, DateTime endTime);

        /// <summary>
        /// 获取最新错误信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<ExceptionLogInfoResponse>> LoadLatestList();

        #endregion

    }
}
