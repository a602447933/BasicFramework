﻿namespace DevOps.Logic
{
    /// <summary>
    /// 日志查询条件类
    /// </summary>
    public class ExceptionLogQuery
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string sKeyWords
        {
            get; set;
        }
        /// <summary>
        ///异常类型
        /// </summary>
        public int exception_type
        {
            get; set;
        }
        /// <summary>
        /// 服务名称
        /// </summary>
        public string service_name
        {
            get; set;
        }
    }

}
