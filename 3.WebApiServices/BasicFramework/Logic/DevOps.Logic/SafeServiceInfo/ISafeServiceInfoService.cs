﻿using Common.Library;
using Common.Model;
using System;
using System.Collections.Generic;
/*
* 命名空间: DevOps.Logic
*
* 功 能： 服务管理逻辑类接口
*
* 类 名： ISafeServiceInfoService
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/05/09 14:34:43 Harvey     创建
*
* Copyright (c) Harvey Harvey Corporation. All rights reserved.
*/
namespace DevOps.Logic
{
    /// <summary>
    /// 服务管理逻辑类接口
    /// </summary>
    public interface ISafeServiceInfoService
    {

        #region 服务基础信息管理

        #region 查询

        /// <summary>
        /// 根据IP地址，服务名称，端口号信息获取服务分页列表信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<ServiceInfoResponse>> LoadPageList(ParametersInfo<ServiceInfoQueryRequest> inputInfo);

        #endregion

        #region 更新功能

        /// <summary>
        /// 更新服务信息
        /// </summary>
        /// <param name="serviceInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> OperateService(ServiceInfoModifyRequest serviceInfo);

        /// <summary>
        /// 物理删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);

        /// <summary>
        /// 远程操作服务信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ControlService(string id, int type);

        /// <summary>
        /// 全面核查
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Check();

        #endregion

        #endregion

    }
}
