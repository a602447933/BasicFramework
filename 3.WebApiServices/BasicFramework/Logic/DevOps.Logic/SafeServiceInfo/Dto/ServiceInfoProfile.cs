﻿/*
* 命名空间: DevOps.Logic
*
* 功 能： 服务管理实体映射管理类
*
* 类 名： ServiceInfoProfile
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/9 14:22:47 	Harvey    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 Harvey Corporation. All rights reserved.
*/
using AutoMapper;
using Common.Library;
using Common.Model;
using DevOps.Model;

namespace DevOps.Logic
{
    /// <summary>
    /// 服务管理实体映射管理类
    /// </summary>
    public class ServiceInfoProfile : Profile
    {
        /// <summary>
        /// 服务管理实体映射管理类 构造函数
        /// </summary>
        public ServiceInfoProfile()
        {

            CreateMap<SafeServiceInfoEntity, ServiceInfoResponse>()
                .ForMember(s => s.framework_version_name, opt => opt.MapFrom(x => x.framework_version.GetEnumDescriptionByValue(typeof(FrameworkVersion))))
                .ForMember(p => p.status_name, opt => opt.MapFrom(x => x.status.GetEnumDescriptionByValue(typeof(ServiceStatus))))

                //.ForMember(p => p.login_account, opt => opt.MapFrom(x => AESEncryptHelper.Decrypt(x.login_account)))
                //.ForMember(p => p.login_password, opt => opt.MapFrom(x => AESEncryptHelper.Decrypt(x.login_password)))
                ;

            CreateMap<ServiceInfoModifyRequest, SafeServiceInfoEntity>()
                //.ForMember(p => p.login_account, opt => opt.MapFrom(x => AESEncryptHelper.Encrypt(x.login_account)))
                //.ForMember(p => p.login_password, opt => opt.MapFrom(x => AESEncryptHelper.Encrypt(x.login_password)))
                ;
        }

    }
}
