﻿using AutoMapper;
using Common.Library;
using Common.Model;
using Mall.Model;
using System;

namespace Mall.Logic
{
    /// <summary>
    /// 移动端用户实体映射
    /// </summary>
    public class ClientInfoProfile: Profile
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public ClientInfoProfile() {


            //移动端
            CreateMap<ClientInfoEntity, WechatUserLoginInfo>();

            CreateMap<ClientLoginRequest, ClientInfoEntity>()
                .ForMember(p => p.is_valid, opt => opt.MapFrom(x =>(int)IsValid.Valid))
                .ForMember(p => p.is_deleted, opt => opt.MapFrom(x => (int)IsDeleted.NotDeleted))
                .ForMember(p => p.create_time, opt => opt.MapFrom(x => DateTime.Now))
                ;

            //PC端
            CreateMap<ClientInfoEntity, ClientInfoResponse>()
                 .ForMember(p => p.sex_name, opt => opt.MapFrom(x => (Convert.ToInt32(x.sex)).GetEnumDescriptionByValue(typeof(GenderType))))
                  .ForMember(p => p.is_valid_name, opt => opt.MapFrom(x => (Convert.ToInt32(x.is_valid)).GetEnumDescriptionByValue(typeof(IsValid))))
                ;
            CreateMap<ClientInfoModifyRequest, ClientInfoEntity>();


        }
    }
}
