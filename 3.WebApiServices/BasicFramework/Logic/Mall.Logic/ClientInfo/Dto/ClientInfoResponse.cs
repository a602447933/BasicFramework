﻿using Mall.Model;

namespace Mall.Logic
{
    /// <summary>
    /// 用户信息返回实体
    /// </summary>
    public class ClientInfoResponse: ClientInfoEntity
    {

        /// <summary>
        /// 性别 1男 2女0未知
        /// </summary>
        public string sex_name
        {
            get; set;
        }


        /// <summary>
        /// 是否有效  100有效 110无效
        /// </summary>
        public string is_valid_name
        {
            get; set;
        }
        
    }
}