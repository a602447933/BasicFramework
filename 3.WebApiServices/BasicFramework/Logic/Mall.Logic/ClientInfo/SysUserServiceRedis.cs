﻿using Common.Library;
using Common.Model;
using Newtonsoft.Json;
using Redis.Library;
using Serialize.Library;
using System;
using System.Collections.Generic;

namespace Mall.Logic
{
    /// <summary>
    /// 管理员Redis操作逻辑类
    /// </summary>
    internal static class SysUserServiceRedis<T>
    {

        #region 用户登录信息

        /// <summary>
        /// 保存用户登录信息在Redis中
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void SaveUserInfo(string key, T value, Action successAction = null, Action failAction = null)
        {
            string userInfoStr = value.ToJson();
            var  userInfoDic = JsonConvert.DeserializeObject<Dictionary<string, object>>(userInfoStr);

            bool resultInfo = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWeb)
                     .StringSet(RedisAuthorityInfo.RedisSessionCode + key, userInfoStr,RedisAuthorityInfo.SessionTimeOut);


            bool userResultInfo = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWeb)
                     .StringSet(RedisAuthorityInfo.RedisSessionCode + userInfoDic["id"].ToString(), userInfoStr);

            if (resultInfo && userResultInfo)
            {
                successAction?.Invoke();
            }
            else
            {
                failAction?.Invoke();
            }
        }


        /// <summary>
        /// 保存用户登录信息在Redis中
        /// </summary>
        /// <param name="value"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void SaveUserInfo(T value, Action successAction = null, Action failAction = null)
        {
            string userInfoStr = value.ToJson();
            var userInfoDic = JsonConvert.DeserializeObject<Dictionary<string, object>>(userInfoStr);

            bool userResultInfo = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWeb)
                     .StringSet(RedisAuthorityInfo.RedisSessionCode + userInfoDic["id"].ToString(), userInfoStr);

            if (userResultInfo)
            {
                successAction?.Invoke();
            }
            else
            {
                failAction?.Invoke();
            }
        }


        /// <summary>
        /// 获取用户登录信息在Redis中
        /// </summary>
        /// <param name="key"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void GetUserInfo(string key, Action<string> successAction, Action failAction = null)
        {
            string resultInfo = RedisClient.GetRedisDb(RedisAuthorityInfo.RedisDbWeb).StringGet(RedisAuthorityInfo.RedisSessionCode + key);

            if (resultInfo.IsNotNullOrEmpty())
            {
                successAction?.Invoke(resultInfo);
            }
            else
            {
                failAction?.Invoke();
            }
        }

        #endregion
    }
}
