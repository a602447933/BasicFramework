﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mall.Logic
{
    /// <summary>
    /// 移动端用户操作逻辑类接口
    /// </summary>
    public interface IClientInfoService
    {

        #region PC管理端逻辑

        #region 查询

        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<ClientInfoResponse>> LoadPageList(ParametersInfo<string> inputInfo);

        #endregion

        #region 更新

        /// <summary>
        /// 更新
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> SaveInfo(ClientInfoModifyRequest inputInfo);

        #endregion

        #region 删除

        /// <summary>
        /// 删除XXX信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);

        #endregion

        #endregion

        #region 移动端逻辑

        /// <summary>
        /// 微信自动授权登录
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<WechatUserLoginInfo> LoginCheck(ClientLoginRequest inputInfo);

        /// <summary>
        /// 通过微信openId获取用户信息
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        ResultJsonInfo<WechatUserLoginInfo> LoadByOpenId(string openId);

        /// <summary>
        /// 微信通过Id获取用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<WechatUserLoginInfo> LoadSingleById(string id);

        #endregion

    }
}
