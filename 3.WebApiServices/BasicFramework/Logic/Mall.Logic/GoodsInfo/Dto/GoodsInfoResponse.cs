﻿using Mall.Model;
using System;

namespace Mall.Logic
{
    /// <summary>
    /// 商品信息返回类
    /// </summary>
    public class GoodsInfoResponse: View_GoodsInfoEntity
    {

        /// <summary>
        /// 是否热卖(100-是 110-不是)
        /// </summary>
        public string is_hot_style_name
        {
            get; set;
        }

        /// <summary>
        /// 是否新款(100-是 110-不是)
        /// </summary>
        public string is_new_style_name
        {
            get; set;
        }

        /// <summary>
        /// 是否推荐(100-是 110-不是)
        /// </summary>
        public string is_recommend_name
        {
            get; set;
        }

        /// <summary>
        /// 是否为特价商品(100-是 110-不是)
        /// </summary>
        public string is_bargain_name
        {
            get; set;
        }

        /// <summary>
        /// 是否上架(100-是 110-不是)
        /// </summary>
        public string is_shelves_name
        {
            get; set;
        }
    }
}
