﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mall.Logic
{
    /// <summary>
    /// 商品保存信息
    /// </summary>
    public class GoodsInfoModifyRequest
    {

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 分类唯一标识符
        /// </summary>
        public string category_id
        {
            get; set;
        }

        /// <summary>
        /// 商品名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 商品副标题
        /// </summary>
        public string title
        {
            get; set;
        }

        /// <summary>
        /// 商品货号
        /// </summary>
        public string goods_no
        {
            get; set;
        }

        /// <summary>
        /// 售卖单价
        /// </summary>
        public decimal selling_price
        {
            get; set;
        }

        /// <summary>
        /// 商品原价
        /// </summary>
        public decimal original_price
        {
            get; set;
        }

        /// <summary>
        /// 是否热卖(100-是 110-不是)
        /// </summary>
        public int is_hot_style
        {
            get; set;
        }

        /// <summary>
        /// 是否新款(100-是 110-不是)
        /// </summary>
        public int is_new_style
        {
            get; set;
        }

        /// <summary>
        /// 是否推荐(100-是 110-不是)
        /// </summary>
        public int is_recommend
        {
            get; set;
        }

        /// <summary>
        /// 是否为特价商品(100-是 110-不是)
        /// </summary>
        public int is_bargain
        {
            get; set;
        }

        /// <summary>
        /// 是否上架(100-是 110-不是)
        /// </summary>
        public int is_shelves
        {
            get; set;
        }

        /// <summary>
        /// 总销量
        /// </summary>
        public int sales_volume
        {
            get; set;
        }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime createt_time
        {
            get; set;
        }

        /// <summary>
        /// 商品详情描述,富文本框
        /// </summary>
        public string introduction
        {
            get; set;
        }

        /// <summary>
        /// 商品排序
        /// </summary>
        public double sort
        {
            get; set;
        }
    }
}
