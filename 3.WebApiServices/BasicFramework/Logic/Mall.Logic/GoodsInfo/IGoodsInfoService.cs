﻿using Common.Model;
using Mall.Model;
using System.Collections.Generic;

namespace Mall.Logic
{
    /// <summary>
    /// 商品信息操作类接口
    /// </summary>
    public interface IGoodsInfoService
    {
        #region 商品信息基础管理模块


        #region 查询

        /// <summary>
        /// 根据关键字 分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<GoodsInfoResponse>> LoadPageList(ParametersInfo<GoodsInfoQuery> inputInfo);

        /// <summary>
        /// 根据id获取对应的信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<GoodsInfoResponse> LoadSingleById(string id);

        /// <summary>
        /// 根据商品id获取附件文件
        /// </summary>
        /// <param name="goodsId"></param>
        /// <returns></returns>
        ResultJsonInfo<List<GoodsPictureEntity>> LoadAttachList(string goodsId);

        /// <summary>
        /// 根据id获取对应的详情信息【基本信息+附件】
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<GoodsDetailResponse> LoadDetailById(string id);

        #endregion

        #region 更新

        /// <summary>
        /// 更新
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> SaveInfo(GoodsInfoModifyRequest inputInfo);

        /// <summary>
        /// 是否开启 热卖  推荐  特价  上架 出现的
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        ResultJsonInfo<int> EnableInfo(string id, int type); 

        /// <summary>
        /// 删除商品信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);

        #endregion

        #endregion
    }
}
