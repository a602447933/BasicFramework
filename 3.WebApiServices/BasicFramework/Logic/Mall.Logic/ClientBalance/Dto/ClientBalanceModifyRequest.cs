﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  Mall.Logic.ClientBalance.Dto
*
* 功 能：
*
* 类 名：ClientBalanceModifyRequest  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/6 10:50:56  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Mall.Logic
{
    /// <summary>
    /// 客户金额变动明细表
    /// </summary>
	public class ClientBalanceModifyRequest
    {

        /// <summary>
        /// 用户唯一标识符
        /// </summary>
        public string client_id
        {
            get; set;
        }

        /// <summary>
        /// 订单唯一标识符
        /// </summary>
        public string order_id
        {
            get; set;
        }

        /// <summary>
        /// 订单编号
        /// </summary>
        public string order_no
        {
            get; set;
        }

        /// <summary>
        /// 变动时间
        /// </summary>
        public DateTime create_time
        {
            get; set;
        }

        /// <summary>
        /// 变动金额
        /// </summary>
        public decimal price
        {
            get; set;
        }

        /// <summary>
        /// 100购买，110退款
        /// </summary>
        public int operation_type
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }
    }
}
