using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using Mall.Model;
using Serialize.Library;
using System;
using System.Collections.Generic;

namespace Mall.Logic
{

    /// <summary>
    /// OrderInfo操作逻辑类
    /// </summary>
    public class OrderInfoServiceImpl : OperationLogicImpl, IOrderInfoService
    {

        #region 查询
        /// <summary>
        ///根据关键字分页获取列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<OrderInfoResponse>> LoadPageList(ParametersInfo<OrderInfoQuery> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<OrderInfoResponse>>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                int isDeleted = (int)IsDeleted.Deleted;
                var result = con.QuerySet<OrderInfoEntity>().Where(a => a.is_deleted != isDeleted);
                //if (inputInfo.parameters.xxx.IsNotNullOrEmpty())
                //{
                //    result.Where(p => p.xxx.Equals(inputInfo.parameters.xxxx));
                //}
                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = " sort";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = " asc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion
                //eg: var queryInfo = result.PageList(inputInfo.page, inputInfo.limit, p => new GoodsInfoEntity { id = p.id }); 

                var queryInfo = result.PageList(inputInfo.page, inputInfo.limit);
                if (queryInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = queryInfo.Items.MapToList<OrderInfoResponse>();
                    resultInfo.Count = queryInfo.Total;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "无对应值存在！";
                }
            }
            return resultInfo;
        }
        #endregion

        #region 更新
        /// <summary>
        /// 更新订单信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> SaveInfo(OrderInfoModifyRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                var result = 0;
                if (inputInfo.id.IsNotNullOrEmpty())
                {
                    #region Modify
                    var modifyInfo = con.QuerySet<OrderInfoEntity>().Where(p => p.id == inputInfo.id).Get();
                    if (modifyInfo != null)
                    {
                        //modifyInfo.xxx = inputInfo.xxx;
                        result = con.CommandSet<OrderInfoEntity>().Update(modifyInfo);
                        if (result > 0)
                        {
                            resultInfo.Code = ActionCodes.Success;
                            resultInfo.Msg = "修改成功！";
                            AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.OrderInfoManage, $"修改订单信息，修改信息：{ JsonHelper.ToJson(inputInfo)}");
                        }
                        else
                        {
                            resultInfo.Code = ActionCodes.InvalidOperation;
                            resultInfo.Msg = "修改失败！";
                        }
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.InvalidOperation;
                        resultInfo.Msg = "对应Id的信息不存在！";
                    }
                    #endregion
                }
                else
                {
                    #region Add
                    var addInfo = inputInfo.MapTo<OrderInfoEntity>();
                    addInfo.id = GuidHelper.GetGuid();
                    result = con.CommandSet<OrderInfoEntity>().Insert(addInfo);
                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "添加成功！";
                        AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.OrderInfoManage, $"添加了一个订单信息，新增信息：{ JsonHelper.ToJson(inputInfo)}");
                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.InvalidOperation;
                        resultInfo.Msg = "添加失败！";
                    }
                    #endregion
                }
            }
            return resultInfo;
        }
        #endregion

        #region 删除
        /// <summary>
        /// 删除订单信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = DataBase.GetConnection(DatabaseName.Sql_BUS_DB.ToString()))
            {
                //删除con.Transaction(tran =>{ });
                //var info = tran.QuerySet<GoodsInfoEntity>().Where(p => p.id.In(ids.ToArray())).ToList();
                //info.ForEach(a => a.is_deleted = (int)IsDeleted.Deleted);
                //var result = tran.CommandSet<GoodsInfoEntity>().Update(info);
                var result = con.CommandSet<OrderInfoEntity>().Where(p => p.id.In(ids.ToArray())).Delete();
                if (result > 0)
                {
                    //con.CommandSet<XXXX>().Where(p => p.xxid.In(ids.ToArray())).Delete();
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "删除成功！";
                    AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.OrderInfoManage, $"删除订单信息成功，物理删除信息：{ JsonHelper.ToJson(ids)}");
                }
                else
                {
                    resultInfo.Msg = "删除失败！";
                }
            }
            return resultInfo;
        }
        #endregion
    }
}
