﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  Mall.Logic
*
* 功 能：订单返回信息传输实体
*
* 类 名：OrderInfoResponse  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/6 11:13:53  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Mall.Logic
{
    /// <summary>
    /// 订单返回信息传输实体
    /// </summary>
	public class OrderInfoResponse
    {

    }
}
