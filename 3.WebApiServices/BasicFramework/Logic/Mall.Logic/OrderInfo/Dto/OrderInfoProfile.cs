using System;
using AutoMapper;
using Common.Library;
using Common.Model;
using Mall.Model;
/*
* 命名空间: Mall.Logic
*
* 功 能： OrderInfoProfile实体类映射帮助类
*
* 类 名： OrderInfoProfile
*
* Version   变更日期           负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2021/08/06 11:11:19     Harvey    创建
*
* Copyright (c) 2021 Harvey Corporation. All rights reserved.
*/

namespace Mall.Logic
{

    /// <summary>
    /// OrderInfo实体类映射帮助类
    /// </summary>
    public class OrderInfoProfile : Profile
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public OrderInfoProfile()
        {
            //eg:CreateMap<from, to>().ForMember(p => p.type_name, opt => opt.MapFrom(x => (Convert.ToInt32(x.type)).GetEnumDescriptionByValue(typeof(DrainCompanyType))));
            CreateMap<OrderInfoEntity, OrderInfoResponse>();
            CreateMap<OrderInfoModifyRequest, OrderInfoEntity>();
        }
    }
}
