﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间:  Mall.Logic
*
* 功 能：订单查询条件传输实体
*
* 类 名：OrderInfoQuery  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/6 11:14:21  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace Mall.Logic
{
    /// <summary>
    /// 订单查询条件传输实体
    /// </summary>
	public class OrderInfoQuery
    {

    }
}
