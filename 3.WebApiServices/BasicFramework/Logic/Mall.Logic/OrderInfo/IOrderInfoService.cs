using Common.Model;
using System.Collections.Generic;

namespace Mall.Logic
{

    /// <summary>
    /// OrderInfo操作逻辑类接口
    /// </summary>
    public interface IOrderInfoService
    {

       #region 查询
       /// <summary>
       ///根据关键字分页获取列表
       /// </summary>
       /// <param name="inputInfo"></param>
       /// <returns></returns>
        ResultJsonInfo<List<OrderInfoResponse>> LoadPageList(ParametersInfo<OrderInfoQuery> inputInfo);
        #endregion

        #region 更新
        /// <summary>
        /// 更新订单信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> SaveInfo(OrderInfoModifyRequest inputInfo);
        #endregion

        #region 删除
        /// <summary>
        /// 删除订单信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);
       #endregion
    }
}
