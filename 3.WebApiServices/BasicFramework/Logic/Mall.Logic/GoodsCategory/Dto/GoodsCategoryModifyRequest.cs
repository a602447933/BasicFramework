﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace Mall.Logic
{
    /// <summary>
    /// 商品分类编辑请求实体
    /// </summary>
    public class GoodsCategoryModifyRequest
    {
        /// <summary>
        /// 唯一编码【要求非空，且最大长度32】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 32, Description = "唯一编码")]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 类名
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 分类图片
        /// </summary>
        public string img_url
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

    }
}
