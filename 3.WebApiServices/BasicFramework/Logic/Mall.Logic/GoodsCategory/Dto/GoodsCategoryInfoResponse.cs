﻿using Mall.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mall.Logic
{
    /// <summary>
    /// 商品分类返回信息
    /// </summary>
    public class GoodsCategoryInfoResponse : GoodsCategoriesEntity
    {
        /// <summary>
        /// 是否有兄长【同级别，排前面的】
        /// </summary>
        public bool have_elder
        {
            get; set;
        }

        /// <summary>
        /// 是否有兄弟【同级别，排后面的】
        /// </summary>
        public bool have_younger
        {
            get; set;
        }
    }
}
