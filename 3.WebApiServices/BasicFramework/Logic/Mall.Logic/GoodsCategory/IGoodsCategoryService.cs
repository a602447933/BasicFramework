﻿using Common.Library;
using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mall.Logic
{
    /// <summary>
    /// 商品分类逻辑接口
    /// </summary>
    public interface IGoodsCategoryService
    {
        #region 基础信息管理模块

        #region 信息查询

        /// <summary>
        /// 获取所有商品分类树状信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<TreeInfo>> LoadAllTreeList();

        /// <summary>
        /// 根据关键字获取所有的商品分类树状信息
        /// </summary>
        /// <param name="keyWords"></param>
        /// <returns></returns>
        ResultJsonInfo<List<GoodsCategoryTreeInfo>> LoadTreeList(string keyWords);

        #endregion

        #region 更新操作

        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <param name="addInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Addnode(GoodsCategoryAddRequest addInfo);

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Modify(GoodsCategoryModifyRequest modifyInfo);

        #endregion

        #region 移动顺序操作
        /// <summary>
        /// 移动顺序操作
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Move(GoodsCategoryMoveRequest inputInfo);

        #endregion

        #region 删除信息

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="removeInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> ids);

        #endregion

        #endregion
    }
}
