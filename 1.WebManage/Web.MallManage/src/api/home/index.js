import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {

  //获取所有可操作菜单
  loadMenuList(par) {
    return httpRequest({
      url: "/api/Home/LoadMenuList/{appSecret}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //获取顶端菜单【在components-》commom->Header.vue中使用】
  loadTopMenu(par) {
    return httpRequest({
      url: "/api/Home/LoadTopMenuList/{appSecret}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //获取子级菜单
  loadSubMenu(par) {
    return httpRequest({
      url: "/api/Home/LoadSubMenus/{appSecret}/{parentId}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
//修改密码
changePassword(par) {
  return httpRequest({
    url: "/api/Authority/UserInfo/ChangePassword",
    method: 'POST',
    modal: false,
    data: par
  });
},
 //获取部门列表信息
 loadDepartmentTreeList(par) {
  return httpRequest({
    url: "/api/Authority/Department/LoadAllTreeList",
    method: 'POST',
    modal: false,
    data: par
  });
},
  //获取岗位树状列表
  loadPostTreeListInfo(par) {
    return httpRequest({
      url: "/api/Authority/Department/LoadAllSelectList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //获取角色树状列表
  loadRoleTreeListInfo(par) {
    return httpRequest({
      url: "/api/Authority/RoleInfo/LoadAllSelectList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
    //获取自己的信息
    loadUserSingle() {
      return httpRequest({
        url: "/api/Authority/UserInfo/LoadUserSingle",
        method: 'GET',
        modal: false,
      });
    },


}
