import http from '@/utils/http';
const {
    httpRequest
} = http;

export default {
    //获取分页列表信息
    loadPageList(par) {
        return httpRequest({
            url: "/api/business/ParameterSetting/LoadPageList",
            method: 'POST',
            modal: false,
            data: par
        });
    },

    //删除信息
    removeInfo(par) {
        return httpRequest({
            url: "/api/business/ParameterSetting/Remove",
            method: 'POST',
            modal: false,
            data: par
        });
    },
    //增加信息
    addInfo(par) {
        return httpRequest({
            url: "/api/business/ParameterSetting/Add",
            method: 'POST',
            modal: false,
            data: par
        });
    },
    //修改信息
    modifyInfo(par) {
        return httpRequest({
            url: "/api/business/ParameterSetting/Modify",
            method: 'POST',
            modal: false,
            data: par
        });
    },

}
