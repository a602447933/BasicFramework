import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {

  //获取所有的消息
  loadPageList(par) {
    return httpRequest({
      url: "/api/Basic/ServerApiInfo/LoadPageList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
   //删除
   removeinfo(par) {
    return httpRequest({
      url: "/api/Basic/ServerApiInfo/Remove",
      method: 'POST',
      modal: false,
      data: par
    });
  },
    //保存或修改
    save(par) {
      return httpRequest({
        url: "/api/Basic/ServerApiInfo/Save",
        method: 'POST',
        modal: false,
        data: par
      });
    },
  
}
