import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {

  //获取按钮列表
  loadPageList(par) {
    return httpRequest({
      url: "/api/FlowButtonInfo/LoadPageList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //编辑按钮
  save(par) {
    return httpRequest({
      url: "/api/FlowButtonInfo/Save",
      method: 'POST',
      modal: false,
      data: par
    });
  },
   //删除按钮
   remove(par) {
    return httpRequest({
      url: "/api/FlowButtonInfo/Remove",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
