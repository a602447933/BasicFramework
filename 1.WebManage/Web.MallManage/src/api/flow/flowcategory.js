import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {

  //获取类型列表
  loadPageList(par) {
    return httpRequest({
      url: "/api/FlowCategory/LoadPageList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //编辑类型
  save(par) {
    return httpRequest({
      url: "/api/FlowCategory/Save",
      method: 'POST',
      modal: false,
      data: par
    });
  },
   //删除类型
   remove(par) {
    return httpRequest({
      url: "/api/FlowCategory/Remove",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
