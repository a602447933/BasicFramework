import http from '@/utils/http';
import store from '@/store'
const {
  httpRequest
} = http;

export default {
  ////////文件操作
  //合并
  mergeFiles(par) {
    return httpRequest({
      baseURL: store.getters.fileServerURI,
      url: "/api/Files/MergeFiles",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //删除指定附件
  attchRemove(par) {
    return httpRequest({
      baseURL: store.getters.fileServerURI,
      url: "/api/Files/RemoveFile",
      method: 'GET',
      modal: false,
      data: par
    });
  },
}