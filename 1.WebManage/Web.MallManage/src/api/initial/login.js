import http from '@/utils/http';
const {httpRequest}=http;
export default {
  //登录检查
  login(par){
    return httpRequest({
      url: "/api/Login/LoginCheck",
      method: 'POST',
      data:par
  });
  },
  //获取验证码
  loadValidateCode(par){
    return httpRequest({
       url: "/api/Login/LoadValidateCode/{uniqueIdentifier}",
       method: 'GET',
       data:par
    });
  }, 
  //刷新Token
  refreshToken(par){
    return httpRequest({
       url: "/api/Login/RefreshToken",
       method: 'POST',
       data:par
    });
  }, 
}