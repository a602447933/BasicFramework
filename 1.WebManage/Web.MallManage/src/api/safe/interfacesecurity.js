import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {
    
  //获取左下的列表信息
  loadInterfaceWarning(par) {
    return httpRequest({
      url: "/api/Safe/ApiMonitorLog/LoadInterfaceWarning",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //获取右下的列表信息
  loadBlacklist(par) {
    return httpRequest({
      url: "/api/Safe/ApiMonitorLog/LoadBlacklist",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //删除接口黑名单
  removeBalckList(par) {
    return httpRequest({
      url: "/api/Safe/ApiMonitorLog/RemoveBalckList/{ip}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
   //加入黑名单
   addBalckList(par) {
    return httpRequest({
      url: "/api/Safe/ApiMonitorLog/AddBalckList/{ip}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
}
