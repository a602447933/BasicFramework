import http from '@/utils/http';
const {
  httpRequest
} = http;

export default {

  //获取所有的应用
  loadSysApplicationList(par) {
    return httpRequest({
      url: "/api/Safe/ApplicationInfo/LoadSysApplicationList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //禁用启用
  forbiddenSysApplication(par) {
    return httpRequest({
      url: "/api/Safe/ApplicationInfo/ForbiddenSysApplication/{applicationId}",
      method: 'GET',
      modal: false,
      data: par
    });
  },
  //获取所有的应用类型
  loadApplicationTypeList() {
    return httpRequest({
      url: "/api/Safe/ApplicationInfo/LoadApplicationTypeList",
      method: 'GET',
      modal: false,
    });
  },
  //添加应用类型
  addSysApplication(par) {
    return httpRequest({
      url: "/api/Safe/ApplicationInfo/AddSysApplication",
      method: 'POST',
      modal: false,
      data: par
    });
  },//删除
  deleteSysApplication(par) {
    return httpRequest({
      url: "/api/Safe/ApplicationInfo/DeleteSysApplication",
      method: 'POST',
      modal: false,
      data: par
    });
  },
 //修改应用类型
 modifySysApplication(par) {
  return httpRequest({
    url: "/api/Safe/ApplicationInfo/ModifySysApplication",
    method: 'POST',
    modal: false,
    data: par
  });
},
}
