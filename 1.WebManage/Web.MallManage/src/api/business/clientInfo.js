/* 
 * @Author: LW  
 * @Date: 2021-01-11 14:22:37  
 * @function:客户管理相关接口
 * ---------------------------------------------------------- 
 */
import http from '@/utils/http';
import store from '@/store';

const {
  httpRequest
} = http;

export default {
    
  //获取客户分页列表
  loadPageList(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Client/ClientInfo/LoadPageList",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //编辑数据
  saveInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Client/ClientInfo/SaveInfo",
      method: 'POST',
      modal: false,
      data: par
    });
  },
  //删除客户信息
  removeInfo(par) {
    return httpRequest({
      baseURL: store.getters.mallURI,
      url: "/api/Client/ClientInfo/Remove",
      method: 'POST',
      modal: false,
      data: par
    });
  },
}
