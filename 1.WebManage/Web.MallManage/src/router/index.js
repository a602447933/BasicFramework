import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
  // mode:'history',
  routes: [{
      path: '/',
      name: 'login',
      component: () => import('@/views/initial/login.vue'),
      meta: {
        requireLogin: false,
      },
    },
    {
      path: '/entrance',
      name: 'entrance',
      component: () => import('@/views/initial/entrance.vue'),
      meta: {
        requireLogin: false,
      },
    },
    {
      path: '/transition/:encrypted',
      name: 'transition',
      component: () => import('@/views/initial/transition.vue'),
      meta: {
        requireLogin: false,
      },
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('@/views/home/index.vue'),
      meta: {
        requireLogin: false
      },
      children: [
        
      ]
    },
  ],
});

//防止重复点击路由报错
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
export default router
