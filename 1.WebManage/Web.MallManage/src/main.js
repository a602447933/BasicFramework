import Vue from 'vue';
import './plugins/vuetify';
import App from './App.vue';
import router from './router';
import store from './store';
//公用样式
import '@/assets/css/common/global.scss';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
//引入字体图标
import './assets/fonts/iconfont.css'
//pc自适应
import './plugins/lib-flexible/flexible'
//ie不能运行问题
import '@babel/polyfill'

//引入echarts
import echarts from 'echarts'
Vue.prototype.$echarts = echarts

//引入视频
// import Video from 'video.js'
// import 'video.js/dist/video-js.css'
// Vue.prototype.$video = Video

//引入signalR
import signalr from '@/utils/signalr'
Vue.prototype.$signalR = signalr

//引入moment
import moment from 'moment'
Vue.prototype.$moment = moment

// 引入vue-UUID组件
import UUID from 'vue-uuid'
Vue.use(UUID)

//引入弹窗拖拽【流程】
import '@/utils/dialogDrag'
import '@/assets/css/common/flow.css'

import dynamicRoutes from "@/utils/dynamicRoutes";
const {mergeRoutes} = dynamicRoutes;

//弹窗提示
import messageTip from '@/utils/message'
Vue.prototype.$messageTip = messageTip;

Vue.use(ElementUI);
router.beforeEach((to, from, next) => {
  if (sessionStorage.getItem('userMenuInfo')) {
    let userMenuInfo = JSON.parse(decodeURIComponent(sessionStorage.getItem('userMenuInfo')));
    if (to.path != "/") {
      let routesInfo = mergeRoutes(userMenuInfo);//这一步必须有，暂时还不能把routesInfo，直接存到sessionStorage中
      if (routesInfo != undefined && router.options.routes.length <= 4) {
        router.addRoutes(routesInfo);
        router.options.routes.push(routesInfo);
        router.replace(to.path);//replace,保证浏览器回退的时候能直接返回到上个页面，不会叠加
      }
    }
  }
  //如果要求登录，就验证登录
  if (to.meta.requireLogin) {
    if (store.getters.isLogin) {
      next();
    } else {
      next('/login');
    }
    //如果不求登录，就直接跳转界面
  } else {
    next();
  }
});

//它会显示你生产模式的消息
Vue.config.productionTip = false;
var vue = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
export default vue
