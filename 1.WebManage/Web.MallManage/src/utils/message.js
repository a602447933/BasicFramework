import { Message, MessageBox } from 'element-ui'

const messageTip  = {
    error(text = '错误',) {
      Message({
        message: text,
        type: 'error',
        offset: 400,
        showClose:true,
        duration: 2 * 1000
      })
    },
    warning(text = '警告') {
      Message({
        message: text,
        type: 'warning',
        offset: 400,
        showClose:true,
        duration: 2 * 1000
      })
   },
   success(text = '成功') {
    Message({
      message: text,
      type: 'success',
      offset: 400,
      showClose:true,
      duration: 1 * 1000
   })
  },
  info(text = '取消') {
    Message({
      message: text,
      type: 'info',
      offset: 400,
      showClose:true,
      duration: 1 * 1000
    })
  },
  boxError(text = 'Box错误') {
    return MessageBox.confirm(text, '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'error'
    })
  },
  boxWarning(text = 'Box询问') {
    return MessageBox.confirm(text, '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'warning'
    })
  },
  boxSuccess(text = 'Box成功') {
    return MessageBox.confirm(text, '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'success'
    })
  },
  boxInfo(text = 'Box取消') {
    return MessageBox.confirm(text, '提示', {
      confirmButtonText: '确定',
      cancelButtonText: '取消',
      type: 'info'
    })
  }
};
export default messageTip;


