/* 
 * @Author: Havery 
 * @Date: 2020-11-12 14:00:31  
 * @function: 长连接
 * ---------------------------------------------------------- 
 */
import * as signalR from "@aspnet/signalr";
// 建立连接
 const signalr={
  connection:null,
  startCon(url, func) {
    var connection = new signalR.HubConnectionBuilder().withUrl(url).build();
    connection.start().then(function () {
      console.log("连接成功");
      if (func) {
        func(connection);
      }
    }).catch(function (ex) {
      console.log("连接失败" + ex);
      setTimeout(() => start(), 5000);
    });
    async function start() {
      try {
        await connection.start();
        console.log("开始连接");
      } catch (err) {
        console.log("连接错误：" + err);
        setTimeout(() => start(), 5000);
      }
    };
    //如果连接关闭以后，自动开启连接
    connection.onclose(async () => {
      start();
    });
  }
} 
export default signalr