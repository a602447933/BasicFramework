/* 
 * @Author: LW  
 * @Date: 2020-07-31 11:29:35  
 * @function:配置信息状态模块
 */

/**
 * state为单一状态树，在state中需要定义我们所需要管理的数组、对象、字符串等等，只有在这里定义了，
 * 在vue.js的组件中才能获取你定义的这个对象的状态。
 */
 const state = {
    //访问接口服务的密钥
    appSecret: "78478E41B903CAA6FF7E08D7F64997D1",
    devopsURI: "8081",//8081 804 后端运维Api服务地址 
    mallURI: "8082",//8082 805 后端商城Api服务地址
    fileServerURI: "8099",//8099 803 //文件服务地址   
};

/**
 * 更改store中state状态的唯一方法就是提交mutation，就很类似事件。
 * 每个mutation都有一个字符串类型的事件类型和一个回调函数， 我们需要改变state的值就要在回调函数中改变。
 * 我们要执行这个回调函数，那么我们需要执行一个相应的调用方法：store.commit
 */
const mutations = {
  /**
   * 初始化
   * @param {*} state 
   */
  initUrl(state) {
    if (state.devopsURI.indexOf("http") == -1) {
      let hrefUrl = window.location.href.substring(0, window.location.href.lastIndexOf(":") + 1);
      state.devopsURI = hrefUrl + state.devopsURI;//后端Api服务地址
      state.mallURI = hrefUrl + state.mallURI;//后端商城Api服务地址
      state.fileServerURI =hrefUrl+ state.fileServerURI;//文件服务地址
    }
  }
};

/**
 * action可以提交mutation，在action中可以执行store.commit，而且action中可以有任何的异步操作。
 * 在页面中如果我们要嗲用这个action，则需要执行store.dispatch
 */
const actions = {
  //执行配置初始化
  initUrl(context) {
    context.commit('initUrl');
  },
}

export default {
  state,
  mutations,
  actions
}


