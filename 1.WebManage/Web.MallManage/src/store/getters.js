/**
 * getter有点类似vue.js的计算属性，当我们需要从store的state中派生出一些状态，
 * 那么我们就需要使用getter，getter会接收state作为第一个参数，而且getter的返回值会根据它的依赖被缓存起来，
 * 只有getter中的依赖值（state中的某个需要派生状态的值）发生改变的时候才会被重新计算。
 */

 //【外部调用口子统一，不然乱糟糟的，东西多了，具体在那个分子都不知道了】
const getters = {
    uniqueIdentifier: state => state.user.uniqueIdentifier,
    userInfo: state => state.user.userInfo,
    isLogin: state => state.user.isLogin,

    appSecret:state => state.config.appSecret,
    devopsURI:state => state.config.devopsURI,
    mallURI:state => state.config.mallURI,
    fileServerURI:state => state.config.fileServerURI,
    filesPath:state=>state.files,
  };

  export default getters;
  