const colors = require('./src/utils/colors.js');
const path = require('path');
var webpack = require('webpack')

let str = '';
for (const [key, value] of Object.entries(colors)) {
  str += `$${key}: ${value};`;
}
const CompressionPlugin = require("compression-webpack-plugin");
const productionGzipExtensions = ['js', 'css'];

console.log(colors);
module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
  productionSourceMap: false,//Vue 配置打包不打包.map文件
  configureWebpack: {
    devtool: 'source-map',
    externals: {
      './cptable': 'var cptable',
      '../xlsx.js': 'var _XLSX'
    },
    resolve:{
      alias:{
        '@':path.resolve(__dirname, './src'),
        '@i':path.resolve(__dirname, './src/assets'),
      } 
    },
    plugins: [
      //npm install compression-webpack-plugin@6.1.1 --save-dev
      //Ignore all locale files of moment.js
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new CompressionPlugin({
          algorithm: 'gzip', // 使用gzip压缩
          test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
          //filename: '[path].gz[query]', // 压缩后的文件名(保持原文件名，后缀加.gz)
          minRatio: 1, // 压缩率小于1才会压缩
          threshold: 10240, // 对超过10k的数据压缩
          deleteOriginalAssets: false, // 是否删除未压缩的源文件，谨慎设置，如果希望提供非gzip的资源，可不设置或者设置为false（比如删除打包后的gz后还可以加载到原始资源文件）
      }),
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 5, 
        minChunkSize: 100
      })
   ],
  },
  devServer: {
    open: true,
    overlay: {
      warnings: false,
      errors: false
    }
  },
  lintOnSave: false,
  css: {
    loaderOptions: {
      postcss: {
        plugins: [
          require('postcss-px2rem')({
            remUnit: 192
          })
        ]
      }
    },
  },
  //修改项目图标
  pwa: {
    iconPaths: {
      favicon32: 'favicon.ico',
      favicon16: 'favicon.ico',
      appleTouchIcon: 'favicon.ico',
      maskIcon: 'favicon.ico',
      msTileImage: 'favicon.ico'
    }
  }
};
