# ERP管理系统-BasicFramework

#### 介绍
前端Layui+asp.net core 2.2+仿路德流程，后端Swagger+asp.net core api+Dapper NoSQL数据持久化+PostgreSql数据库的前后端分离的 通用运维系统。
1.WebManage 文件夹放前端代码
2.DesktopApplication 文件夹桌面应用
3.WebApiServices 文件夹放服务端代码
4.HelpTool 放工具类代码

#### 软件架构
注意事项：
 **_

### 后端服务使用的工具类库，不是直接引用的，是通过自建私有nuget库来引用的。如果要在自己的机子上跑起来，需要增加nuget连接【私有小服务器，请不要恶意破坏】
自己也可以建立Nuget服务，讲Lib中的工具类库生成nuget包，自己管理。
_** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0826/173445_d209c3b1_1337151.png "屏幕截图.png")

软件架构说明
结构图如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0826/172953_bf4aade2_1337151.png "屏幕截图.png")
前端结构：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0826/173303_8ef31cae_1337151.png "屏幕截图.png")

在线体验
演示地址：http://119.3.125.240:806/
账号密码：admin admin123

####项目演示

![输入图片说明](https://images.gitee.com/uploads/images/2020/0826/174055_979a5ac8_1337151.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0826/174131_660a4c84_1337151.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
