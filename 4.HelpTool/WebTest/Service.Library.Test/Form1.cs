﻿using System;
using System.Windows.Forms;

namespace Service.Library.Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 异步委托显示
        /// </summary>
        /// <param name="value"></param>
        private delegate void SetTextBoxValue(string value);
        internal void SetMyTextBoxValue(string value)
        {
            value = "------------------------------\r\n" + value;
            if (txt_output_info.InvokeRequired)
            {
                SetTextBoxValue objSetTextBoxValue = new SetTextBoxValue(SetMyTextBoxValue);
                IAsyncResult result = this.txt_output_info.BeginInvoke(objSetTextBoxValue, new object[] { value });
                try
                {
                    objSetTextBoxValue.EndInvoke(result);
                }
                catch
                {

                }
            }
            else
            {
                this.txt_output_info.Text += value + Environment.NewLine;
                this.txt_output_info.SelectionStart = this.txt_output_info.TextLength;
                this.txt_output_info.ScrollToCaret();
            }
        }

        /// <summary>
        /// 获取IIS信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_iis_info_Click(object sender, EventArgs e)
        {
            try
            {
                string result = string.Empty;

                //IISServiceInfo serviceInfo = new IISServiceInfo() {
                //    //ServiceIp="192.168.1.101",
                //    //LoginAccount= "administrator",
                //    //LoginPassword= "zhdl86158818@"
                //    ServiceIp = "localhost",
                //    LoginAccount = "ASUS",
                //    LoginPassword = "luowei123"
                //};

                //if (IISWorkerClient.ExistIISService("W3SVC"))
                //{
                //    //IIS版本号
                //    string iisVersion = IISWorkerClient.GetIISMajorVersion();
                //    result += "IIS版本号：" + iisVersion + "\r\n";

                //    string riisVersion = IISWorkerClient.GetIIsVersion(serviceInfo);
                //    result += "IIS版本号：" + riisVersion + "\r\n";
                    
                //    //获取最小SiteId
                //    int siteId = IISWorkerClient.SiteId(serviceInfo);
                //    result += "SiteId：" + siteId + "\r\n";

                //    // var createSiteResult = IISWorkerClient.CreateSite(serviceInfo, "ApiOfficialWebsite1", siteId,"8086","192.168.1.101",@"E:\Publish\WebApi\OfficialWebsiteApi");
                //    //IISWorkerClient1.CreateWebSite(serviceInfo, "ApiOfficialWebsite1","192.168.1.101:8086",@"E:\Publish\WebApi\OfficialWebsiteApi");

                //    //IISWorkerClient.DelSite(serviceInfo, "ApiBasicFramework");

                //    //IISWorkerClient.StopAppPool(serviceInfo, "ApiBasicFramework");
                //    IISWorkerClient.StopAppPool(serviceInfo, "ZHDL.WebApi");
                //    //IISWorkerClient.StartWebSite(serviceInfo, "ApiBasicFramework");//可以实现

                //    //IISWorkerClient.StopWebSite(serviceInfo, "ZHDL.WebApi");

                //    //var stopWebSite= IISWorkerClient.StopWebSite(serviceInfo, "ApiBasicFramework");

                //    result += "创建网站结果：" + 1 + "\r\n";

                //    SetMyTextBoxValue(result);
                //}
                //else
                //{
                //    MessageBox.Show("IIS没有安装");
                //}
            }
            catch (Exception ex)
            {
                SetMyTextBoxValue(ex.Message);
            }
        }


    }
}
