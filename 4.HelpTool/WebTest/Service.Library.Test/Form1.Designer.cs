﻿namespace Service.Library.Test
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_output_info = new System.Windows.Forms.TextBox();
            this.btn_iis_info = new System.Windows.Forms.Button();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_output_info);
            this.groupBox2.Location = new System.Drawing.Point(8, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(775, 216);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "输出信息";
            // 
            // txt_output_info
            // 
            this.txt_output_info.Location = new System.Drawing.Point(6, 20);
            this.txt_output_info.Multiline = true;
            this.txt_output_info.Name = "txt_output_info";
            this.txt_output_info.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_output_info.Size = new System.Drawing.Size(760, 183);
            this.txt_output_info.TabIndex = 2;
            // 
            // btn_iis_info
            // 
            this.btn_iis_info.Location = new System.Drawing.Point(699, 235);
            this.btn_iis_info.Name = "btn_iis_info";
            this.btn_iis_info.Size = new System.Drawing.Size(75, 23);
            this.btn_iis_info.TabIndex = 27;
            this.btn_iis_info.Text = "IIS测试";
            this.btn_iis_info.UseVisualStyleBackColor = true;
            this.btn_iis_info.Click += new System.EventHandler(this.btn_iis_info_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 433);
            this.Controls.Add(this.btn_iis_info);
            this.Controls.Add(this.groupBox2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txt_output_info;
        private System.Windows.Forms.Button btn_iis_info;
    }
}

