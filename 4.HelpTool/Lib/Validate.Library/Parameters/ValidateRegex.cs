﻿namespace Validate.Library
{
    /// <summary>
    /// 验证正则表达式
    /// </summary>
    public class ValidateRegex
    {

        /// <summary>
        ///限制为数字 
        /// </summary>
        public const string Number = @"^\d+$";

        /// <summary>
        /// 限制为时间类型
        /// </summary>
        public const string DateTime = @"(\d{2,4})[-/]?([0]?[1-9]|[1][12])[-/]?([0][1-9]|[12]\d|[3][01])\s*([01]\d|[2][0-4])?[:]?([012345]?\d)?[:]?([012345]?\d)?";

        /// <summary>
        /// 限制为浮点类型
        /// </summary>
        public const string Decimal = @"^\d+[.]?\d+$";

        /// <summary>
        /// 限制为固定电话号码格式
        /// </summary>
        public const string Telphone = @"^(\d{3,4}-)?\d{6,8}$";

        /// <summary>
        /// 限制为手机号码格式
        /// </summary>
        public const string Mobile = @"/^1\d{10}$";

        /// <summary>
        /// 限制为手机号码格式
        /// </summary>
        public const string TelphoneOrMobile = @"^(0\d{2,3}-?\d{7,8})|(1\d{10})$";


        /// <summary>
        /// 限制为Email
        /// </summary>
        public const string Email = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

        /// <summary>
        /// 限制为数字和英文字母
        /// </summary>
        public const string NumAndEn = @"^[A-Za-z0-9]+$";


        /// <summary>
        /// 限制为字符串只包含汉字
        /// </summary>
        public const string ChineseCh = @"^[\u4e00-\u9fa5]+$";

        /// <summary>
        /// 限制为Url
        /// </summary>
        public const string Url = @"/^((ht|f)tps?):\/\/([\w\-]+(\.[\w\-]+)*\/)*[\w\-]+(\.[\w\-]+)*\/?(\?([\w\-\.,@?^=%&:\/~\+#]*)+)?/";
    }
}
