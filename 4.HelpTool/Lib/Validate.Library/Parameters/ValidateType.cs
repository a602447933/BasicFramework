﻿using System;

namespace Validate.Library
{
    /// <summary>
    /// 验证类型
    /// </summary>
    [Flags]
    public enum ValidateType
    {
        /// <summary>
        /// 字段或属性是否为空字串
        /// </summary>
        NotEmpty = 0x0001,
        /// <summary>
        /// 字段或属性的最小长度
        /// </summary>
        MinLength = 0x0002,
        /// <summary>
        /// 字段或属性的最大长度
        /// </summary>
        MaxLength = 0x0004,

        /// <summary>
        /// 字段或属性的值是否包含在指定的数据源数组中 eg：ValidateType.IsInCustomArray, CustomArray = new string[] { "现结", "到付", "月结" }
        /// </summary>
        InCustomArray = 0x0010,

        /// <summary>
        /// 匹配正则表达式
        /// </summary>
        MatchingRegex= 0x0040

    }
}
