﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Validate.Library
{
    /// <summary>
    /// 定义一些常用操作, 方便业务调用.
    /// </summary>
    public static class ResourceOperations
    {
        /// <summary>
        /// 创建
        /// </summary>
        public static OperationAuthorizationRequirement Create = new OperationAuthorizationRequirement { Name = "Create" };
        /// <summary>
        /// 查看
        /// </summary>
        public static OperationAuthorizationRequirement Read = new OperationAuthorizationRequirement { Name = "Read" };
        /// <summary>
        /// 修改
        /// </summary>
        public static OperationAuthorizationRequirement Update = new OperationAuthorizationRequirement { Name = "Update" };
        /// <summary>
        /// 删除
        /// </summary>
        public static OperationAuthorizationRequirement Delete = new OperationAuthorizationRequirement { Name = "Delete" };
    }
}
