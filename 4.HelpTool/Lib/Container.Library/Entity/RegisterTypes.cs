﻿using System.Collections.Generic;

namespace Container.Library
{
    /// <summary>
    /// 注入类型组信息
    /// </summary>
    public class RegisterTypes
    {
        /// <summary>
		/// 注入命令的集合
		/// </summary>
        public List<RegisterType> InstanceList { get; set; }
    }
}
