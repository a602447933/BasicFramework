﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Container.Library
{
    public class RegisterMapping
    {

        /// <summary>
        /// 配置文件的路径名
        /// </summary>
        private const string fileConfigPath = @"Config\Mapper\files.config.json";

        /// <summary>
        /// 文件存放的文件夹路径
        /// </summary>
        private const string directoryPath = @"Config\Mapper\";

        /// <summary>
        /// 配置文件全路径
        /// </summary>
        private static string fileConfigFullName;

        /// <summary>
        /// 映射文件全路径
        /// </summary>
        private static string directoryFullPath;

        /// <summary>
        /// 集合
        /// </summary>
        private static List<RegisterType> instanceList;

        /// <summary>
        /// 侦听文件系统对象
        /// </summary>
        private static FileSystemWatcher sWatcher = new FileSystemWatcher()
        {
            NotifyFilter = NotifyFilters.CreationTime | NotifyFilters.LastWrite
        };

        static RegisterMapping()
        {
            string rootDic = string.Empty;

            rootDic = AppDomain.CurrentDomain.BaseDirectory;

            fileConfigFullName = string.Format("{0}{1}", rootDic, fileConfigPath);
            directoryFullPath = string.Format("{0}{1}", rootDic, directoryPath);

            sWatcher.Changed += (s, e) => { InitList(); };
            sWatcher.Path = Path.GetDirectoryName(directoryFullPath);
            InitList();
            sWatcher.EnableRaisingEvents = true;
        }

        /// <summary>
        /// 同步锁
        /// </summary>
        private static readonly object locker = new object();

        /// <summary>
        /// 初始化集合
        /// </summary>
        private static void InitList()
        {
            lock (locker)
            {
                instanceList = new List<RegisterType>();

                // 获取命令集合
                var files = GetDataCommandFileList();

                RegisterTypes types = null;

                foreach (var fileInstance in files.InstanceList)
                {
                    types = $"{directoryFullPath}{fileInstance.FilePath}".FileToObject<RegisterTypes>();

                    if (types == null) continue;

                    foreach (var commandInstance in types.InstanceList)
                    {

                        var count = instanceList.Count(p => p.ImplementOrProfile.Equals(commandInstance.ImplementOrProfile));
                        if (count > 1)
                        {
                            throw new AggregateException("已经加入相同的键:" + commandInstance.ImplementOrProfile);
                        }
                        else
                        {
                            instanceList.Add(commandInstance);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// 获取命令文件
        /// </summary>
        /// <returns>命令文件集合</returns>
        private static ConfigFiles GetDataCommandFileList()
        {
            return fileConfigFullName.FileToObject<ConfigFiles>();
        }

        /// <summary>
        /// 返回注入类信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static List<RegisterType> GetTypesList()
        {
            return instanceList;
        }
    }
}
