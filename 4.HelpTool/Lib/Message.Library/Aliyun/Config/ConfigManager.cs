﻿using System;
using System.IO;
using System.Text;
using Encrypt.Library;

namespace Message.Library.Aliyun
{
    /// <summary>
    /// 创建工厂
    /// </summary>
    public class ConfigManager
    {

        /// <summary>
        ///  Aliyun配置信息
        /// </summary>
        internal static AliyunConfig config { get; private set; }

        static ConfigManager()
        {
            try
            {
                string rootDic = string.Empty;
                //相对路径
                rootDic = Directory.GetCurrentDirectory();
                //rootDic = AppDomain.CurrentDomain.BaseDirectory;
                //rootDic = AppContext.BaseDirectory;
                Encoding encoding = Encoding.Default;
                string filePath = Path.Combine(rootDic, "Config/Message/aliyun.config.json");
                //获取当前程序bin目录路径
                using (StreamReader sr = new StreamReader(filePath, GetEncoding(filePath)))
                {
                    config = sr.ReadToEnd().JsonToObject<AliyunConfig>();
                    if (config.IsEncryption)
                    {
                        config.SignName = AESEncryptHelper.Decrypt(config.SignName);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 获取文件Encoding格式
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static Encoding GetEncoding(string filePath)
        {
            if (filePath == null)
            {
                throw new ArgumentNullException("filePath");
            }
            Encoding encoding = Encoding.Default;
            if (File.Exists(filePath))
            {
                try
                {
                    using (FileStream stream1 = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                    {
                        if (stream1.Length > 0)
                        {
                            using (StreamReader reader1 = new StreamReader(stream1, true))
                            {
                                char[] chArray1 = new char[1];
                                reader1.Read(chArray1, 0, 1);
                                encoding = reader1.CurrentEncoding;
                                reader1.BaseStream.Position = 0;
                                if (encoding == Encoding.UTF8)
                                {
                                    byte[] buffer1 = encoding.GetPreamble();
                                    if (stream1.Length >= buffer1.Length)
                                    {
                                        byte[] buffer2 = new byte[buffer1.Length];
                                        stream1.Read(buffer2, 0, buffer2.Length);
                                        for (int num1 = 0; num1 < buffer2.Length; num1++)
                                        {
                                            if (buffer2[num1] != buffer1[num1])
                                            {
                                                encoding = Encoding.Default;
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        encoding = Encoding.Default;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                if (encoding == null)
                {
                    encoding = Encoding.UTF8;
                }
            }
            return encoding;
        }
    }
}
