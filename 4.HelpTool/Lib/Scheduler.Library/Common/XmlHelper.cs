﻿using System.IO;
using System.Xml.Serialization;

namespace Scheduler.Library
{
    /// <summary>
    /// XML序列化帮助类
    /// </summary>
    internal static class XmlHelper
    {
        /// <summary>
        /// 序列化成bytes。
        /// </summary>
        /// <typeparam name="T">消息的类型。</typeparam>
        /// <param name="message">消息的实例。</param>
        /// <returns></returns>
        internal static byte[] ToXmlBytes<T>(this T message)
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var msStream = new MemoryStream())
            {
                xmlSerializer.Serialize(msStream, message);
                return msStream.ToArray();
            }
        }

        /// <summary>
        /// 将对象序列化为XML文件
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="fileName">文件路径</param>
        /// <param name="data"></param>
        internal static void ToXml<T>(this T data,string fileName)
        {
            FileStream fs = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                fs = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                serializer.Serialize(fs, data);
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }

        /// <summary>
        /// 序列化消息为XML字符串。
        /// </summary>
        /// <param name="message">消息类型</param>
        /// <typeparam name="T">消息实例</typeparam>
        /// <returns></returns>
        internal static string ToXml<T>(this T message) where T : class, new()
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var msStream = new StringWriter())
            {
                xmlSerializer.Serialize(msStream, message);
                return msStream.ToString();
            }
        }

        /// <summary>
        /// 反序列化消息。
        /// </summary>
        /// <typeparam name="T">消息的类型。</typeparam>
        /// <param name="bytes">bytes。</param>
        /// <returns></returns>
        internal static T XmlToObject<T>(this byte[] bytes) where T : class, new()
        {
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var msStream = new MemoryStream(bytes))
            {
                return xmlSerializer.Deserialize(msStream) as T;
            }
        }

        /// <summary>
        /// 将XML文件反序列化为对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="fileName">文件路径</param>
        /// <returns>序列化的对象</returns>
        internal static T XmlToObject<T>(string fileName) where T : class
        {
            FileStream fs = null;
            T result;
            try
            {
                fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                result = (T)serializer.Deserialize(fs);
            }
            catch //(Exception ex)
            {
                result = default(T);
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
            return result;
        }

    }
}
