﻿namespace Scheduler.Library
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 调度任务的封装
    /// </summary>
    public static class Scheduler
    {
        private static readonly IScheduler scheduler = new SchedulerService();

        /// <summary>
        /// 调度一个新任务
        /// </summary>
        /// <param name="name">任务名称</param>
        /// <param name="trigger">任务触发器</param>
        /// <param name="start">是否立即开始</param>
        /// <param name="methodToExecute">任务内容</param>
        public static void ScheduleTask(string name, Trigger trigger, bool start, Action methodToExecute)
        {
            try
            {
                scheduler.Schedule(name, trigger, methodToExecute, null, start);
            }
            catch
            {
                throw new FormatException("调度一个新任务出错");
            }
        }

        /// <summary>
        /// 调度并立即启动一个新任务
        /// </summary>
        /// <param name="name">任务名称</param>
        /// <param name="trigger">任务触发器</param>
        /// <param name="methodToExecute">任务内容</param>
        public static void ScheduleTask(string name, Trigger trigger, Action methodToExecute)
        {
            try
            {
                scheduler.Schedule(name, trigger, methodToExecute, null, true);
            }
            catch
            {
                throw new FormatException("调度并立即启动一个新任务出错");
            }
        }

        /// <summary>
        /// 调度并立即启动一个新任务
        /// </summary>
        /// <param name="name">任务的名称</param>
        /// <param name="trigger">任务的触发器</param>        
        /// <param name="methodToExecute">任务内容</param>
        /// <param name="onCompletedAction">完成任务的回调</param>
        public static void ScheduleTask(string name, Trigger trigger, Action methodToExecute, Action<SchedulerTask> onCompletedAction)
        {
            try
            {
                scheduler.Schedule(name, trigger, methodToExecute, onCompletedAction, true);
            }
            catch
            {
                throw new FormatException("调度并立即启动一个新任务出错");
            }
        }

        /// <summary>
        /// 调度并立即启动一个新任务
        /// </summary>
        /// <param name="trigger">任务的触发器</param>
        /// <param name="methodToExecute">任务内容</param>
        public static void ScheduleTask(Trigger trigger, Action methodToExecute)
        {
            try
            {
                scheduler.Schedule(Guid.NewGuid().ToString().Replace("-", "").ToUpper(), trigger, methodToExecute, null, true);
            }
            catch
            {
                throw new FormatException("调度并立即启动一个新任务出错");
            }
        }

        /// <summary>
        /// 调度并立即启动一个新任务
        /// </summary>
        /// <param name="trigger">任务的触发器</param>
        /// <param name="methodToExecute">任务内容</param>
        /// <param name="onCompletedAction">完成任务的回调</param>
        public static void ScheduleTask(Trigger trigger, Action methodToExecute, Action<SchedulerTask> onCompletedAction)
        {
            try
            {
                scheduler.Schedule(Guid.NewGuid().ToString().Replace("-", "").ToUpper(), trigger, methodToExecute, onCompletedAction, true);
            }
            catch
            {
                throw new FormatException("调度并立即启动一个新任务出错");
            }
        }

        /// <summary>
        /// 是否正在运行
        /// </summary>
        public static bool IsStarted => scheduler.IsStarted;

        /// <summary>
        /// 是否已经停止
        /// </summary>
        public static bool IsShutDown => scheduler.IsShutDown;

        /// <summary>
        /// 暂停指定任务
        /// </summary>
        /// <param name="name">任务名称</param>
        public static void Pause(string name)
        {
            try
            {
                scheduler.Pause(name);
            }
            catch
            {

                throw new FormatException("暂停指定任务出错");
            }
        }

        /// <summary>
        /// 运行指定的任务
        /// </summary>
        /// <param name="name">任务名称</param>
        public static void Run(string name)
        {
            try
            {
                scheduler.Run(name);
            }
            catch
            {
                throw new FormatException("运行指定的任务出错");
            }
        }

        /// <summary>
        /// 恢复指定任务
        /// </summary>
        /// <param name="name">任务名称</param>
        public static void Resume(string name)
        {
            try
            {
                scheduler.Resume(name);
            }
            catch
            {
                throw new FormatException("恢复指定任务出错");
            }

        }

        /// <summary>
        /// 删除指定任务
        /// </summary>
        /// <param name="name">任务名称</param>
        public static void Delete(string name)
        {
            try
            {
                scheduler.Delete(name);
            }
            catch
            {
                throw new FormatException("删除指定任务出错");
            }
        }

        /// <summary>
        /// 返回任务名称数组
        /// </summary>
        /// <returns>任务名称数组</returns>
        public static string[] GetTaskNames()
        {
            try
            {
                return scheduler.GetTaskNames();
            }
            catch
            {
                throw new FormatException("返回任务名称数组出错");
            }
        }

        /// <summary>
        /// 暂停所有任务
        /// </summary>
        public static void PauseAll()
        {
            try
            {
                scheduler.PauseAll();
            }
            catch (Exception)
            {
                throw new FormatException("暂停所有任务出错");
            }
        }

        /// <summary>
        /// 恢复所有任务
        /// </summary>
        public static void ResumeAll()
        {
            try
            {
                scheduler.ResumeAll();
            }
            catch
            {
                throw new FormatException("恢复所有暂停任务出错");
            }
        }

        /// <summary>
        /// 获取所有任务的摘要信息
        /// </summary>
        /// <returns>所有任务的摘要信息</returns>
        public static IList<TaskSummary> GetStatuses()
        {
            try
            {
                return scheduler.GetStatus();
            }
            catch
            {
                throw new FormatException("获取所有任务的摘要信息出错");
            }
        }

        /// <summary>
        /// 获取指定任务摘要信息
        /// </summary>
        /// <param name="name">任务名称</param>
        /// <returns>任务摘要信息</returns>
        public static TaskSummary GetStatus(string name)
        {
            try
            {
                return scheduler.GetStatus(name);
            }
            catch
            {
                throw new FormatException("获取指定任务摘要信息出错");
            }
        }

        /// <summary>
        /// 关闭调度服务
        /// </summary>
        public static void ShutDown()
        {
            try
            {
                if (scheduler.IsStarted)
                    scheduler.ShutDown();
            }
            catch (Exception)
            {
                throw new FormatException("关闭调度服务出错");
            }
        }

        /// <summary>
        /// 开启调度服务
        /// </summary>
        public static void StartUp()
        {
            try
            {
                if (scheduler.IsShutDown)
                    scheduler.StartUp();
            }
            catch (Exception)
            {

                throw new FormatException("开启调度服务出错");
            }
        }
    }
}
