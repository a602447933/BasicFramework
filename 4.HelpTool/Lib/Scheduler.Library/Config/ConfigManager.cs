﻿using System;
using System.IO;

namespace Scheduler.Library
{
    /// <summary>
    /// 用于读取缓存的配置信息
    /// </summary>
    public class ConfigManager
    {
        /// <summary>
        /// 数据库配置信息
        /// </summary>
        public static RedisConfig config { get; private set; }

        /// <summary>
        /// 静态代码块，程序启动时初始化
        /// </summary>
        static ConfigManager()
        {
            try
            {
                //string rootDic  = AppContext.BaseDirectory;
                string rootDic = AppDomain.CurrentDomain.BaseDirectory;
                using (StreamReader sr = new StreamReader(Path.Combine(rootDic, "Config\\Redis\\redis.config.json")))
                {
                    config = sr.ReadToEnd().JsonToObject<RedisConfig>();
                    if (config.IsEncryption)
                    {
                        config.Password = AESEncryptHelper.Decrypt(config.Password);
                        config.Masters = AESEncryptHelper.Decrypt(config.Masters);
                    }
                    config.Success = true;
                }
            }
            catch (Exception ex)
            {
               config.Success = false;
               config.Ex = ex.Message;
            }
        }
    }
}
