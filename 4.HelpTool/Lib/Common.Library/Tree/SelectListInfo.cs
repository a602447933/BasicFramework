﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Library
{
    /// <summary>
    /// 选择下拉框实体
    /// </summary>
    public class SelectListInfo
    {
        /// <summary>
        ///构造函数
        /// </summary>
        public SelectListInfo() {

            children = new List<SelectListInfo>();

        }
        /// <summary>
        /// 显示信息
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string value { get; set; }

        /// <summary>
        /// 类型 optgroup
        /// </summary>
        public string type  { get; set; }

        /// <summary>
        /// 是否选中
        /// </summary>
        public string selected { get; set; }

        /// <summary>
        /// 是否不可用
        /// </summary>
        public string disabled { get; set; }

        /// <summary>
        /// 子节点
        /// </summary>
        public List<SelectListInfo> children { get; set; }

    }
}
