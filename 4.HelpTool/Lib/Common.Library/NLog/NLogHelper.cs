﻿using NLog;
using NLog.Config;
using System;
using System.Threading.Tasks;

namespace Common.Library
{
    /// <summary>
    /// NLog日志操作
    /// </summary>
    public class NLogHelper
    {
        private static Logger logger = LogManager.GetCurrentClassLogger(); //初始化日志类

        /// <summary>
        /// 静态构造函数
        /// </summary>
        static NLogHelper()
        {
            //初始化配置日志
            LogManager.Configuration = new XmlLoggingConfiguration(System.AppDomain.CurrentDomain.BaseDirectory.ToString() + "\\Config\\nlog.config");
        }

        /// <summary>
        /// 记录信息日志
        /// </summary>        
        /// <param name="title">要显示的消息标题</param>
        /// <param name="message">要显示的信息</param>
        public static void LogInfo(string title, string message)
        {
            Task.Run(() =>
            {
                try
                {
                    logger?.Info($"{Environment.NewLine}------------------------------------------------------------{Environment.NewLine}{title}\r\n{message}");
                }
                catch (Exception exception)
                {
                    //NLog:catch setup errors
                    logger.Error(exception, "Stopped program because of exception");
                }
                //finally
                //{
                //    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                //    NLog.LogManager.Shutdown();
                //}
            });
        }

        /// <summary>
        /// 调试信息，详尽信息次于 Trace ，在生产环境中通常不启用
        /// </summary>
        /// <param name="title">要显示的消息标题</param>
        /// <param name="ex">异常信息</param>
        public static void LogDebug(string title, Exception exp)
        {
            Task.Run(() =>
            {
                try
                {
                    logger?.Debug(exp, $"{Environment.NewLine}------------------------------------------------------------{Environment.NewLine}{"Debug异常"}:{title}\r\n{exp.StackTrace}");
                }
                catch (Exception exception)
                {
                    //NLog:catch setup errors
                    logger.Error(exception, "Stopped program because of exception");
                }
                //finally
                //{
                //    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                //    NLog.LogManager.Shutdown();
                //}
            });
        }

        /// <summary>
        /// 调试信息，详尽信息次于 Trace ，在生产环境中通常不启用
        /// </summary>
        /// <param name="title">要显示的消息标题</param>
        /// <param name="ex">异常信息</param>
        public static void LogWarn(string title, Exception exp)
        {
            Task.Run(() =>
            {
                try
                {
                    logger?.Warn(exp, $"{Environment.NewLine}------------------------------------------------------------{Environment.NewLine}{"Warn异常"}:{title}\r\n{exp.StackTrace}");
                }
                catch (Exception exception)
                {
                    //NLog:catch setup errors
                    logger.Error(exception, "Stopped program because of exception");
                }
                //finally
                //{
                //    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                //    NLog.LogManager.Shutdown();
                //}
            });
        }

        /// <summary>
        /// 错误异常日志记录
        /// </summary>
        /// <param name="title"></param>
        /// <param name="exp"></param>
        public static void LogError(string title, Exception exp)
        {
            Task.Run(() =>
            {
                try
                {
                    logger?.Error(exp, $"{Environment.NewLine}------------------------------------------------------------{Environment.NewLine}{"Error异常"}:{title}\r\n{exp.StackTrace}");
                }
                catch (Exception exception)
                {
                    //NLog:catch setup errors
                    logger.Error(exception, "Stopped program because of exception");
                }
                //finally
                //{
                //    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                //    NLog.LogManager.Shutdown();
                //}
            });
        }

        /// <summary>
        /// 非常严重的错误
        /// </summary>
        /// <param name="title"></param>
        /// <param name="exp"></param>
        public static void LogFatal(string title, Exception exp)
        {
            Task.Run(() =>
            {
                try
                {
                    logger?.Fatal(exp, $"{Environment.NewLine}------------------------------------------------------------{Environment.NewLine}{"Fatal异常"}:{title}\r\n{exp.StackTrace}");
                }
                catch (Exception exception)
                {
                    //NLog:catch setup errors
                    logger.Error(exception, "Stopped program because of exception");
                }
                //finally
                //{
                //    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                //    NLog.LogManager.Shutdown();
                //}
            });
        }

        /// <summary>
        /// Dapper异常日志记录
        /// </summary>
        /// <param name="title"></param>
        /// <param name="msg"></param>
        public static void LogDapperError(string title, string  msg)
        {
            Task.Run(() =>
            {
                try
                {
                    logger?.Error($"{Environment.NewLine}{"Dapper异常"}:{title}\r\n{msg}");
                }
                catch (Exception exception)
                {
                    //NLog:catch setup errors
                    logger.Error(exception, "Stopped program because of exception");
                }
                //finally
                //{
                //    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
                //    NLog.LogManager.Shutdown();
                //}
            });
        }
    }
}
