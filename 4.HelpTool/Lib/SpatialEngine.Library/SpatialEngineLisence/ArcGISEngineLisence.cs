﻿/***************************************************************************************************
  	创建时间:	2018-09-10
	文件名字:	ArcGISEngineLisence.cs
	作    者:	周业
	版    本:	V1.000.000
	说    明:	ArcEngine许可注册类，统一注册ArcEngine
	历史记录：	
	<作  者>		<修改时间>		  <版  本>		       <目  的>	
****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESRI.ArcGIS.esriSystem;

namespace SpatialEngine.Library
{
    public class ArcGISEngineLisence
    {
        /// <summary>
        /// 许可初始化变量
        /// </summary>
        private IAoInitialize _aoInitialize;
        /// <summary>
        /// 初始化许可
        /// </summary>
        public void InitialLincese()
        {
            try
            {
                #region
                /*
                if (!ESRI.ArcGIS.RuntimeManager.Bind(ESRI.ArcGIS.ProductCode.Engine))
                {
                    return;
                }
                _aoInitialize = new AoInitializeClass();
                if (_aoInitialize.IsProductCodeAvailable(esriLicenseProductCode.esriLicenseProductCodeEngineGeoDB) == esriLicenseStatus.esriLicenseAvailable &&
                    _aoInitialize.Initialize(esriLicenseProductCode.esriLicenseProductCodeEngineGeoDB) == esriLicenseStatus.esriLicenseCheckedOut)
                {
                }
                else
                {
                    return;
                }
                 */
                #endregion
                ESRI.ArcGIS.RuntimeManager.Bind(ESRI.ArcGIS.ProductCode.EngineOrDesktop);
                _aoInitialize = new AoInitializeClass();
                if (_aoInitialize == null)
                {
                }
                esriLicenseStatus status = _aoInitialize.IsProductCodeAvailable(esriLicenseProductCode.esriLicenseProductCodeAdvanced);//10.1版本专用
                if (status == esriLicenseStatus.esriLicenseAvailable)
                {
                    _aoInitialize.Initialize(esriLicenseProductCode.esriLicenseProductCodeAdvanced);

                }
                else if (status == esriLicenseStatus.esriLicenseUnavailable)
                {
                }
                status = _aoInitialize.IsExtensionCodeAvailable(esriLicenseProductCode.esriLicenseProductCodeAdvanced, esriLicenseExtensionCode.esriLicenseExtensionCode3DAnalyst);
                if (status == esriLicenseStatus.esriLicenseAvailable)
                {
                    status = _aoInitialize.CheckOutExtension(esriLicenseExtensionCode.esriLicenseExtensionCode3DAnalyst);
                    if (status == esriLicenseStatus.esriLicenseNotInitialized)
                    {
                    }
                }
                else if (status == esriLicenseStatus.esriLicenseNotLicensed)
                {
                }
                else if (status == esriLicenseStatus.esriLicenseUnavailable)
                {
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 关闭许可
        /// </summary>
        public void ShutDownLicense()
        {
            if (_aoInitialize != null)
            {
                _aoInitialize.Shutdown();
            }
        }
    }
}
