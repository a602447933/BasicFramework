﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SpatialEngine.Library.SpatialArcGISEngineBase;

namespace SpatialEngine.Library
{
    public class SpatialEngineTest
    {

        public void Test()
        {
            SpatialArcGISEngineBase oSpatialEngineBase = SpatialEngineFactory.CreateSpatialEngineBase();
            PointStruct pPointStruct = new PointStruct();
            pPointStruct.X = 1.0;
            pPointStruct.Y = 1.0;

            var patroltaskPoint = new Dictionary<string, object>();
            patroltaskPoint.Add("objectid",9);
            patroltaskPoint.Add("task_id", "3043c1621c454e46a684e1722f7cee14");
            patroltaskPoint.Add("task_name", "task_name");
            patroltaskPoint.Add("x", "107.497855423883");
            patroltaskPoint.Add("y", "31.2203621580406");
            oSpatialEngineBase.CreateNewPointFeature("sczhwater.watersde.patroltask_point", pPointStruct, patroltaskPoint);
        }

    }
}
