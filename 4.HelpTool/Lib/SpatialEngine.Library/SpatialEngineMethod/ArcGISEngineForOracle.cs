﻿/***************************************************************************************************
  	创建时间:	2018-09-10
	文件名字:	ArcGISEngineForOracle.cs
	作    者:	周业
	版    本:	V1.000.000
	说    明:	空间数据库引擎操作的辅助类，统一操作基于Oracle数据库的Arcgis SDE。
	历史记录：	
	<作  者>		<修改时间>		  <版  本>		       <目  的>	
****************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Windows.Forms;
using ESRI.ArcGIS.Geodatabase;
using ESRI.ArcGIS.esriSystem;
using ESRI.ArcGIS.DataSourcesGDB;
using ESRI.ArcGIS.Carto;
using ESRI.ArcGIS.Geometry;
using BoatLeaf.Import.FileHelper;
using BoatLeaf.Certification;

namespace SpatialEngine.Library
{
    public class ArcGISEngineForOracle:SpatialArcGISEngineBase
    {
        #region 变量和属性

        //XElement对象
        private XElement _oXElement;

        #endregion

        #region 结构体和枚举

        #endregion

        #region 构造和析构函数

        /// <summary>
        /// 无参构造函数，初始化 SpatialEngine.Library.SpatialEngineMethod.ArcGISEngineForOracle 类的新实例
        /// </summary>
        public ArcGISEngineForOracle()
        {
            XElement oElement = GetDatabaseConnectElment("", "");
            ArcGISEngineLisence pArcGISEngineLisence = new ArcGISEngineLisence();
            try
            {
                pArcGISEngineLisence.InitialLincese();
                IPropertySet Property = new PropertySetClass();
                Property.SetProperty("SERVER", oElement.Descendants("server").First().Value);
                Property.SetProperty("INSTANCE", oElement.Descendants("instance").First().Value);
                Property.SetProperty("DATABASE", oElement.Descendants("database").First().Value);
                Property.SetProperty("USER", oElement.Descendants("user").First().Value);
                Property.SetProperty("PASSWORD", oElement.Descendants("password").First().Value);
                Property.SetProperty("VERSION", oElement.Descendants("version").First().Value);
                IWorkspaceFactory pWorkspaceFactory = new SdeWorkspaceFactoryClass();//创建数据库连接类
                pWorkSpace = pWorkspaceFactory.Open(Property, 0);//SDE连接
                Property = null;
                GC.Collect();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                pArcGISEngineLisence.ShutDownLicense();
            }
        }

        /// <summary>
        /// 构造函数，初始化 SpatialEngine.Library.SpatialEngineMethod.ArcGISEngineForOracle 类的新实例
        /// </summary>
        /// <param name="strXmlFilePath">xml文件路径</param>
        /// <param name="strXmlNodePath">节点路径</param>
        public ArcGISEngineForOracle(string strXmlFilePath, string strXmlNodePath)
        {
            XElement oElement = GetDatabaseConnectElment(strXmlFilePath, strXmlNodePath);
            ArcGISEngineLisence pArcGISEngineLisence = new ArcGISEngineLisence();
            try
            {
                pArcGISEngineLisence.InitialLincese();
                IPropertySet Property = new PropertySetClass();
                Property.SetProperty("SERVER", oElement.Descendants("server").First().Value);
                Property.SetProperty("INSTANCE", oElement.Descendants("instance").First().Value);
                Property.SetProperty("DATABASE", oElement.Descendants("database").First().Value);
                Property.SetProperty("USER", oElement.Descendants("user").First().Value);
                Property.SetProperty("PASSWORD", oElement.Descendants("password").First().Value);
                Property.SetProperty("VERSION", oElement.Descendants("version").First().Value);
                IWorkspaceFactory pWorkspaceFactory = new SdeWorkspaceFactoryClass();//创建数据库连接类
                pWorkSpace = pWorkspaceFactory.Open(Property, 0);//SDE连接
                Property = null;
                GC.Collect();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                pArcGISEngineLisence.ShutDownLicense();
            }
        }

        #endregion

        #region 类事件

        #endregion

        #region 类方法

        #region 动态方法

        /// <summary>
        /// 获取数据库连接字符串
        /// </summary>
        /// <param name="strXmlFilePath">String类型，xml文件路径</param>
        /// <param name="strXmlNodePath">String类型，节点路径</param>
        /// <returns>XElement类型，返回数据库连接元素</returns>
        private XElement GetDatabaseConnectElment(string strXmlFilePath, string strXmlNodePath)
        {
            try
            {
                if (strXmlFilePath.Trim() != "" && strXmlNodePath.Trim() != "")
                {
                    if (System.IO.File.Exists(strXmlFilePath))
                    {
                        _oXElement = XElement.Load(strXmlFilePath);
                        return _oXElement.XPathSelectElement(strXmlNodePath);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    string strPath = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, "Config\\system.xml");
                    _oXElement = XElement.Load(strPath);
                    return _oXElement.XPathSelectElement(@"/spatialengineconfig/spatialengineconstr");
                }
            }
            catch (Exception ex)
            {
                TxtHelper.WriteLog(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// 解密字符串
        /// </summary>
        /// <param name="strEncrypt">String类型，加密的密文</param>
        /// <returns>String类型，解密后的明文</returns>
        private string GetRsaDecryptString(string strEncrypt)
        {
            try
            {
                string strTextToDecrypt = strEncrypt.Trim().Substring(0, strEncrypt.Trim().IndexOf("\r\n"));
                string strPrivateKey = strEncrypt.Trim().Substring(strEncrypt.Trim().IndexOf("\r\n")).Replace("\r\n", "");
                RsaSecurity oRsaSecurity = new RsaSecurity();
                return oRsaSecurity.RsaDecrypt(strTextToDecrypt, strPrivateKey);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// 按照类别获取图层集合
        /// </summary>
        /// <param name="pMap">IMap接口类型，地图接口</param>
        /// <param name="strUidValue">String类型，UID值</param>
        /// <param name="pEsriGeometryType">图层类型（点，线，面）</param>
        /// <remarks>（FeatureLayer:{40A9E885-5533-11d0-98BE-00805F7CED21},DataLayer:{6CA416B1-E160-11D2-9F4E-00C04F6BC78E},GeoFeatureLayer:{E156D7E5-22AF-11D3-9F99-00C04F6BC78E},
        /// GraphicsLayer:{34B2EF81-F4AC-11D1-A245-080009B6F22B},FDOGraphicsLayer{5CEAE408-4C0A-437F-9DB3-054D83919850},ConverageAnnotationLayer{0C22A4C7-DAFD-11D2-9F46-00C04F6BC78E},
        /// RasterLayer:{D02371C7-35F7-11D2-B1F2-00C04F8EDEFF},GroupLayer:{EDAD6644-1810-11D1-86AE-0000F8751720},all datalayer:{6CA416B1-E160-11D2-9F4E-00C04F6BC78E}）</remarks>
        /// <returns></returns>
        public List<ILayer> GetAllLayerByType(IMap pMap, string strUidValue, esriGeometryType pEsriGeometryType)
        {
            try
            {
                List<ILayer> pResultList = new List<ILayer>();
                if (pMap != null && pMap.LayerCount > 0)
                {
                    UID pUID = new UIDClass();
                    pUID.Value = strUidValue;
                    IEnumLayer pEnumLayer = pMap.get_Layers(pUID, true);
                    ILayer pLayer = pEnumLayer.Next();
                    while (pLayer != null)
                    {
                        if (pEsriGeometryType == esriGeometryType.esriGeometryNull)
                        {
                            pResultList.Add(pLayer);
                        }
                        else
                        {
                            if (((IFeatureLayer)pLayer).FeatureClass.ShapeType == pEsriGeometryType)
                            {
                                pResultList.Add(pLayer);
                            }
                        }
                        pLayer = pEnumLayer.Next();
                    }
                }
                return pResultList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 遍历Map的图层
        /// </summary>
        /// <param name="pMap">IMap接口类型，地图接口</param>
        /// <param name="strLayerName">String类型，图层名</param>
        /// <returns>ILayer接口类型，返回查询到的图层</returns>
        public ILayer SearchLayerFromMap(IMap pMap, string strLayerName)
        {
            try
            {
                ILayer pResultLayer = null;
                for (int i = 0; i < pMap.LayerCount; i++)
                {
                    ILayer pLayer = pMap.get_Layer(i);
                    if (pLayer is GroupLayer || pLayer is ICompositeLayer)
                    {
                        pResultLayer = SarchLayerFromSubLayer(pLayer, strLayerName);
                        if (pResultLayer != null)
                        {
                            break;
                        }
                    }
                    else
                    {
                        if (pLayer.Name.Equals(strLayerName))
                        {
                            pResultLayer = pLayer;
                            break;
                        }
                    }
                }
                return pResultLayer;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 递归遍历子图层
        /// </summary>
        /// <param name="pLayer">ILayer接口类型，父级图层</param>
        /// <param name="strLayerName">String类型，图层名</param>
        /// <returns></returns>
        public ILayer SarchLayerFromSubLayer(ILayer pLayer, string strLayerName)
        {
            try
            {
                ILayer pResultLayer = null;
                ICompositeLayer pCompositeLayer = pLayer as ICompositeLayer;
                for (int i = 0; i < pCompositeLayer.Count; i++)
                {
                    ILayer pSubLayer = pCompositeLayer.Layer[i];
                    if (pSubLayer is GroupLayer || pSubLayer is ICompositeLayer)
                    {
                        pResultLayer = SarchLayerFromSubLayer(pSubLayer, strLayerName);
                        if (pResultLayer != null)
                        {
                            break;
                        }
                    }
                    else
                    {
                        while (pSubLayer.Name.Equals(strLayerName))
                        {
                            pResultLayer = pSubLayer;
                            break;
                        }
                    }
                }
                return pResultLayer;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取要素图层
        /// </summary>
        /// <param name="pEnumDataset">IEnumDataset类型，可枚举的数据集</param>
        /// <returns>List&lt;IFeatureLayer&gt;返回要素图层集合</returns>
        public List<IFeatureLayer> GetFeatureLayer(IEnumDataset pEnumDataset)
        {
            List<IFeatureLayer> pList = new List<IFeatureLayer>();
            if (pEnumDataset != null)
            {
                for (IDataset pDataset = pEnumDataset.Next(); pDataset != null; pDataset = pEnumDataset.Next())
                {
                    if (pDataset.Type == esriDatasetType.esriDTFeatureClass)
                    {
                        IFeatureLayer pFeatureLayer = new FeatureLayerClass();
                        pFeatureLayer.FeatureClass = pDataset as IFeatureClass;
                        pFeatureLayer.Name = pDataset.Name;
                        pList.Add(pFeatureLayer);
                    }
                }

            }
            return pList;
        }

        /// <summary>
        /// 根据要素数据集名称获取要素图层集合
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="nameOfFeatureDataset">String类型，要素数据集名称</param>
        /// <returns></returns>
        public List<IFeatureLayer> GetFeatureLayer(IWorkspace pWorkspace, string nameOfFeatureDataset)
        {
            IFeatureDataset pFeatureDataset = GetFeatureDataset(pWorkspace, nameOfFeatureDataset);
            List<IFeatureLayer> pList = new List<IFeatureLayer>();
            IEnumDataset pEnumDataset = pFeatureDataset.Subsets;
            pEnumDataset.Reset();
            if (pEnumDataset != null)
            {
                for (IDataset pDataset = pEnumDataset.Next(); pDataset != null; pDataset = pEnumDataset.Next())
                {
                    if (pDataset.Type == esriDatasetType.esriDTFeatureClass)
                    {
                        IFeatureLayer pFeatureLayer = new FeatureLayerClass();
                        pFeatureLayer.FeatureClass = pDataset as IFeatureClass;
                        pFeatureLayer.Name = pDataset.Name;
                        pList.Add(pFeatureLayer);
                    }
                }
            }
            return pList;
        }

        /// <summary>
        /// 根据图层名称集合获取要素图层集合
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="listLayerName">List&lt;string&gt;类型，图层名称集合</param>
        /// <returns>List&lt;IFeatureLayer&gt;类型，返回要素图层集合</returns>
        public List<IFeatureLayer> GetFeatureLayer(IWorkspace pWorkspace, List<string> listLayerName)
        {
            List<IFeatureLayer> pList = new List<IFeatureLayer>();
            foreach (string strLayerName in listLayerName)
            {
                IFeatureLayer pFeatueLayer = new FeatureLayerClass();
                pFeatueLayer.FeatureClass = GetFeatureClass(pWorkspace, strLayerName);
                pFeatueLayer.Name = strLayerName;
                if (pFeatueLayer != null)
                {
                    pList.Add(pFeatueLayer);
                }
            }
            return pList;
        }

        /// <summary>
        /// 获取要素类
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="nameOfFeatureClass">String类型，要素类名称</param>
        /// <returns>IFeatureClass接口类型，返回要素类</returns>
        public IFeatureClass GetFeatureClass(IWorkspace pWorkspace, string nameOfFeatureClass)
        {
            IFeatureClass pFeatureClass = null;
            if (pWorkspace != null)
            {
                if (!(pWorkspace is IFeatureWorkspace))
                {
                    return pFeatureClass;
                }
                IFeatureWorkspace pFCWorkspace = pWorkspace as IFeatureWorkspace;
                try
                {
                    pFeatureClass = pFCWorkspace.OpenFeatureClass(nameOfFeatureClass);
                }
                catch
                {
                }
            }
            return pFeatureClass;
        }

        /// <summary>
        /// 根据图层名称集合获取要素类集合
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="listLayerName">List&lt;string&gt;类型，图层名称集合</param>
        /// <returns>List&lt;IFeatureClass&gt;类型，返回要素类集合</returns>
        public List<IFeatureClass> GetFeatureClass(IWorkspace pWorkspace, List<string> listLayerName)
        {
            List<IFeatureClass> pList = new List<IFeatureClass>();
            foreach (string strLayerName in listLayerName)
            {
                IFeatureClass pFeatureClass = GetFeatureClass(pWorkspace, strLayerName);
                if (pFeatureClass != null)
                {
                    pList.Add(pFeatureClass);
                }
            }
            return pList;
        }

        /// <summary>
        /// 获取所有数据集集合
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <returns>List&lt;IDataset&gt;类型，返回数据集接口集合</returns>
        public List<IDataset> GetDataset(IWorkspace pWorkspace)
        {
            IEnumDataset pEnumDataset = pWorkspace.get_Datasets(esriDatasetType.esriDTAny);
            List<IDataset> pList = new List<IDataset>();
            if (pEnumDataset != null)
            {
                for (IDataset pDataset = pEnumDataset.Next(); pDataset != null; pDataset = pEnumDataset.Next())
                {
                    pList.Add(pDataset);
                }
            }
            return pList;
        }

        /// <summary>
        /// 获取要素数据集集合
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <returns>List&lt;IFeatureDataset&gt;类型，返回要素数据集接口集合</returns>
        public List<IFeatureDataset> GetFeatureDataset(IWorkspace pWorkspace)
        {
            IEnumDataset pEnumDataset = pWorkspace.get_Datasets(esriDatasetType.esriDTFeatureDataset);
            List<IFeatureDataset> pList = new List<IFeatureDataset>();
            if (pEnumDataset != null)
            {
                for (IDataset pDataset = pEnumDataset.Next(); pDataset != null; pDataset = pEnumDataset.Next())
                {
                    pList.Add(pDataset as IFeatureDataset);
                }
            }
            return pList;
        }

        /// <summary>
        /// 获取要素数据集
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="nameOfFeatureDataset">String类型，要素数据集名称</param>
        /// <returns>IFeatureDataset接口类型，返回要素数据集</returns>
        public IFeatureDataset GetFeatureDataset(IWorkspace pWorkspace, string nameOfFeatureDataset)
        {
            try
            {
                IFeatureDataset pFeatureDataset = null;
                if (nameOfFeatureDataset == null || nameOfFeatureDataset.Trim() == "") return pFeatureDataset;
                if (pWorkspace is IFeatureWorkspace)
                {
                    IFeatureWorkspace pFCWorkspace = pWorkspace as IFeatureWorkspace;
                    pFeatureDataset = pFCWorkspace.OpenFeatureDataset(nameOfFeatureDataset);
                }
                return pFeatureDataset;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 获取数据集名称
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <returns>List&lt;string&gt;类型，返回数据集名称集合</returns>
        public List<string> GetDatasetNames(IWorkspace pWorkspace)
        {
            IEnumDatasetName enumDatasetName = pWorkspace.get_DatasetNames(esriDatasetType.esriDTFeatureDataset);
            List<string> listDatasetNames = GetEnumDatasetNames(enumDatasetName);
            return listDatasetNames;
        }

        /// <summary>
        /// 获取枚举的数据集名称
        /// </summary>
        /// <param name="pEnumDatasetName">IEnumDatasetName接口类型，枚举的数据集名称</param>
        /// <returns>List&lt;string&gt;类型，返回数据集名称集合</returns>
        public List<string> GetEnumDatasetNames(IEnumDatasetName pEnumDatasetName)
        {
            List<string> pList = new List<string>();
            if (pEnumDatasetName != null)
            {
                for (IDatasetName datasetName = pEnumDatasetName.Next(); datasetName != null; datasetName = pEnumDatasetName.Next())
                {
                    pList.Add(datasetName.Name.ToUpper());
                }
            }
            return pList;
        }

        /// <summary>
        /// 获取枚举数据集名称
        /// </summary>
        /// <param name="pEnumDataset">IEnumDataset接口类型，枚举数据集</param>
        /// <returns>List&lt;string&gt;类型，返回数据集名称集合</returns>
        public List<string> GetEnumDatasetNames(IEnumDataset pEnumDataset)
        {
            List<string> pList = new List<string>();
            if (pEnumDataset != null)
            {
                for (IDataset pDataset = pEnumDataset.Next(); pDataset != null; pDataset = pEnumDataset.Next())
                {
                    pList.Add(pDataset.Name.ToUpper());
                }
            }
            return pList;
        }

        /// <summary>
        /// 获取要素List
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="nameOfFeatureClass">String类型，要素图层名</param>
        /// <param name="strWhereClause">String类型，要素查询条件</param>
        /// <returns>List&lt;IFeature&gt;类型，返回要素集合</returns>
        public List<IFeature> GetFeature(IWorkspace pWorkspace, string nameOfFeatureClass, string strWhereClause)
        {
            List<IFeature> pList = new List<IFeature>();
            IFeatureLayer pFeatureLayer = new FeatureLayerClass();
            pFeatureLayer.FeatureClass = GetFeatureClass(pWorkspace, nameOfFeatureClass);
            pFeatureLayer.Name = nameOfFeatureClass;
            IFeatureSelection pFeatureSelection = pFeatureLayer as IFeatureSelection;
            IQueryFilter pQueryFilter = new QueryFilterClass();
            IFeatureCursor pFeatureCursor;
            pQueryFilter.WhereClause = strWhereClause;
            pFeatureSelection.SelectFeatures(pQueryFilter, esriSelectionResultEnum.esriSelectionResultNew, false);
            ICursor pCursor;
            ISelectionSet pSelectionSet = pFeatureSelection.SelectionSet;
            pSelectionSet.Search(pQueryFilter, false, out pCursor);
            pFeatureCursor = pCursor as IFeatureCursor;
            IFeature pFeature = pFeatureCursor.NextFeature();
            while (pFeature != null)
            {
                pList.Add(pFeature);
                pFeature = pFeatureCursor.NextFeature();
            }

            return pList;
        }

        /// <summary>
        /// 获取Geometry List
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="nameOfFeatureClass">String类型，要素图层名</param>
        /// <param name="strWhereClause">String类型，要素查询条件</param>
        /// <param name="BufferRadius">String类型，缓冲区半径</param>
        /// <returns>List&lt;IGeometry&gt;类型，返回集合对象集合</returns>
        public List<IGeometry> GetBufferGeometry(IWorkspace pWorkspace, string nameOfFeatureClass, string strWhereClause, string BufferRadius)
        {
            List<IGeometry> pList = new List<IGeometry>();
            IFeatureLayer pFeatureLayer = new FeatureLayerClass();
            pFeatureLayer.FeatureClass = GetFeatureClass(pWorkspace, nameOfFeatureClass);
            pFeatureLayer.Name = nameOfFeatureClass;
            IFeatureSelection pFeatureSelection = pFeatureLayer as IFeatureSelection;
            IQueryFilter pQueryFilter = new QueryFilterClass();
            IFeatureCursor pFeatureCursor;
            pQueryFilter.WhereClause = strWhereClause;
            pFeatureSelection.SelectFeatures(pQueryFilter, esriSelectionResultEnum.esriSelectionResultNew, false);
            ICursor pCursor;
            ISelectionSet pSelectionSet = pFeatureSelection.SelectionSet;
            pSelectionSet.Search(pQueryFilter, false, out pCursor);
            pFeatureCursor = pCursor as IFeatureCursor;
            IGeometry pBufferGeometry;
            ITopologicalOperator pTopologicalOperator;
            IFeature pFeature = pFeatureCursor.NextFeature();
            while (pFeature != null)
            {
                pTopologicalOperator = pFeature.Shape as ITopologicalOperator;
                double dBufferRadius = Convert.ToDouble(BufferRadius);
                pBufferGeometry = pTopologicalOperator.Buffer(dBufferRadius);
                pList.Add(pBufferGeometry);
                pFeature = pFeatureCursor.NextFeature();
            }

            return pList;
        }

        /// <summary>
        /// 根据要素类型获取集合对象类型
        /// </summary>
        /// <param name="featType">esriFeatureType类型，要素类型</param>
        /// <param name="geomType">esriGeometryType类型，传递的拷贝值</param>
        private void GetGeometryTypeByFeatureType(esriFeatureType featType, ref esriGeometryType geomType)
        {
            switch (featType)
            {
                case (esriFeatureType)1:
                    if (geomType == (esriGeometryType)13)
                    {
                        geomType = (esriGeometryType)3;
                    }
                    break;
                case (esriFeatureType)7:
                    geomType = (esriGeometryType)1;
                    break;
                case (esriFeatureType)10:
                    geomType = (esriGeometryType)3;
                    break;
            }
        }

        /// <summary>
        /// 获取空间参考
        /// </summary>
        /// <param name="pFeatureDataset">IFeatureDataset接口类型，要素数据集</param>
        /// <returns>ISpatialReference接口类型，返回空间参考</returns>
        public ISpatialReference GetSpatialReference(IFeatureDataset pFeatureDataset)
        {
            if (pFeatureDataset == null)
            {
                return null;
            }
            IGeoDataset pGeoDataset = pFeatureDataset as IGeoDataset;
            return pGeoDataset.SpatialReference;
        }

        /// <summary>
        /// 判断要素类是否存在
        /// </summary>
        /// <param name="pWorkspace">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="nameOfFeatureClass">String类型，要素类名称</param>
        /// <returns>Boolean类型，要素类存在就返回true,要素类不存在就返回false</returns>
        public bool IsExistFeatureClass(IWorkspace pWorkspace, string nameOfFeatureClass)
        {
            if (pWorkspace != null)
            {
                IFeatureClass pFeatureClass = GetFeatureClass(pWorkspace, nameOfFeatureClass);
                if (pFeatureClass != null)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 判断是否打开数据库编辑空间
        /// </summary>
        /// <param name="ws">IWorkspace接口类型，打开的工作空间</param>
        /// <param name="withUndoRedo">Boolean类型，是否撤销或恢复</param>
        /// <returns>Boolean类型，打开编辑工作空间成功就返回true,打开编辑工作空间失败就返回false</returns>
        public bool OpenWorkspaceForEdit(IWorkspace ws, bool withUndoRedo)
        {
            IWorkspaceEdit wse = ws as IWorkspaceEdit;
            if (wse == null)
            {
                return false;
            }
            if (wse.IsBeingEdited())
            {
                return true;
            }
            try
            {
                wse.StartEditing(withUndoRedo);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region 静态方法

        #endregion

        #endregion
    }
}
