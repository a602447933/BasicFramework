﻿namespace Redis.Library
{
    /// <summary>
    /// 配置信息
    /// </summary>
    public class RedisConfig
    {

        /// <summary>
        /// 当前配置名称（连接池中连接名称）
        /// 此属性为必须
        /// </summary>
        public string SectionName { get; set; }

        /// <summary>
        ///密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///主Redis库，亦可是sentinel服务器地址
        /// </summary>
        public string Masters { get; set; }

        /// <summary>
        /// 从redis库
        /// </summary>
        public string Slaves { get; set; }

        /// <summary>
        /// 非集群模式下可以指定读写db
        /// </summary>
        public int DefaultDatabase { get; set; }

        /// <summary>
        /// 管理员模式
        /// </summary>
        public bool AllowAdmin { get; set; } 

        /// <summary>
        /// 连接保持(s)
        /// </summary>
        public int KeepAlive { get; set; } = 300;

        /// <summary>
        /// 连接超时(ms)
        /// </summary>
        public int ConnectTimeout { get; set; } = 180 * 1000;

        /// <summary>
        ///重连次数
        /// </summary>
        public int ConnectRetry { get; set; } = 10;

        /// <summary>
        /// 任务忙重试次数
        /// 0-50之间的整数
        /// </summary>
        public int BusyRetry { get; set; } = 10;

        /// <summary>
        /// 重试等待时长(ms)
        /// </summary>
        public int BusyRetryWaitMS { get; set; } = 1000;

        /// <summary>
        /// 是否加密
        /// </summary>
        public bool IsEncryption { get; set; }

        /// <summary>
        ///连接池大小
        /// </summary>
        public int PoolSize { get; set; } = 11;

        /// <summary>
        /// 命令超时时间 (ms)
        /// </summary>
        public int CommandTimeout { get; set; } = 60000;

        /// <summary>
        /// 扩展
        /// 有一些redis因为禁用了某些命令需要添加如下部分
        /// $CLIENT=,$CLUSTER=,$CONFIG=,$ECHO=,$INFO=,$PING=
        /// </summary>
        public string Extention { get; set; } = "";

        ///// <summary>
        /////哨兵模式下服务名称
        ///// </summary>
        //public string ServiceName{ get; set; }

        ///// <summary>
        ///// 配置类型 单列 集群 哨兵
        ///// </summary>
        //public int Type{get;set;}

        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 异常信息
        /// </summary>
        public string Ex { get; set; }

        /// <summary>
        /// 异常相关配置
        /// </summary>
        public ExceptionLog ExceptionConfig { get; set; }

    }

    /// <summary>
    /// 异常相关配置
    /// </summary>
    public  class ExceptionLog
    {
        /// <summary>
        /// 使用的数据库节点
        /// </summary>
        public int Database { get; set; }

        /// <summary>
        /// 异常对应的HashId
        /// </summary>
        public string ExceptionHashId { get; set; }
        
    }
}

