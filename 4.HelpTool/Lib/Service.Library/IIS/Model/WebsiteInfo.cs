﻿/*
* 命名空间: Service.Library.IIS
*
* 功 能： IIS网站信息实体类
*
* 类 名： IISWebsiteInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/5/8 11:47:39 	罗维    创建
* ─────────────────────────────────────────────────
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/

using System.Collections.Generic;

namespace Service.Library
{
    /// <summary>
    /// IIS网站信息
    /// </summary>
    public class WebsiteInfo
    {
        /// <summary>
        /// IP
        /// </summary>
        public string IP { get; set; }

        /// <summary>
        /// 端口
        /// </summary>
        public string Port { get; set; }

        /// <summary>
        /// 站点IP:Port
        /// </summary>
        public string DomainPort { get; set; }

        /// <summary>
        /// 应用程序池
        /// </summary>
        public string AppPool { get; set; }

        /// <summary>
        /// 框架版本【4,2,0 表示无托管】
        /// </summary>
        public string FrameworkVersion { get; set; }


        /// <summary>
        /// IIS托管模式【0 集成、1 经典】
        /// </summary>
        public int HostedMode { get; set; }
        
        /// <summary>
        /// 网站名称
        /// </summary>
        public string SiteName { get; set; }
        /// <summary>
        /// 物理路径
        /// </summary>
        public string PhysicalPath { get; set; }

    }
}
