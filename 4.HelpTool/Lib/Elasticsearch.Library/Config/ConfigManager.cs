﻿using System;
using System.IO;

namespace Elasticsearch.Library
{
    /// <summary>
    /// 用于读取配置信息
    /// </summary>
    public class ConfigManager
    {
        /// <summary>
        /// 配置信息
        /// </summary>
        public static EsConfig config { get; private set; }

        /// <summary>
        /// 静态代码块，程序启动时初始化
        /// </summary>
        static ConfigManager()
        {
            try
            {
                string rootDic = AppDomain.CurrentDomain.BaseDirectory;
                using (StreamReader sr = new StreamReader(Path.Combine(rootDic, "Config\\ElasticSearch\\es.config.json")))
                {
                    config = sr.ReadToEnd().JsonToObject<EsConfig>();
                    if (config.IsEncryption)
                    {
                        for (int i = 0; i < config.Urls.Count; i++)
                        {
                            config.Urls[i] = AESEncryptHelper.Decrypt(config.Urls[i]);
                        }
                    }
                    config.Success = true;
                }
            }
            catch (Exception ex)
            {
                config.Success = false;
                config.Ex = ex.Message;
            }
        }
    }
}
