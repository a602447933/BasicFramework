﻿using System;

namespace Dapper.Library
{
    public class DapperExtensionException : System.Exception
    {
        public DapperExtensionException(string msg) : base(msg)
        {

        }
    }
}
