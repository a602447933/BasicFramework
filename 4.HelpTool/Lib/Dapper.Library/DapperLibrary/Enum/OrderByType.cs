﻿namespace Dapper.Library
{
    public enum OrderByType
    {
        Asc = 1,
        Desc = -1
    }
}
