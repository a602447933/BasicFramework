﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dapper.Library
{
    public enum DBType
    {
        SqlServer,
        MySql,
        Oracle,
        PostgreSql,
        Access
    }
}
