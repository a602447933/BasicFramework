﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dapper.Library
{
    public class Identity : BaseAttrbute
    {
        /// <summary>
        /// 是否自增
        /// </summary>
        public bool IsIncrease { get; set; }
        public Identity(bool IsIncrease = false)
        {
            this.IsIncrease = IsIncrease;
        }
    }
}
