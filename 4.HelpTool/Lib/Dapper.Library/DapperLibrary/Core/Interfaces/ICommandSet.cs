﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Dapper.Library
{
	public interface ICommandSet<T>: ICommand<T>
	{
		ICommand<T> Where(Expression<Func<T, bool>> predicate);

		ICommand<T> Where(string sqlWhere, object param = null);

		ICommand<T> WhereIf(bool where, Expression<Func<T, bool>> truePredicate, Expression<Func<T, bool>> falsePredicate);

		ICommand<T> AsTableName(Type type, string tableName);

        /// <summary>
        /// 增加判断不存在条件
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        ICommand<T> IfNotExists(Expression<Func<T, object>> predicate);

    }
}
