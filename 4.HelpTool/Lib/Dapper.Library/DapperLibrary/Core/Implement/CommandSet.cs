﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Text;
using Dapper;


using Dapper.Library;

namespace Dapper.Library
{
    /// <summary>
    /// 指令集
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CommandSet<T> : Command<T>, ICommandSet<T>
    {
        public CommandSet(IDbConnection conn, SqlProvider sqlProvider) : base(conn, sqlProvider)
        {
            TableType = typeof(T);
            SetContext = new DataBaseContext<T>
            {
                Set = this,
                OperateType = EOperateType.Command
            };

            sqlProvider.Context = SetContext;
            WhereExpressionList = new List<LambdaExpression>();
            WhereBuilder = new StringBuilder();
            Params = new DynamicParameters();
            IfNotExistsList = new List<LambdaExpression>();

        }

        public CommandSet(IDbConnection conn, SqlProvider sqlProvider, IDbTransaction dbTransaction) : base(conn, sqlProvider, dbTransaction)
        {
            TableType = typeof(T);
            SetContext = new DataBaseContext<T>
            {
                Set = this,
                OperateType = EOperateType.Command
            };

            sqlProvider.Context = SetContext;
            WhereExpressionList = new List<LambdaExpression>();
            WhereBuilder = new StringBuilder();
            Params = new DynamicParameters();
            IfNotExistsList = new List<LambdaExpression>();
        }

        internal CommandSet(IDbConnection conn, SqlProvider sqlProvider, Type tableType, LambdaExpression whereExpression) : base(conn, sqlProvider)
        {
            TableType = tableType;
            //WhereExpression = whereExpression;
            SetContext = new DataBaseContext<T>
            {
                Set = this,
                OperateType = EOperateType.Command
            };

            sqlProvider.Context = SetContext;
            WhereExpressionList = new List<LambdaExpression>();
            WhereExpressionList.Add(whereExpression);
            WhereBuilder = new StringBuilder();
            Params = new DynamicParameters();
            IfNotExistsList = new List<LambdaExpression>();
        }

        public ICommand<T> AsTableName(Type type, string tableName)
        {
            SqlProvider.AsTableNameDic.Add(type, tableName);
            return this;
        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="predicate"></param>
		/// <returns></returns>
        public ICommand<T> Where(Expression<Func<T, bool>> predicate)
        {
            WhereExpressionList.Add(predicate);
            return this;
        }
		/// <summary>
		/// 使用sql查询条件
		/// </summary>
		/// <param name="sqlWhere"></param>
		/// <param name="param"></param>
		/// <returns></returns>
		public ICommand<T> Where(string sqlWhere, object param = null)
		{
			WhereBuilder.Append(" AND " + sqlWhere);
			if (param != null)
			{
				Params.AddDynamicParams(param);
			}
			return this;
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="where"></param>
		/// <param name="truePredicate"></param>
		/// <param name="falsePredicate"></param>
		/// <returns></returns>
		public ICommand<T> WhereIf(bool where, Expression<Func<T, bool>> truePredicate, Expression<Func<T, bool>> falsePredicate)
		{
			if (where)
				WhereExpressionList.Add(truePredicate);
			else
				WhereExpressionList.Add(falsePredicate);
			return this;
		}


        ///// <summary>
        ///// 增加判断不存在条件
        ///// </summary>
        ///// <param name="predicate"></param>
        ///// <returns></returns>
        //public ICommand<T> IfNotExists(Expression<Func<T, bool>> predicate)
        //{
        //    IfNotExistsExpression = IfNotExistsExpression == null ? predicate : ((Expression<Func<T, bool>>)IfNotExistsExpression).And(predicate);
        //    return this;
        //}

        /// <summary>
        /// 增加判断不存在条件
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public ICommand<T> IfNotExists(Expression<Func<T, object>> predicate)
        {
            IfNotExistsList.Add(predicate);
            return this;
        }
    }
}
