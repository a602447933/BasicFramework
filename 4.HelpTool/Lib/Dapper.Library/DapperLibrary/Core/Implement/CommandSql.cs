﻿using System;
using System.Data;
using System.Linq.Expressions;

namespace Dapper.Library
{
    /// <summary>
    /// 指令集
    /// </summary>
    public class CommandSql<T> : Command<T>, ICommandSql<T>
    {
        public CommandSql(IDbConnection conn, SqlProvider sqlProvider) : base(conn, sqlProvider)
        {
            TableType = typeof(T);
            SetContext = new DataBaseContext<T>
            {
                Set = this,
                OperateType = EOperateType.Command
            };
            sqlProvider.Context = SetContext;
            Params = new DynamicParameters();
        }

        public CommandSql(IDbConnection conn, SqlProvider sqlProvider, IDbTransaction dbTransaction) : base(conn, sqlProvider, dbTransaction)
        {
            TableType = typeof(T);
            SetContext = new DataBaseContext<T>
            {
                Set = this,
                OperateType = EOperateType.Command
            };

            sqlProvider.Context = SetContext;
            Params = new DynamicParameters();
        }

        /// <summary>
        /// 使用SQL执行Command语句
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public CommandSql<T> BySql(string predicate, object parameter)
        {
            SqlProvider.Context.Set.StringSql = predicate;
            SqlProvider.Context.Set.Params.AddDynamicParams(parameter);
            return this;
        }
    }
}
