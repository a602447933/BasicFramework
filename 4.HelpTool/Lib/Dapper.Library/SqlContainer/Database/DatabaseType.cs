﻿namespace Dapper.Library.SqlContainer
{
    /// <summary>
    /// 数据库类型
    /// </summary>
    public enum DatabaseType
    {
        SqlServer,
        MySql,
        Oracle,
        PostgreSql,
        Access,
    }
}
