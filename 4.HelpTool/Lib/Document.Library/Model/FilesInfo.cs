﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Document.Library
{
    /// <summary>
    /// 文件信息
    /// </summary>
    public class FilesInfo
    {

        /// <summary>
        /// 名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 路径
        /// </summary>
        public string path { get; set; }


        /// <summary>
        /// 大小
        /// </summary>
        public double size { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public string media_type { get; set; }

        /// <summary>
        /// 格式
        /// </summary>
        public string media_sub_type { get; set; }

    }
}
