﻿using System;

namespace Document.Library
{    
    /// <summary>
    /// 压缩文件服务接口
    /// </summary>
    public interface IZipFileService
    {
        /// <summary>
        /// 压缩配置
        /// </summary>
        ZipConfig Config { get; }

        /// <summary>
        /// 压缩目录
        /// <para>
        /// 注意：该方法将对异常进行日志记录，但不处理异常，如果有异常，该方法内部将写异常日志，并抛出异常
        /// </para>
        /// </summary>       
        /// <exception cref="System.Exception"></exception>
        void ZipDirectory();

        /// <summary>
        /// 压缩单个文件
        /// <para>
        /// 注意：该方法将对异常进行日志记录，但不处理异常，如果有异常，该方法内部将写异常日志，并抛出异常
        /// </para>
        /// </summary>        
        /// <exception cref="System.Exception"></exception>
        void ZipSingleFile();

        /// <summary>
        /// 解压文件到指定目录
        /// </summary>
        /// <param name="zipedFile">已压缩的文件</param>
        /// <param name="unZipFilesDirPath">解压目标目录</param>
        /// <returns>解压结果</returns>
        bool UnZipFile(string zipedFile, string unZipFilesDirPath);

        /// <summary>
        /// 解压文件到指定目录
        /// </summary>
        /// <param name="zipedFile">已压缩的文件</param>
        /// <param name="unZipFilesDirPath">解压目标目录</param>
        /// <returns>解压结果</returns>
        bool FastUnZipFile(string zipedFile, string unZipFilesDirPath);
    }   
}
