﻿using Encrypt.Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EncryptionDecryption
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, EventArgs e)
        {
            Task.Run(() => {
                string imports = txt_import.Text;
                string results =AESEncryptHelper.Encrypt(imports);
                SetValue(txt_result, results);
            });
        }



        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button2_Click(object sender, EventArgs e)
        {
            Task.Run(() => {
                string imports = txt_import.Text;
                string results = AESEncryptHelper.Decrypt(imports);
                SetValue(txt_result, results);
            });
        }

        /// <summary>
        /// 设置值
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="msg"></param>
        private void SetValue(TextBox textBox, string msg)
        {
            BeginInvoke(new Action(() =>
            {
                textBox.Text = msg;
            }));
        }

    }
}
