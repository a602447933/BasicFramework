﻿namespace TableToEntity
{
    partial class CreateDtoWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreateCS = new System.Windows.Forms.Button();
            this.txtLogicName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtProfile = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtResponse = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEntity = new System.Windows.Forms.TextBox();
            this.txtQuery = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label_namespace = new System.Windows.Forms.Label();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.txtResult = new System.Windows.Forms.TextBox();
            this.comTableType = new System.Windows.Forms.ComboBox();
            this.labDBType = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textNameSpace = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCreateCS
            // 
            this.btnCreateCS.Location = new System.Drawing.Point(474, 419);
            this.btnCreateCS.Name = "btnCreateCS";
            this.btnCreateCS.Size = new System.Drawing.Size(91, 23);
            this.btnCreateCS.TabIndex = 58;
            this.btnCreateCS.Text = "生成逻辑文件";
            this.btnCreateCS.UseVisualStyleBackColor = true;
            this.btnCreateCS.Click += new System.EventHandler(this.btnCreateCS_Click);
            // 
            // txtLogicName
            // 
            this.txtLogicName.Location = new System.Drawing.Point(368, 311);
            this.txtLogicName.Name = "txtLogicName";
            this.txtLogicName.Size = new System.Drawing.Size(124, 21);
            this.txtLogicName.TabIndex = 56;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(301, 315);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 12);
            this.label6.TabIndex = 55;
            this.label6.Text = "Logic名称：";
            // 
            // txtProfile
            // 
            this.txtProfile.Location = new System.Drawing.Point(94, 311);
            this.txtProfile.Name = "txtProfile";
            this.txtProfile.Size = new System.Drawing.Size(131, 21);
            this.txtProfile.TabIndex = 54;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 314);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 12);
            this.label7.TabIndex = 53;
            this.label7.Text = "Profile名称：";
            // 
            // txtResponse
            // 
            this.txtResponse.Location = new System.Drawing.Point(368, 370);
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.Size = new System.Drawing.Size(197, 21);
            this.txtResponse.TabIndex = 46;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(295, 373);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 45;
            this.label2.Text = "返回实体名：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 376);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 44;
            this.label3.Text = "编辑实体名：";
            // 
            // txtEntity
            // 
            this.txtEntity.Location = new System.Drawing.Point(94, 373);
            this.txtEntity.Name = "txtEntity";
            this.txtEntity.Size = new System.Drawing.Size(181, 21);
            this.txtEntity.TabIndex = 43;
            // 
            // txtQuery
            // 
            this.txtQuery.Location = new System.Drawing.Point(369, 343);
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.Size = new System.Drawing.Size(196, 21);
            this.txtQuery.TabIndex = 42;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(295, 346);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 41;
            this.label1.Text = "查询实体名：";
            // 
            // label_namespace
            // 
            this.label_namespace.AutoSize = true;
            this.label_namespace.Location = new System.Drawing.Point(18, 346);
            this.label_namespace.Name = "label_namespace";
            this.label_namespace.Size = new System.Drawing.Size(77, 12);
            this.label_namespace.TabIndex = 40;
            this.label_namespace.Text = "数据库实体：";
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(94, 343);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(183, 21);
            this.txtModel.TabIndex = 39;
            // 
            // txtResult
            // 
            this.txtResult.Location = new System.Drawing.Point(12, 6);
            this.txtResult.Multiline = true;
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(558, 259);
            this.txtResult.TabIndex = 38;
            // 
            // comTableType
            // 
            this.comTableType.DisplayMember = "0";
            this.comTableType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comTableType.FormattingEnabled = true;
            this.comTableType.Items.AddRange(new object[] {
            "List",
            "Tree"});
            this.comTableType.Location = new System.Drawing.Point(94, 277);
            this.comTableType.Name = "comTableType";
            this.comTableType.Size = new System.Drawing.Size(184, 20);
            this.comTableType.TabIndex = 60;
            // 
            // labDBType
            // 
            this.labDBType.AutoSize = true;
            this.labDBType.Location = new System.Drawing.Point(17, 280);
            this.labDBType.Name = "labDBType";
            this.labDBType.Size = new System.Drawing.Size(77, 12);
            this.labDBType.TabIndex = 59;
            this.labDBType.Text = "数据表类型：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(301, 280);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 62;
            this.label4.Text = "命名空间：";
            // 
            // textNameSpace
            // 
            this.textNameSpace.Location = new System.Drawing.Point(368, 277);
            this.textNameSpace.Name = "textNameSpace";
            this.textNameSpace.Size = new System.Drawing.Size(197, 21);
            this.textNameSpace.TabIndex = 61;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(228, 315);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 12);
            this.label5.TabIndex = 63;
            this.label5.Text = "Profile";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(498, 315);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 12);
            this.label8.TabIndex = 64;
            this.label8.Text = "ServiceImpl";
            // 
            // CreateDtoWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 454);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textNameSpace);
            this.Controls.Add(this.comTableType);
            this.Controls.Add(this.labDBType);
            this.Controls.Add(this.btnCreateCS);
            this.Controls.Add(this.txtLogicName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtProfile);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtResponse);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtEntity);
            this.Controls.Add(this.txtQuery);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_namespace);
            this.Controls.Add(this.txtModel);
            this.Controls.Add(this.txtResult);
            this.Name = "CreateDtoWindow";
            this.Text = "CreateDto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnCreateCS;
        private System.Windows.Forms.TextBox txtLogicName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtProfile;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtResponse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEntity;
        private System.Windows.Forms.TextBox txtQuery;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_namespace;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtResult;
        private System.Windows.Forms.ComboBox comTableType;
        private System.Windows.Forms.Label labDBType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textNameSpace;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
    }
}