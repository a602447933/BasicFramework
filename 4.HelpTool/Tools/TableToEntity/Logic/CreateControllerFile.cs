﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
* 命名空间:  TableToEntity
*
* 功 能：生成Api控制器文件
*
* 类 名：CreateControllerFile  
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1   2021/8/4 16:43:23  LW     创建
*
* Copyright (c) 2020 ZHDL Corporation. All rights reserved.
*/
namespace TableToEntity
{
    /// <summary>
    /// 生成Api控制器文件
    /// </summary>
	public class CreateControllerFile
    {
        /// <summary>
        /// 创建列表对应的逻辑文件
        /// </summary>
        /// <param name="dtoInfo"></param>
        public static void CreateListController(DtoInfo dtoInfo)
        {

            string fileName = Path.Combine(dtoInfo.LogicFolder, dtoInfo.LogicName + "Controller.cs");
            StringBuilder sb = new StringBuilder();
            using (StreamWriter sw = File.CreateText(fileName))
            {
                sb.AppendLine("using Microsoft.AspNetCore.Authorization;");
                sb.AppendLine("using System.Collections.Generic;");
                sb.AppendLine("using Microsoft.AspNetCore.Authorization");
                sb.AppendLine("using Microsoft.AspNetCore.Mvc;");
                sb.AppendLine("using Validate.Library;");
                sb.AppendLine("using Common.Model;");
                sb.AppendLine("using Container.Library;");
                sb.AppendLine(string.Format("using {0};", dtoInfo.NameSpace));

                sb.AppendLine("");
                sb.AppendLine("namespace Api.XXXX.Areas.xxxx.Controllers");
                sb.AppendLine("{");
                sb.AppendLine("");
                sb.AppendLine("    /// <summary>");
                sb.AppendLine(string.Format("    /// {0}", dtoInfo.LogicName + "接口控制器"));
                sb.AppendLine("    /// </summary>");
                sb.AppendLine("    [ApiExplorerSettings(GroupName = \"XXXX\")]");
                sb.AppendLine("    [Route(\"ap /XXXX/[controller]\")]");
                sb.AppendLine("    [ApiController]");

                sb.AppendLine(string.Format("    public class {0}Controller:BaseApiController", dtoInfo.LogicName));
                sb.AppendLine("    {");
                sb.AppendLine(string.Format("         private readonly I{0}Service {1}Service = null;", dtoInfo.LogicName, dtoInfo.LogicName.ToLower()));
                sb.AppendLine("         /// <summary>");
                sb.AppendLine("         /// 构造函数");
                sb.AppendLine("         /// </summary>");
                sb.AppendLine(string.Format("          public {0}Controller()", dtoInfo.LogicName));
                sb.AppendLine("          {");
                sb.AppendLine(string.Format("                {0}Service = UnityCIContainer.Instance.GetService<I{1}Service>();", dtoInfo.LogicName.ToLower(), dtoInfo.LogicName));
                sb.AppendLine("          }");
               
                #region 查询

                sb.AppendLine("       #region 查询");
                sb.AppendLine("       /// <summary>");
                sb.AppendLine("       ///根据关键字分页获取列表");
                sb.AppendLine("       /// </summary>");
                sb.AppendLine("       /// <param name=\"inputInfo\"></param>");
                sb.AppendLine("       /// <returns></returns>");
                sb.AppendLine("       [HttpPost(\"LoadPageList\")]");
                sb.AppendLine("       [Authorize]");
                sb.AppendLine("       [ClientApiFilter(DeviceType.PCBack, true)]");
                sb.AppendLine(string.Format("       " +
                                      "public ResultJsonInfo<List<{0}>> LoadPageList([FromBody]ParametersInfo<{1}> inputInfo)", dtoInfo.ResponseModel, dtoInfo.QueryModel));
                sb.AppendLine("       {");
                sb.AppendLine(string.Format("           " +
                    "                     var resultInfo = new ResultJsonInfo<List<{0}>>();", dtoInfo.ResponseModel));
                sb.AppendLine("           TryCatch(() =>");
                sb.AppendLine("           {");
                sb.AppendLine(string.Format("               resultInfo = {0}Service.LoadPageList(inputInfo);", dtoInfo.LogicName.ToLower()));
                sb.AppendLine("           }, ex =>");
                sb.AppendLine("           {");
                sb.AppendLine("              resultInfo.SystemExc(resultInfo, ex, \"获取分页列表失败\");");
                sb.AppendLine("           }, $\"系统错误，XXXXX管理-获取分页列表失败\");");
                sb.AppendLine("           return resultInfo;");
                sb.AppendLine("       }");
                sb.AppendLine("       #endregion");

                //获取附件
                #region 获取附件
                sb.AppendLine("       ///// <summary>");
                sb.AppendLine("       ///// 根据XXXXid获取附件文件");
                sb.AppendLine("       ///// </summary>");
                sb.AppendLine("       ///// <param name=\"XXId\"></param>");
                sb.AppendLine("       ///// <returns></returns>");
                sb.AppendLine("       //[HttpGet(\"LoadAttachList /{goodsId}\")]");
                sb.AppendLine("       //[Authorize]");
                sb.AppendLine("       //[ClientApiFilter(DeviceType.PCBack, true)]");
                sb.AppendLine("       //public ResultJsonInfo<List<GoodsPictureEntity>> LoadAttachList(string goodsId)");
                sb.AppendLine("       //{");
                sb.AppendLine("       //    var resultInfo = new ResultJsonInfo<List<GoodsPictureEntity>>();");
                sb.AppendLine("       //    TryCatch(() =>");
                sb.AppendLine("       //    {");
                sb.AppendLine("       //        resultInfo = goodsInfoService.LoadAttachList(goodsId);");
                sb.AppendLine("       //    }, ex =>");
                sb.AppendLine("       //    {");
                sb.AppendLine("       //        resultInfo.SystemExc(resultInfo, ex, \"根据商品id获取附件文件失败\");");
                sb.AppendLine("       //    }, $\"系统错误，商品管理-根据商品id获取附件文件失败\");");
                sb.AppendLine("       //    return resultInfo;");
                sb.AppendLine("       //}");
                #endregion

                #endregion

                #region 更新
                sb.AppendLine("       #region 更新");

                sb.AppendLine("       /// <summary>");
                sb.AppendLine("       /// 更新xxx信息");
                sb.AppendLine("       /// </summary>");
                sb.AppendLine("       /// <param name=\"inputInfo\"></param>");
                sb.AppendLine("       /// <returns></returns>");
                sb.AppendLine("       [HttpPost(\"SaveInfo\")]");
                sb.AppendLine("       [Authorize]");
                sb.AppendLine("       [ClientApiFilter(DeviceType.PCBack, true)]");
                sb.AppendLine(string.Format("       public ResultJsonInfo<int> SaveInfo([FromBody]{0} inputInfo)", dtoInfo.ModifyModel));
                sb.AppendLine("       {");
                sb.AppendLine("             var resultInfo = new ResultJsonInfo<int>();");
                sb.AppendLine("             TryCatch(() =>");
                sb.AppendLine("             {");
                sb.AppendLine("                  inputInfo.Validate();");
                sb.AppendLine(string.Format("                  resultInfo = {0}Service.SaveInfo(inputInfo);", dtoInfo.LogicName.ToLower()));
                sb.AppendLine("              }, ex =>");
                sb.AppendLine("              {");
                sb.AppendLine("                  resultInfo.SystemExc(resultInfo, ex, \"更新XXXX信息失败\");");
                sb.AppendLine("              }, $\"系统错误，XXX管理-更新XXX信息失败\");");
                sb.AppendLine("              return resultInfo;");
                sb.AppendLine("       }");

                sb.AppendLine("       #endregion");
                #endregion

                #region 删除

                sb.AppendLine("       #region 删除");
                sb.AppendLine("       /// <summary>");
                sb.AppendLine("       /// 删除XXX信息");
                sb.AppendLine("       /// </summary>");
                sb.AppendLine("       /// <param name=\"ids\"></param>");
                sb.AppendLine("       /// <returns></returns>");
                sb.AppendLine("       [HttpPost(\"Remove\")]");
                sb.AppendLine("       [Authorize]");
                sb.AppendLine("       [ClientApiFilter(DeviceType.PCBack, true)]");
                sb.AppendLine("       public ResultJsonInfo<int> Remove(List<string> ids)");
                sb.AppendLine("       {");
                sb.AppendLine("            var resultInfo = new ResultJsonInfo<int>();");
                sb.AppendLine("            TryCatch(() =>");
                sb.AppendLine("            {");

                sb.AppendLine(string.Format("                 resultInfo = {0}Service.Remove(ids);", dtoInfo.LogicName.ToLower()));

                sb.AppendLine("            }, ex =>");
                sb.AppendLine("            {");

                sb.AppendLine("               resultInfo.SystemExc(resultInfo, ex, \"物理删除xxx失败\");");

                sb.AppendLine("            }, $\"系统错误，XX管理-物理XX消息失败\");");
                sb.AppendLine("            return resultInfo;");
                sb.AppendLine("       }");
                sb.AppendLine("       #endregion");
                #endregion

                sb.AppendLine("    }");
                sb.AppendLine("}");

                sw.Write(sb.ToString());
                sb.Clear();
            }
        }
    }
}
